<?php
require( __DIR__ . '/main.inc.php');

$S->setLang();
$S->id_country = 104;

$req = 'cmd=_notify-validate';
foreach($_POST as $k=>$v){
    $v = urlencode(stripslashes($v));
    $req .= "&{$k}={$v}";
}

$txn_id = trim($_POST['txn_id']); //ID Transazione
$ipn_log_check = __DIR__ . "/../../log/ipn/all/{$txn_id}.log";
$fp = fopen($ipn_log_check,"w");
fwrite($fp, json_encode($_POST) ."\n\nREQ: {$req}" );
fclose($fp);

// Now Post all of that back to PayPal's server using curl, and validate everything with PayPal
// We will use CURL instead of PHP for this for a more universally operable script (fsockopen has issues on some environments)

/*
 * ESEMPIO RISPOSTA
 * cmd=_notify-validate&mc_gross=730.00&protection_eligibility=Ineligible&address_status=unconfirmed&item_number1=&tax=0.00&item_number2=&payer_id=3UBLHC6F54WMQ&address_street=Via+della+Libert%C3%A0&payment_date=08%3A29%3A09+Aug+28%2C+2015+PDT&payment_status=Pending&charset=windows-1252&address_zip=32323&mc_shipping=0.00&mc_handling=0.00&first_name=test&address_country_code=IT&address_name=Mario+Rossi&notify_version=3.8&custom=40&payer_status=verified&address_country=Italy&num_cart_items=2&mc_handling1=0.00&mc_handling2=0.00&address_city=Empoli&verify_sign=ADajE2wTYaVJChjQBypPXztcInlFAsfP313FJDqkh2Q8du7saWph0C2F&payer_email=nadia.sarti-buyer%40sottosotto.it&mc_shipping1=0.00&mc_shipping2=0.00&tax1=0.00&tax2=0.00&txn_id=6D398958AH383802V&payment_type=instant&last_name=buyer&address_state=&item_name1=ABITO+DONNA&receiver_email=nadia.sarti%40sottosotto.it&item_name2=CAPOSPALLA+DONNA&quantity1=1&quantity2=1&pending_reason=unilateral&txn_type=cart&mc_gross_1=305.00&mc_currency=EUR&mc_gross_2
=425.00&residence_country=IT&test_ipn=1&transaction_subject=40&payment_gross=&ipn_track_id=dfd86a9ac20b5
 */

if(array_key_exists('test_ipn', $_POST) && 1 === (int) $_POST['test_ipn']){
	$_sandbox = true;
    $url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
}else{
	$_sandbox = false;
    $url = 'https://www.paypal.com/cgi-bin/webscr';
}

$curl_result = $curl_err = '';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded", "Content-Length: " . strlen($req)));
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
$curl_result = @curl_exec($ch);
$curl_err = curl_error($ch);
curl_close($ch);

//$req = str_replace("&", "\n", $req);
//Verifica risultato
//mail("parrella@inol3.com",1,1);
if(strpos($curl_result, "VERIFIED") !== false ){//|| 1) {
	//$txn_id = trim($_POST['txn_id']); //ID Transazione
	$id_basket = trim($_POST['custom']);
	//mail("parrella@inol3.com",2, $req );
	$verified = true;
	
	//Verifica che l'indirizzo e-mail o il codice commerciante sia lo stesso
	if( $_POST['receiver_email']!=paypal_business && $_POST['receiver_id']!=paypal_business ){
		$verified = false;
	}

	//Verifica che la transazione sia completata
	if( $_sandbox ){
		$_POST['payment_status'] = 'Completed';
	}
	if( $_POST['payment_status']!='Completed' ){
		//mail("parrella@inol3.com",'payment_status',1);
		$verified = false;
	}

	//Verifica che la transazione non sia già stata registrata
	$ipn_log = __DIR__ . "/../../log/ipn/{$txn_id}.log";
	if( file_exists( $ipn_log ) ){
		//mail("parrella@inol3.com",'ipn exists',1);
		$verified = false;
	}
	if( !$S->Order_existsCartSes($id_basket) ){
		//mail("parrella@inol3.com",'order not exists',1);
		$verified = false;
	}
	//mail("parrella@inol3.com",3,1);
	if( $verified ){
		//mail("parrella@inol3.com",4,1);
		//Eseguo registrazione ordine
		$id_order = $S->Order_setOrder($id_basket, 'paypal' , $_POST , false);
		$S->Order_sendOrderMail($id_basket);
		//mail("parrella@inol3.com",5,1);
		//Registro IPN in un log
		$fp = fopen($ipn_log,"w");
		fwrite($fp, json_encode($_POST) ."\n\nREQ: {$req}" );
		fclose($fp);
		//mail("parrella@inol3.com",6,1);
	}
	
	
} else {
    $req .= "\n\nSotto Sotto Data NOT verified from Paypal!";
    mail("parrella@inol3.com", "IPN interaction not verified", "$req");
    exit();
}

unset( $S );
?>