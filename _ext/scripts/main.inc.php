<?php
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');

session_start();

if( file_exists('/is_inol3') || file_exists('/is_rp') ){ //Locale
	define('maindir','/SottoSotto/');
}else{
	define('maindir','/');
}

require( __DIR__ . "/config.inc.php");

spl_autoload_register(function($class){
	include( path_site . '_ext/scripts/class/' . $class . '.class.php' );	
});

require( path_site . "_ext/scripts/class/Mailchimp/Mailchimp.php");

$cn = new NE_mysql();
$cn->Charset();
$S = new Orders($cn);

$mandrill_apikey = $S->P('mandrill-apikey');
if( $mandrill_apikey ){
	$S->mailsender = 'mandrill';
	require( path_site . "_ext/scripts/class/Mandrill/Mandrill.php");	
}else{
	require( path_site . "_ext/scripts/class/PHPMailer/PHPMailerAutoload.php");
}

$S->images = json_decode(images,false);

/*foreach($_SESSION as $k=>$v){
	unset($_SESSION[ $k ]);
}*/
//$S->id_customer = $_SESSION['customer_id'] = 1;//Da rimuovere
?>
