<?php
$relative_path = '../../';
require("main.inc.php");

$pre = false;
if( _DEBUGMODE && isset($_GET["act"]) ){
	$_POST = $_GET;
	$pre = true;
}

$act = $_POST['act'];
if( isset($_POST['id_lang']) ) {
	$S->id_lang = $_POST['id_lang'];
	$lang_tmp = isset($_POST['lang']) ? $_POST['lang'] : 'auto';
	$S->setLang($_POST['id_lang'], $lang_tmp );
}else {
	$S->setLang();
}
$S->id_customer = $_POST['id_customer']>0 ? $_POST['id_customer'] : 0;
$S->setCountry();

$Ajax = new Ajax( $S );
$ret = $Ajax->Exec($act);
if( $pre ){
	echo '<pre>';print_r($ret);echo '</pre>';
}else{
	echo $ret;
}

class Ajax{
	private $cn=NULL;
	private $S=NULL;
	private $UI=NULL;

	public function __construct($S){
		$this->S = &$S;
		$this->UI = new UI( $S );
	}
	public function __destruct(){}

	private function BasketFields($basket){
		$_fields = array();
		$_fields['totPrice_txt'] = '&euro; ' . $this->S->Money( $basket->totPrice );
		$_fields['shipping_cost_txt'] =  $basket->shipping_cost_txt;
		$_fields['totBasket_txt'] = $basket->totBasket_txt;
		$_fields['buono_cost_txt'] = $basket->buono_cost_txt;

		return $_fields;
	}

	private function errorsToTxt($_errors){
		$code = array();
		$code[] = '<ul class="errorList">';
		foreach($_errors as $e){
			$code[] = "<li>{$e}</li>";
		}
		$code[] = '</ul>';
		return implode("\n",$code);
	}

	public function Exec($act,$return_type='json'){
		$_status = 0;
		$_errors = array();
		$_errors_fields = array();
		$_msg = '';
		$_fields = array();

		switch($act){
			case 'formContact':

				switch($_POST['to']){
					default:
					case 'bo1':
					case 'bo2':
						$to = 'customercare@sottosotto.it';
						break;
					case 'fi1':
						$to = 'outlet-firenzepietrapiana@sottosotto.it';
						break;
					case 'fi2':
						$to = 'outlet-campibisenzio@sottosotto.it';
						break;
				}
				if( server!='live' || _DEBUGMODE) {
					$to = 'parrella@inol3.com';
				}

				if( empty($_POST['name']) ){
					$_errors[] = $this->S->W('Inserisci il tuo nome');
				}
				if( empty($_POST['surname']) ){
					$_errors[] = $this->S->W('Inserisci il tuo cognome');
				}
				if( !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) && empty($_POST['tel']) ){
					$_errors[] = $this->S->W('&Egrave; necessario inserire almeno un contatto (indirizzo e-mail / telefono) valido.');
				}

				if( !empty($_POST['email']) && !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) ){
					$_errors[] = $this->S->W('Indirizzo e-mail non valido');
				}

				if( empty( trim($_POST['msg'])) ){
					$_errors[] = $this->S->W('Scrivi un messaggio');
				}

				if( !isset($_POST['privacy']) ){
					$_errors[] = $this->S->W('&Egrave; necessario accettare le condizioni sulla privacy');
				}

				if( count($_errors)==0){

					$bodymail = $this->S->getMailTemplate('contatti.html');
					$arr = array("Nome"=>'name',"Cognome"=>'surname' , "Telefono"=>'tel' , "E-mail" => 'email' , 'Messaggio' => 'msg');
					$fields = "";
					foreach($arr as $label=>$k) {
						$fields .= '<tr><td width="120">' . $label . '</td><td>' . $_POST[$k] . '</td></tr>';
					}
					$bodymail = str_replace("#*fields*#" , $fields , $bodymail);

					$this->S->sendMail($to,'Sotto Sotto' , $_POST['email'] , $_POST['name']." ".$_POST['surname'] , "Contatti da sito web" , $bodymail);
					$_status = 1;
				}else{
					$_msg = $this->errorsToTxt($_errors);
				}

				$this->Wait();
				break;
			case 'order-return':
				$flag = false;
				foreach($_POST['prod_return'] as $k=>$v ){
					if($v>0){
						$flag = true;
					}
				}

				if(!$flag){
					$_errors[] = $this->S->W('&Egrave; necessario selezionare almeno un prodotto per il reso');
				}
				if( strlen($_POST['note'])<10 ){
					$_errors[] = $this->S->W('Indicare cortesemente una descrizione appropriata sul motivo del reso.');
				}

				if( count($_errors)>0 ){
					$_msg = $this->errorsToTxt($_errors);
				}else{
					$this->S->Order_setBasketReturn($_POST,true);
					$_fields['title'] = $this->S->W('Messaggio inviato');
					$_msg = $this->S->W('La tua richiesta del reso è stata presa in carico. Al più presto sarai contattato dal nostro staff.');
					$_status = 1;
				}
				$this->Wait();
				break;
			case 'cookielaw-accept':
				setcookie('so_cp',1,time()+365*24*60*60,'/');
				$_status = 1;
				break;
			case 'search-products':
				$opts_product_item_view = new stdClass();
				$opts_product_item_view->ajax = 1;

				$html = array();
				$data = $this->S->ProductsList( $_POST );

				//RADDOPPIO CATALOGO (togliere)
				/*$list = array_merge($data->list , $data->list , $data->list , $data->list);
				$data->list = $list;
				$data->n = $data->n*4;
				$data->tot = $data->tot*4;*/

				//FINE RADDOPPIO CATALOGO

				$n = count($data->list);
				if($n>0){
					foreach($data->list as $r){
						//print_r($r);
						$r = $this->S->ProductsList( array("id"=>$r->id) );
						$html[] = $this->UI->ProductItemView( $r , $opts_product_item_view);
					}
					$_msg = implode("",$html);
				}

				$_fields["n"] = $n;
				$_fields['founded'] = $this->S->W('Trovati %s prodotti %txt=' . $data->tot);
				$_fields['page_max'] = $data->page_max;
				$_fields['page'] = $data->page;
				if($data->q){
					$_fields["q"] = $data->q;
				}

				$opts = new stdClass();
				$opts->id_lang = $_POST['id_lang'];
				$opts->type = 'products';
				if( !empty($_POST['ca']) ){
					$opts->ca = $_POST['ca'];
				}
				if( !empty($_POST['gr']) ){
					$opts->gr = $_POST['gr'];
				}
				if( !empty($_POST['range_price']) ){
					$opts->range_price = $_POST['range_price'];
				}
				if( !empty($_POST['page']) ){
					$opts->page = $_POST['page'];
				}
				if( !empty($_POST['sort']) ){
					$opts->sort = $_POST['sort'];
				}

				$_fields['url'] = $this->S->Url('products' , $opts);
				$_fields['categorie'] = $data->categorie;
				$_fields['marchi'] = $data->marchi;
				$_fields['campagne'] = $data->campagne;

				$_status = 1;
				//print_r($list);
				//$this->Wait();
				break;
			case 'recovery-pw':
				if( empty($_POST['email']) ){
					$_errors[] = $this->S->W('Inserire indirizzo e-mail');
				}else if( !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) ){
					$_errors[] = $this->S->W('Indirizzo e-mail non valido');
				}

				if( count($_errors)==0 ){
					$ck = $this->S->rememberCustomerPW( $_POST['email'] );
					if($ck){
						$_status = 1;
					}else{
						$_errors[] = $this->S->W('L&apos;indirizzo e-mail non corrisponde a nessun utente');
					}
				}

				if( count($_errors)>0 ){
					$_msg = $this->errorsToTxt($_errors);
				}
				$this->Wait();

				break;
			case 'signup-nl':
				if( empty($_POST['email']) ){
					$_errors[] = $this->S->W('E-mail è richiesta');
				}else if( !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) ){
					$_errors[] = $this->S->W('La e-mail non è valida');
				}

				if( !isset($_POST['privacy']) ){
					$_errors[] = $this->S->W('&Egrave; necessario accettare i termini e condizioni di uso e le condizioni sulla privacy');
				}

				if( count($_errors)==0 ){
					$mc_group = null;
					if( isset($_POST['mc-group-name']) && count($_POST['mc-group-name'])>0 ) {
						$mc_group = array(
							"name" => $_POST['mc-group-name'],//array di name
							"groups" => $_POST['mc-group-groups'] //array di groups
						);
					}

					$ret = $this->S->signupNewsletter($_POST['email'],$_POST['id_lang'],$_POST['id_customer'],$_POST['ip'],$_POST['ua'],$mc_group);
					if($ret>0){
						$_status = 1;
						$_msg = $this->S->W('Grazie per esserti iscritto');
						$_fields = array('title'=>'Success');
					}else{
						$_errors[] = $this->S->W('E-mail già presente nella nostra lista');
					}
				}
				if( count($_errors)>0 ){
					$_msg = $this->errorsToTxt($_errors);
				}
				$this->Wait();
				break;
			case 'changePw':
				$ck = $this->S->verifyCustomerPW($_POST['id_customer'],$_POST['pw']);
				if( !$ck ){
					$_errors[] = $this->S->W('Password corrente non valida');
				}

				$min_length = (int)$this->S->P('pw-min-length');
				if( strlen($_POST['new_pw'])<$min_length ){
					$_errors[] = $this->S->W('La password deve essere di almeno %s caratteri %txt=' . $min_length);
				}else if($_POST['new_pw']!=$_POST['new_pw2']){
					$_errors[] = $this->S->W('La password di conferma non coincide');
				}

				if(count($_errors)==0){
					$this->S->setCustomerNameAndPassword($_POST['id_customer'] , false , $_POST['new_pw']);
					$_status = 1;
					$_msg = $this->S->W('Password changed');
				}else{
					$_msg = $this->errorsToTxt($_errors);
				}
				$this->Wait();
				break;
			case 'registerAccount':
				$ip = $_SERVER['REMOTE_ADDR'];
				$ua = $_SERVER['HTTP_USER_AGENT'];

				$email = trim($_POST['email']);
				$pw = trim($_POST['pw']);

				$email_flag = false;
				if( empty($email) ){
					$_errors[] = $this->S->W('L&apos;indirizzo e-mail è necessario');
				}else if( !filter_var($email,FILTER_VALIDATE_EMAIL) ){
					$_errors[] = $this->S->W('Indirizzo e-mail non valido');
				}else{
					$ck = $this->S->verifyCustomerEmailReg($email);
					if(!$ck){
						$_errors[] = $this->S->W('L&apos;indirizzo e-mail è associato con un altro account');
					}else{
						$email_flag = true;
					}
				}
				if( $email_flag && $email!=$_POST['email_confirm'] ){
					$_errors[] = $this->S->W('L&apos;indirizzo e-mail non è valido');
				}

				if( strlen($pw)<8 ){
					$_errors[] = $this->S->W('La password deve essere di almeno 8 caratteri');
				}else if($pw!=$_POST['pw_confirm']){
					$_errors[] = $this->S->W('La password di conferma non corrisponde');
				}
			case 'saveAccount':
			case 'BasketConfirmation':
				/*if( empty($_POST['company']) ){
					$_errors[] = $this->S->W('La ragione sociale è necessaria');
				}*/

				if( $act!='BasketConfirmation' ){
					if( !($_POST['id_country-bill']>0) ){
						$_errors[] = $this->S->W('Selezionare lo stato');
					}else{
						/*
						//******* VERIFICA PARTITA IVA *****
						
						$Country = $this->S->getCountry($_POST['id_country-bill']);
						
						$vat_required = $Country->cee ? true : false;
						if( empty($_POST['vat']) && $vat_required){
							$_errors[] = $this->S->W('VAT is required');
						}else{
							$vat_required = true;
						}
						
						//CONTROLLA SE LA PARTITA IVA NON DEVE ESSERE CONTROLLATA
						$vat_disallowed = file(path_site . '_public/extra/vat_disallowed.inc.php');
						$vat_disallowed = array_map('trim',$vat_disallowed);							
						if( in_array($_POST['vat'],$vat_disallowed) ){
							$ck = true;
						}else{//Controlla la partita iva							
							if( $Country ){
								if( $Country->vat_check && strlen($_POST['vat'])>0 ){ //Indica il controllo della partita Iva col server esterno europeo
									$ck = $this->S->checkVatNumber($Country->iso_code_vat,$_POST['vat']);
								}else{
									$ck = strlen($_POST['vat'])>5 || $vat_required ? true : false;
								}
							}else{
								$ck = false;
							}
						}
						
						if( !$ck ){
							$_errors[] = $this->S->W('VAT is not valid');
						}else if($vat_required || strlen($_POST['vat'])>0 ){
							$id_customer = $act=='registerAccount' ? 0 : $_POST['id_customer'];
							$ck = $this->S->uniqueVatNumber($_POST['vat'],$id_customer,$_POST['id_country-bill']);
							if( !$ck ){
								$_errors[] = $this->S->W("VAT number is registered by another user");
							}
						}
						/* FINE VERIFICA PARTITA IVA */
					}
				}

				if( $act=='BasketConfirmation' ){
					//Dati contatti
					$arr = array("name"=>"Nome","surname"=>"Cognome","city"=>"Città","address"=>"Indirizzo","zip"=>"CAP","tel"=>"Telefono");
					$aree = array("bill"=>"Tuoi dati","shipping"=>"Dati di spedizione");
					foreach($aree as $area=>$area_descr){
						foreach($arr as $k=>$v){
							if( empty($_POST[$k . "-" . $area]) ){
								$txt = "Il campo {$v} ne \"{$area_descr}\" è richiesto";
								$_errors[] = $this->S->W($txt);
							}
						}
					}
				}

				if( !($_POST["privacy"]==1) && $act!='saveAccount' ){
					$_errors[] = $this->S->W('&Egrave; necessario accettare i termini e condizioni di uso e le condizioni sulla privacy');
				}

				if( $act=='BasketConfirmation' && !isset($_POST['payment'])){
					$_errors[] = $this->S->W('Selezionare il metodo di pagamento preferito');
				}

				if( count($_errors)==0 ){
					if( $act=='BasketConfirmation'){
						$_status = 1;
						$this->S->Order_saveOrderData($_POST['id_basket'],$_POST);
					}else if($act=='registerAccount'){
						$_status = 1;
						$id = $this->S->registerCustomer( $_POST , true);
						$_fields['id_customer'] = $id;
					}else if($act=='saveAccount'){
						$_status = 1;
						$this->S->saveCustomer($_POST['id_customer'],$_POST);
					}
				}else{
					$_msg = $this->errorsToTxt($_errors);
				}
				$this->Wait();
				break;
			case 'updatePriceBasket':
				$list = array();
				$total_basket = 0;
				foreach($_POST["price"] as $k=>$v){
					$list[] = array(
						"id" => $k,
						"v" => "&euro; " . $this->S->Money( $v * $_POST["qty"][ $k ] )
					);
				}

				$Basket = $this->S->Order_getBasket($_POST['id_basket']);

				$_status = 1;
				$_fields["list"] = $list;
				$_fields["total_basket"] = $Basket->totBasket;
				$_fields["total_basket_txt"] = "&euro; " . $this->S->Money( $Basket->totBasket );
				$_fields["buono_cost"] = $Basket->buono_cost;
				$_fields["buono_cost_txt"] = $Basket->buono_cost_txt;
				$_fields["vat"] = $Basket->vat;
				$_fields["vat_cost"] = $Basket->vat_cost;
				$_fields["vat_cost_txt"] = $Basket->vat_cost>0 ? "&euro; " . $this->S->Money( $Basket->vat_cost ) : $this->S->W("Free");
				$_fields["total"] = $Basket->totPrice;
				$_fields["total_txt"] = "&euro; " . $this->S->Money( $Basket->totPrice );
				break;
			case 'updateBasket':
				$list = array();
				foreach($_POST["qty"] as $k=>$v){
					$this->S->Order_updateQty($_POST["id_basket"],$k,$v);
				}

				$_status = 1;
				break;
			case 'removeProductBasket':
				$this->S->Order_removeBasket($_POST["id_basket"],$_POST["id_ps"]);
				$_status = 1;
				break;
			case 'addCoupon':
				$coupon = trim($_POST["coupon"]);
				if( empty($coupon) ){
					$_errors[] = $this->S->W('Insert coupon code');
				}else{
					$ck = $this->S->Order_checkCoupon($_POST['id_customer'],$coupon,$_POST['id_country']);
					if( $ck==-1){
						$_errors[] = $this->S->W('Coupon code is incorrect');
					}else if($ck==-2){
						$_errors[] = $this->S->W('Coupon just used');
					}
				}

				$_errors = array();
				//$_errors[] = $this->S->W('Discounts can\'t be combined with other promotions');

				if( count($_errors)==0 ){
					$_status = 1;
					$r = $this->S->Order_addCoupon($_POST['id_basket'],$coupon,$_POST['id_country']);
					if( $r ){
						$_status = 1;
					}else{
						$_msg = $this->S->W('Coupon code is incorrect');
					}
				}else{
					$_msg = $this->errorsToTxt($_errors);
				}
				break;

			//OLD
			case 'InvitaAmici':

				if(empty($_POST['emails'])){
					$_errors[] = $this->S->W('Nessun indirizzo e-mail inserito');
				}else{
					$emails = explode(",",$_POST['emails']);

					$emails_errors = $_POST['email'] = array();
					foreach($emails as $e){
						$e = trim($e);
						if( !filter_var($e,FILTER_VALIDATE_EMAIL) ){
							$emails_errors[] = $e;
						}

						$_POST['email'][] = $e;
					}
					if( count($emails_errors)>0 ){
						$_errors[] = $this->S->W("I seguenti indirizzi e-mail non sono validi: ") . implode(", " , $emails_errors);
					}
				}

				if(count($_errors)>0 ){
					$_msg = $this->errorsToTxt($_errors);
				}else{
					$ret = $this->Exec('inviaInvitaAmici','return');
					if($ret['status']==1){
						$_status = 1;
						$_fields['title'] = $this->S->W('Inviti inviati');
						$_msg = $this->S->W('Gli inviti sono stati inviati con successo.');
					}else{
						$_msg = $ret['msg'];
					}
				}

				$this->Wait();
				break;
			case 'save-datiSpedizione':
			case 'save-datiFatturazione':
				$_sped = $act=='save-datiFatturazione' ? '' : '_sped';

				if( $act=='save-datiFatturazione' && empty($_POST['name_surname'])){
					$_errors[] = $this->S->W('Nome necessario');
				}else if( $act=='save-datiSpedizione' && empty($_POST['name_sped'])){
					$_errors[] = $this->S->W('Nome necessario');
				}

				if( empty($_POST['surname' . $_sped]) ){
					$_errors[] = $this->S->W('Cognome necessario');
				}
				/*if( empty($_POST['co' . $_sped]) ){
					$_errors[] = $this->S->W('C/O (presso) necessario');
				}*/
				if( empty($_POST['address' . $_sped]) ){
					$_errors[] = $this->S->W('Indirizzo necessario');
				}
				if( empty($_POST['cap' . $_sped]) ){
					$_errors[] = $this->S->W('CAP necessario');
				}
				if( empty($_POST['city' . $_sped]) ){
					$_errors[] = $this->S->W('Localit&agrave; necessaria');
				}
				if( 2!=strlen($_POST['province' . $_sped]) ){
					$_errors[] = $this->S->W('Provincia necessaria');
				}
				if( strlen($_POST['phone' . $_sped])<6 ){
					$_errors[] = $this->S->W('Telefono necessario o non valido');
				}

				if( count($_errors)==0 ){
					$type = $act=='save-datiSpedizione' ? 'spedizione' : 'fatturazione';
					$this->S->setDataCustomer($_POST['id_customer'],$type,$_POST);
					$_status = 1;
				}else{
					$_msg = $this->errorsToTxt($_errors);
				}

				$this->Wait();
				break;
			case 'save-infoAccount':
				/*if( empty($_POST['name']) ){
					$_errors[] = $this->S->W('Il nome utente &egrave; necessario');
				}*/

				if( empty($_POST['pw']) && empty($_POST['pw_new']) ){
					$pwsave = false;
				}else{
					$pwsave = true;
					$min_length = $this->S->P('pw-min-length');
					if( strlen($_POST['pw_new'])<$min_length ){
						$_errors[] = $this->S->W('La password deve essere di almeno 6 caratteri');
					}else if( $_POST['pw_new']!=$_POST['pw_new2'] ){
						$_errors[] = $this->S->W('La password di conferma non corrisponde');
					}
					if( !$this->S->verifyCustomerPW($_POST['id_customer'],$_POST['pw']) ){
						$_errors[] = $this->S->W('La password non &egrave; corretta');
					}
				}

				if(count($_errors)>0){
					$_msg = $this->errorsToTxt($_errors);
				}else{
					$pw = $pwsave ? $_POST['pw_new'] : false;
					$this->S->setCustomerNameAndPassword($_POST['id_customer'] , false , $pw);
					$_status = 1;
				}

				$this->Wait();
				break;
			case 'remember_pw':
				if( empty($_POST['email']) ){
					$_errors[] = $this->S->W('Inserisci il tuo indirizzo e-mail');
				}else if( !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) ){
					$_errors[] = $this->S->W('Indirizzo e-mail non valido');
				}

				if( count($_errors)==0 ){
					$ret = $this->S->rememberCustomerPW($_POST['email']);
					if(!$ret){
						$_errors[] = $this->S->W('Indirizzo e-mail non trovato');
					}else{
						$_msg = $this->S->W('Una e-mail con la tua nuova password &egrave; stata inviata al tuo indirizzo di posta');
						$_status = 1;
					}
				}

				if( count($_errors)>0 ){
					$_msg = $this->errorsToTxt($_errors);
				}

				$this->Wait();
				break;
			case 'register':
				if( empty($_POST['name_surname']) ){
					$_errors[] = $this->S->W('Il campo nome &egrave; necessario');
				}
				if( empty($_POST['surname']) ){
					$_errors[] = $this->S->W('Il campo cognome &egrave; necessario');
				}
				if( empty($_POST['email']) ){
					$_errors[] = $this->S->W('Il campo e-mail &egrave; necessario');
				}else if( !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) ){
					$_errors[] = $this->S->W('Indirizzo e-mail non valido');
				}else{
					$ck = $this->S->getCustomerByMail($_POST['email']);
					if($ck!=-1){
						$_errors[] = $this->S->W('L\'e-mail scelta &egrave; gi&agrave; in uso da un altro utente');
					}
				}
				$min_length = $this->S->P('pw-min-length');
				if( strlen($_POST['pw'])<$min_length ){
					$_errors[] = $this->S->W('La password deve essere di almeno 6 caratteri');
				}else if( $_POST['pw']!=$_POST['pw2'] ){
					$_errors[] = $this->S->W('La password di conferma non corrisponde');
				}
				if($_POST['privacy']!=1){
					$_errors[] = $this->S->W('&Egrave; necessario accettare il consenso per la privacy e le condizioni');
				}

				if( count($_errors)==0 ){
					$this->S->registerCustomer( $_POST );
					$_status = 1;
				}else{
					$_msg = $this->errorsToTxt($_errors);
				}
				$this->Wait();
				break;
			case 'login':
				$login = $_POST['login'];
				$pw = $_POST['pw'];

				if(empty($login)){
					$_errors[] = $this->S->W("Inserisci il tuo indirizzo e-mail");
				}else if( !filter_var($login,FILTER_VALIDATE_EMAIL) ){
					$_errors[] = $this->S->W("Indirizzo e-mail non valido");
				}
				if( empty($pw) ){
					$_errors[] = $this->S->W('La password è richiesta');
				}

				if( count($errors)==0 ){
					$ret = $this->S->LoginCustomer($login,$pw);
					if( $ret>0 ){
						$_status = 1;
					}else{
						switch($ret){
							case "-1":
								$_msg = $this->S->W('I dati di accesso non sono validi<br>Prova nuovamente o clicca su "Hai dimenticato la tua password?"');
								break;
							case "-2":
								$_msg = $this->S->W('Il tuo account non è abilitato. Conferma il tuo indirizzo e-mail cortesemente.');
								break;
						}
					}
				}

				$this->Wait();
				break;
			case 'addWishlist':
				$type = $_POST['type'];
				$id = $type=='brand' ? $_POST['id_brand'] : $_POST['id_product'];
				$ret = $this->S->Product_addWishlist($id,$type);
				if( $ret ){
					$_fields['operation'] = $ret; //"remove" or "add"
					$_status = 1;
				}else{
					$_msg = $this->S->W('Accedi con il tuo account perch&eacute; possiamo ricordati di un prodotto');
				}
				$this->Wait();
				break;
			case 'search':
			case 'all-products':
				$opts = NULL;

				$opts = new stdClass();
				if($act=='search'){
					$opts->search = 'search';
					$opts->search_string = $_POST['s'];
				}else{
					$opts->search = 'all';
				}
				$opts->orderBy = $_POST['orderBy'];
				$opts->order = $_POST['order'];
				$opts->page = $_POST['page'];
				$opts->limit = 12;

				$products = $this->S->Product(false,false,$opts);
				//$nProd = count($products);
				$nProd = $products->prod_tot;
				if($act=='search'){
					$_fields['founded'] = $nProd;
				}

				if($nProd==0){
					$code[] = '<div class="Margin"></div>';
					$code[] = '<section class="Page"><div class="container"><div class="row"><div class="col-md-12"><h3 class="text-center">' . $this->S->W('Nessun risultato trovato') . '</h3></div></div></section>';
				}else{
					/*$opts->page = $_POST['page'];
					$opts->limit = 12;

					$products = $this->S->Product(false,false,$opts);*/

					$maxPage = ceil($nProd/$opts->limit);
					if( $opts->page > $maxPage-1 ){
						$_fields['stop'] = 1;
						$opts->page = $maxPage-1;
					}else{
						$_fields['stop'] = 0;
					}
					$_fields['page'] = intval($opts->page)+1;

					$code = array();
					if( count($products)>0 ){
						if( $opts->page==0 ){
							if(isset($_fields['founded'])){
								$code[] = '<div class="col-md-12"><h4>' . $this->S->W("Risultati trovati: <b>%s</b> %txt={$_fields['founded']}") . '</h4><hr></div>';
							}else{
								$code[] = '<div class="Margin"></div>';
							}
							$code[] = '<section id="PL" class="Page"><div class="container">';
							$code[] = '<div class="row">';
							$code[] = '<div class="col-md-12">';
							$code[] = '<ul class="ProductsOrderBar">';

							$code[] = '<li><a href="#" class="visibility-off">&nbsp;</a></li>';

							$arr = array();
							$o = new stdClass();
							$o->ob = 'a';
							$o->label = 'Disponibilit&agrave;';
							$arr[] = $o;
							$o = new stdClass();
							$o->ob = 'p';
							$o->label = 'Prezzo';
							$arr[] = $o;
							$o = new stdClass();
							$o->ob = 'std';
							$o->label = 'Standard';
							$arr[] = $o;

							$class = false;
							foreach($arr as $o){
								if( $opts->orderBy==$o->ob ){
									$class = 'fired';
									if( $o->ob!='std' ){
										if( $opts->order=='asc'){
											$order = 'desc';
											$class .= ' asc';
										}else{
											$order = 'asc';
											$class .= ' desc';
										}
									}
								}else{
									if($o->ob!='std'){
										$class = "desc";
										$order = 'desc';
									}
								}

								$class = $class ? ' class="' . $class . '"' : '';
								$code[] = '<li><a href="javascript:void(0);" onclick="System.Search(\'' . $o->ob . '\',\'' . $order .'\',0);"' . $class . '">' . $this->S->W($o->label) . '</a></li>';
							}
							$code[] = '</ul>';
							$code[] = '</div>';
						}

						$opts = new stdClass();
						$opts->ajax = true;
						foreach($products as $product){
							$code[] = '<div class="col-md-4 col-sm-6 col-xs-12">' . $this->UI->ProductItem($product,$opts) . '</div>';
						}

						if($opts->page==0){
							$code[] = '</div>';
							$code[] = '</div></section>';
						}
					}
				}
				$code = implode("\n",$code);
				$code = $this->S->toHtml($code);

				$_status = 1;
				$_msg = $code;
				break;
			case 'brand_products':
				$opts = NULL;
				$opts = new stdClass();
				$opts->orderBy = $_POST['orderBy'];
				$opts->order = $_POST['order'];

				$products = $this->S->Product(false,$_POST['id_brand'],$opts);

				$code = array();
				if( count($products)>0 ){
					$code[] = '<div class="Margin"></div>';
					$code[] = '<section id="PL" class="Page"><div class="container">';
					$code[] = '<div class="row">';
					$code[] = '<div class="col-md-12">';
					$code[] = '<ul class="ProductsOrderBar">';

					$code[] = '<li><a href="javascript:void(0);" onclick="System.openOtherBrandsMenu();" class="otherbrands">' . $this->S->W('Altre Offerte') . '</a></li>';

					$arr = array();
					$o = new stdClass();
					$o->ob = 'a';
					$o->label = $this->S->W('Disponibilit&agrave;');
					$arr[] = $o;
					$o = new stdClass();
					$o->ob = 'p';
					$o->label = $this->S->W('Prezzo');
					$arr[] = $o;
					$o = new stdClass();
					$o->ob = 'std';
					$o->label = $this->S->W('Standard');
					$arr[] = $o;

					$class = false;
					foreach($arr as $o){
						if( $opts->orderBy==$o->ob ){
							$class = 'fired';

							if( $o->ob!='std' ){
								if( $opts->order=='asc'){
									$order = 'desc';
									$class .= ' asc';
								}else{
									$order = 'asc';
									$class .= ' desc';
								}
							}
						}else{
							if($o->ob!='std'){
								$class = "desc";
								$order = 'desc';
							}
						}

						$class = $class ? ' class="' . $class . '"' : '';
						$code[] = '<li><a href="javascript:void(0);" onclick="System.BrandProducts(\'' . $o->ob . '\',\'' . $order .'\');"' . $class . '>' . $this->S->W($o->label) . '</a></li>';
					}
					$code[] = '</ul>';
					$code[] = '</div>';

					$opts = new stdClass();
					$opts->ajax = true;
					foreach($products as $product){
						$code[] = '<div class="col-md-4 col-sm-6 col-xs-12">' . $this->UI->ProductItem($product,$opts) . '</div>';
					}
					$code[] = '</div>';
					$code[] = '</div></section>';
				}
				$code = implode("\n",$code);
				$code = $this->S->toHtml($code);

				$_status = 1;
				$_msg = $code;
				break;
			case 'sendBasket':
				$id_cart_ses = $_POST['id_cart_ses'];
				$step = $_POST['step'];

				switch($step){
					case 1:
						if( $_POST['regalo']==1 ){
							if( empty($_POST['regalo_destinatario']) ){
								$_errors[] = $this->S->W('Destinatario non compilato');
							}
							if( empty($_POST['regalo_mittente']) ){
								$_errors[] = $this->S->W('Mittente non compilato');
							}
							if( empty($_POST['regalo_messaggio']) ){
								$_errors[] = $this->S->W('Messaggio non compilato');
							}
						}else{
							$this->S->Order_deleteBasketGift($id_cart_ses);
						}

						if( count($_errors)==0 && $_POST['regalo']==1 ){
							$this->S->Order_setBasketGift($id_cart_ses,$_POST['regalo_destinatario'],$_POST['regalo_mittente'],$_POST['regalo_messaggio']);
						}
						break;
					case 2:
						$_POST = array_map('trim', $_POST);
						if( $_POST['use_customer_data']==1 ){
							$use_customer_data = 1;
						}else{
							$use_customer_data = $_POST['use_customer_data'] = 0;
							$_POST['name_sped'] = $_POST['name_surname'];
							$_POST['surname_sped'] = $_POST['surname'];
							$_POST['co_sped'] = $_POST['co'];
							$_POST['address_sped'] = $_POST['address'];
							$_POST['cap_sped'] = $_POST['cap'];
							$_POST['city_sped'] = $_POST['city'];
							$_POST['province_sped'] = $_POST['province'];
							$_POST['phone_sped'] = $_POST['phone'];
						}

						if( empty($_POST['name_surname']) ){
							$_errors[] = $this->S->W('Campo Nome necessario');
						}
						if( empty($_POST['surname']) ){
							$_errors[] = $this->S->W('Campo Cognome necessario');
						}
						if( strlen($_POST['name_sped'])==0 && $use_customer_data ){
							$_errors[] = $this->S->W('Campo Nome spedizione necessario');
						}
						if( empty($_POST['surname_sped']) && $use_customer_data ){
							$_errors[] = $this->S->W('Campo Cognome spedizione necessario');
						}
						if( empty($_POST['address']) && !$use_customer_data ){
							$_errors[] = $this->S->W('Campo Indirizzo necessario');
						}
						if( empty($_POST['address_sped']) && $use_customer_data ){
							$_errors[] = $this->S->W('Campo Indirizzo spedizione necessario');
						}
						if( empty($_POST['cap']) && !$use_customer_data ){
							$_errors[] = $this->S->W('Campo CAP necessario');
						}
						if( empty($_POST['cap_sped']) && $use_customer_data ){
							$_errors[] = $this->S->W('Campo CAP spedizione necessario');
						}
						if( empty($_POST['city']) && !$use_customer_data ){
							$_errors[] = $this->S->W('Campo Localit&agrave; necessario');
						}
						if( empty($_POST['city_sped']) && $use_customer_data ){
							$_errors[] = $this->S->W('Campo Localit&agrave; spedizione necessario');
						}
						if( empty($_POST['phone']) && !$use_customer_data ){
							$_errors[] = $this->S->W('Campo Telefono necessario');
						}
						if( empty($_POST['phone_sped']) && $use_customer_data ){
							$_errors[] = $this->S->W('Campo Telefono spedizione necessario');
						}
						if( !empty( $_POST['email_sped']) && $use_customer_data && !filter_var($_POST['email_sped'],FILTER_VALIDATE_EMAIL) ){
							$_errors[] = $this->S->W('Formato e-mail spedizione non valido');
						}

						if( count($_errors)==0 ){
							$this->S->Order_setCustomerAddress( $_POST );
						}

						break;
					case 3:
						$basket = $this->S->Order_getBasket(false,$id_cart_ses);
						if( $basket->contrassegno ){
							$_fields["gotoPaypal"] = 0;
						}else{
							$_fields["gotoPaypal"] = 1;
						}
						break;
				}

				if( count($_errors)>0 ){
					$_msg = $this->errorsToTxt($_errors);
				}else{
					$_status = 1;
				}

				break;
			case 'removeBasketProd':
				$id_prod_cart_ses = $_POST['id_prod_cart_ses'];
				$ret = $this->S->Order_removeBasket($id_prod_cart_ses);
				if( $ret ){
					$_status = 1;
					$basket = $this->S->Order_getBasket();
					$_fields = $this->BasketFields($basket);
					$tmp = $this->Exec('getBasket',false);
					$_fields['BasketQuickly'] = $tmp['fields']['BasketQuickly'];
					$_fields['totQty'] = $tmp['fields']['totQty'];
				}
				break;
			case 'updateBasketQty':
				$id_prod_cart_ses_change = $_POST['id_prod_cart_ses_change'];
				foreach($_POST['qty'] as $id_prod_cart_ses=>$qty){
					if( $id_prod_cart_ses_change == $id_prod_cart_ses ){
						$ret = $this->S->Order_updateQty($id_prod_cart_ses,$qty);
						if( $ret===-1 ){
							$_msg = $this->S->W("Quantità non disponibile");
						}else if($ret){
							$_status = 1;

							$basket = $this->S->Order_getBasket();
							$_fields = $this->BasketFields($basket);

							$_fields['qty'] = $qty;
							foreach($basket->list as $r){
								if( $r->id==$id_prod_cart_ses_change ){
									$_fields['totPriceRow_txt'] = '&euro; ' . $this->S->Money( $r->totPriceRow );
								}
							}
						}
					}
				}
				$this->Wait();
				break;
			case 'addBasket':
				$qty = $_POST['qty'];
				if( !($_POST['idart']>0) ) {
					$_msg = $this->S->W('Selezionare una taglia');
				}else if($qty<1){
					$_msg = $this->S->W('Selezionare una quantità');
				}else if( $qty > $this->S->Qty($_POST['idart']) ){
					$_msg = $this->S->W('Quantità non disponibile');
				}else{
					$this->S->Order_addBasket($_POST['idart']);
					$_status = 1;
				}

				$this->Wait();
				break;

				foreach($_POST['qty'] as $k=>$v){
					list($id_color,$id_ps,$id_size) = explode(",",$k);
					$PS = $this->S->ProductSimple($id_ps);
					$qty = intval($v);
					if( $qty>0 && $PS->id_color=$id_color ){
						$qty = "+{$qty}";
						$ret = $this->S->Order_addBasket($id_ps,$qty);
						$flag = true;
					}
				}

				if( !$flag ){
					$_msg = $this->S->W('Scegli una quantità');
				}else{
					$_fields['id_color'] = $_POST['id_color'];
					$_status = 1;
				}

				$this->Wait();
				break;
			case 'getBasket':
				$basket = $this->S->Order_getBasket();
				$code = array();
				$code[] = '<table width="100%" class="table table-striped">';
				/* Se si vuole attivare un numero massimo di prodotti visibili
				$BasketQuickly_prod_show = $this->S->P('BasketQuickly_prod_show');
				$n = count($basket->list)>$BasketQuickly_prod_show ? $BasketQuickly_prod_show : count($basket->list);
				for($i=0;$i<$n;$i++){
					$l = $basket->list[$i];*/
				foreach($basket->list as $l){

					$txt = $l->quant_prod>0 ? $l->quant_prod . ' x ' : '';
					$txt .= $l->Product->titolo;
					$opts = new stdClass();
					$opts->id = $l->Product->id;
					$opts->type = 'scheda-prodotto';
					$code[] = '<tr><td>';
					$code[] = '<a href="javascript:void(0);" onclick="System.removeBasketQuickly(' . $l->id . ');" class="glyphicon glyphicon-remove"></a>';
					$code[] = '<a href="' . $this->S->Url('scheda-prodotto', $opts) .'" class="aProd">';
					$code[] = '<span>' . $txt .'</span><br>';
					$code[] = '<img src="' . $this->S->Img($l->Product->main_img,'prodBasketQuickly') .'" alt="">';
					$code[] = '<div class="price">&euro; ' . $this->S->Money($l->totPriceRow) . '</div>';
					$code[] = '</a>';
					$code[] = '</td></tr>';
				}

				/*if( count($basket->list)>$BasketQuickly_prod_show ){
					$delta = count($basket->list) - $BasketQuickly_prod_show;
					$code[] = '<tr><td colspan="3"><div class="text-center va"><br>&hellip;' . $this->S->W('altri <b>%s</b> prodotti nel carrello %txt='.$delta) . '<br></td></tr>';
				}*/

				$code[] = '<tr><td>
					<div class="Tot">' . $this->S->W('Totale') .': <b>&euro; ' . $this->S->Money($basket->totPrice) . '</b></div>
					<br>
				</td></tr>';
				$code[] = '</table>';
				$_fields['BasketQuickly'] = implode("\n",$code);
				$_fields['totQty'] = $basket->totQty;//Quantità totale nel carrello

				$_status = 1;
				break;
		}


		$ret = array('status'=>$_status, 'msg'=>$_msg, 'errors'=>$_errors,'errors_fields'=>$_errors_fields,'fields'=>$_fields);
		if( $return_type=='json' ){
			return json_encode($ret);
		}else{
			return $ret;
		}
	}

	private function Wait(){
		usleep(.5*1000000);
	}

	private function postToMsg(){
		$msg = "";
		foreach($_POST as $k=>$v){
			$msg .= "{$k} = ";
			if( is_array($v) ){
				foreach($v as $sub_k=>$sub_v){
					$msg .= "<br> --{$sub_k} = {$sub_v}";
				}
			}else{
				$msg .= $v;
			}
			$msg .= "<br>";
		}
		return $msg;
	}
}

$cn->Close();
unset($S,$cn,$Ajax);
?>