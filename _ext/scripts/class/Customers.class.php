<?php
class Customers extends System{
	private $customer_pw_key = 'wG$h%J2g3a%LEf';
	
	public function getCustomer($id=false , $setThisCustomer=true){//$setThisCustomer -> se true salva il record del customer nell'oggetto $this
		$id = $id ? $id : $this->id_customer;
		if( $id>0 ){
			$q = "SELECT c.*, crg.reg_code FROM `^customers` c LEFT JOIN `^customers_reg_code` crg ON c.id = crg.id_customer WHERE c.id = {$id}";
			$Customer = $this->cn->OQ($q);
		}else{
			return false;
		}
		
		if( is_object($Customer) ){
            $q_base = "SELECT
				shi.*,
				cou.code AS country_code,
				IF( cou.name!='', cou.name, '-') AS country,
				IF( cou.iso_code_vat IS NULL, cou.code, cou.iso_code_vat ) AS code_vat,
				IF( cou.id>0, cou.cee, NULL) AS cee
			FROM
			`^customers_shipping_info` shi LEFT JOIN `^countries` cou ON shi.id_country=cou.id 
			WHERE shi.id_customer = {$id} AND shi.type='#*type*#'";

            $q = str_replace("#*type*#" , "billing" , $q_base );
			$Customer->BillingInfo = $this->cn->OQ($q);

            $q = str_replace("#*type*#" , "shipping" , $q_base);
			$Customer->ShippingInfo = $this->cn->Q($q);
			
			$Customer->sell_enable = false;
			$Customer->brands_disallowed = array();
			$Customer->brands_disallowed_str = false;
			if( $Customer->BillingInfo->id_country>0 ){
				$q = "SELECT sell_enable FROM `^countries` WHERE id = {$Customer->BillingInfo->id_country}";
				$x = $this->cn->OF($q);
				$Customer->sell_enable = $x ? true : false;
				
				$q = "SELECT id_griffe FROM `^e_griffe_countries_disallow` WHERE id_country = {$Customer->BillingInfo->id_country}";
				$brands = $this->cn->Q($q);
				foreach($brands as $b){
					$Customer->griffe_disallowed[] = $b->id_brand;
				}
				$Customer->griffe_disallowed_str = $this->cn->n>0 ? implode(",", $Customer->griffe_disallowed) : '';
			}
		}
		//echo "<pre>";print_r($Customer);echo "</pre>";

		if( $setThisCustomer ){
			$this->Customer = $Customer;
		}
		return $Customer;
	}
	
	public function signupNewsletter($email,$id_lang,$id_customer=0,$ip='',$ua='', $mc_group=null){
		$email = trim($email);
		if( !filter_var($email,FILTER_VALIDATE_EMAIL) ){
			return -1;
		}
		$q = "SELECT * FROM `^nl_subscribers` WHERE email = '{$email}'";
		$o = $this->cn->OQ($q);
		
		if( $o==-1 ){
			$id_customer_query = $id_customer>0 ? $id_customer : "NULL";
			$q = "INSERT INTO `^nl_subscribers` (email,id_lang,id_customer,`ip`,`ua`,`date_subscribe`) VALUES ('{$email}',{$id_lang},{$id_customer_query},'{$ip}','" . addslashes($ua) . "',NOW())";
			$this->cn->Q($q);
			$id = $this->cn->last_id;
		}else{
			$id = $o->id;
			if( !empty($o->date_unsubscribe) ){
				$id_customer_query = $id_customer>0 ? $id_customer : ($o->id_customer>0 ? $o->id_customer : "NULL"); 
				$q = "UPDATE `^nl_subscribers` SET date_unsubscribe = NULL, id_customer = {$id_customer_query} WHERE id = {$id}";
				$this->cn->Q($q);
			}
		}

		$apikey = $this->P('mailchimp-apikey');
		$id_list = $this->P('mailchimp-idlist',false,$id_lang);
		$opts = array();
		$mc = new Mailchimp($apikey,$opts);

		$merge_vars = array();
		$merge_vars['NAME'] = $email;
		$merge_vars['PRIVACY'] = "Accetto l'Informativa sulla privacy";

		if($mc_group){
			$merge_vars['GROUPINGS'] = array();
			for($i=0;$i<count($mc_group['name']);$i++){
				$groups = explode("," , $mc_group['groups'][$i] );
				$merge_vars['GROUPINGS'][] = array(
					'name' => $mc_group['name'][$i],
					'groups' => $groups
				);
			}
		}

		try{
			$result = $mc->call('lists/subscribe', array(
				'id'	=>	$id_list,
				'email'	=>	array('email' => $email),
				'merge_vars' => $merge_vars,
				'double_optin'	=>	false,
				'update_existing'	=>	true,
				'replace_interests'	=>	false
			));
			$mailchimp_status = 1;
		}catch(Exception $e){
			$mailchimp_status = 0;
		}
		
		$q = "UPDATE `^nl_subscribers` SET mailchimp_status = {$mailchimp_status}, mailchimp_idlist = '{$id_list}' WHERE id = {$id}";
		$this->cn->Q($q);
		
		//INVIO E-MAIL
		$mail = $this->getMailTemplate('newsletter.html');
		if( $id_customer>0 ){
			$company = $this->Customer->company;
		}else{
			$company = $this->W('user');
		}
		$mail = str_replace("#*company*#" , $company , $mail);

		$mail = str_replace("#*name*#" , $this->Customer->name , $mail);
		$mail = str_replace("#*surname*#" , $this->Customer->surname , $mail);
	
		$mail = $this->footerMail($mail);
		
		$subject = $this->W('Grazie per esserti iscritto alla nostra newsletter');
		$this->sendMail($email, null, "no-reply@accademiaweb.com","Accademia", $subject , $mail);
		unset($subject,$mail);
		
		return $id;
	}
	
	public function saveCustomer($id,$data){
		$data = array_map("trim", $data);
		$data_slashes = array_map("addslashes", $data);
		
		$q = "UPDATE `^customers` SET company='{$data_slashes['company']}', vat = '{$data_slashes['vat']}' , `name` = '{$data_slashes['name-bill']}' , `surname` = '{$data_slashes['surname-bill']}' WHERE id = {$id}";
		$this->cn->Q($q);

        $arr = array('bill'=>'billing','shipping'=>'shipping');
        foreach($arr as $suffix=>$type){
            $q = "UPDATE `^customers_shipping_info` SET
                `name` = '{$data_slashes['name-' . $suffix]}',
                `surname` = '{$data_slashes['surname-' . $suffix]}',
                id_country = {$data_slashes['id_country-' . $suffix]},
                city = '{$data_slashes['city-' . $suffix]}',
                `address` = '{$data_slashes['address-' . $suffix]}',
                `address_number` = '{$data_slashes['address_number-' . $suffix]}',
                `zip` = '{$data_slashes['zip-' . $suffix]}',
                `tel` = '{$data_slashes['tel-' . $suffix]}',
                `tel2` = '{$data_slashes['tel2-' . $suffix]}',
                `email_address` = '{$data_slashes['email-' . $suffix]}'

                WHERE
                id_customer = {$id} AND type='{$type}'
            ";
            $this->cn->Q($q);
        }
	} 
	
	public function Auth(){
        if( isset($_GET['actnum']) ){
			$actnum = trim($_GET['actnum']);
			$actnum = str_replace('"','',$actnum);
			$this->activeCustomer($actnum);
		}else if( isset($_GET["logout"])  ){
			unset($_SESSION['id_customer']);
            $this->setRememberCookie(0);//Elimina cookie
			header("Location: " . $this->Url('home') );exit;
		}

		$v = $this->isAuth();
		
		if(
            !$v
            &&
            (
                isset($_POST['login']) && isset($_POST['pw'])
                ||
                isset($_COOKIE['cst'])
            )
        ) {
            $this->Login();
		}else if($this->Page->auth && !$v ){
			//header("Location: " . $this->Url('this') . '?auth_req' );exit;
			$this->Page->auth_req = true;
		}else if($v && isset($_COOKIE['cst']) ){//Reimposta durata del cookie
            $this->setRememberCookie($_COOKIE['cst']);
        }
	}
	
	/*protected function setLogin($id_customer){
		$q = "SELECT * FROM `^customers` WHERE id = {$id_customer} AND enable=1";
		$C = $this->cn->OQ($q);
		if($C!=-1){
			$this->id_customer = $_SESSION['id_customer'] = $id_customer;
			$cst = sha1( $this->key_remember_login . $id_customer );
            $this->setRememberCookie($cst);
			return $C;
		}else{
			return false;
		}
	}*/
	
	public function isAuth(){
		if( $this->id_customer>0 ){
			$_SESSION['id_customer'] = $this->id_customer;
			return 1;
		}else if( isset($_SESSION['id_customer']) && $_SESSION['id_customer']>0 ){
			$this->id_customer = $_SESSION['id_customer'];
			return 1;
		}else if( isset($_SESSION['auth_token']) && strlen($_SESSION['auth_token'])>0 ){
			return 2;
		}else{
			return 0;
		}
	}
	
	protected $key_remember_login = '#singuiu4i$%9sk#q';
	public function Login(){
        $remember = false;
		if( isset($_POST['login']) && isset($_POST['pw'])){
			$id_user = $this->LoginCustomer($_POST['login'], $_POST['pw']);
			if( $_POST['remember']==1 ){
				$remember = true;
			}
		}else if(isset($_COOKIE['cst']) ){
			$q = "SELECT id FROM `^customers` WHERE SHA1( CONCAT('{$this->key_remember_login}', id) ) = '" . $_COOKIE['cst'] . "'";
			$id_user = $this->cn->OF($q);
			$remember = true;
		}
		
		if( $id_user>0 ){
			$this->id_customer = $_SESSION['id_customer'] = $id_user;
			if( $remember ){
				$value = sha1( $this->key_remember_login . $id_user );
			}else{
				$value = '';
			}
            $this->setRememberCookie($value);
			header("Location: " . $this->Url('products') );exit;
		}else{
			header("Location: " . $this->Url('home') );exit;
		}
	}

    protected function setRememberCookie($value){
        $expire = time();
        if( empty($value) ){ //cancella cookie
            $expire -= 3600;
        }else{
            $expire += 30*24*60*60;//30 giorni
        }

        setcookie('cst',$value,$expire,'/');
    }
	
	public function registerCustomer($data,$send_mail_reg=false){
		$ck = $this->verifyCustomerEmailReg($data['email']);
		if( $ck ){ 
			//$data = array_map("trim", $data);
			
			$reg_code = $this->newCustomerActnum();
			$q = "INSERT INTO `^customers`
			(email,pw,`enable`,`company`,`vat`,`name`,`surname`,`cf`,`created`)
			VALUES
			('{$data['email']}',SHA1('" . $this->customer_pw_key . addslashes($data['pw'])  . "'), 0, '" . addslashes($data['company']) . "', '" . addslashes($data['vat']) . "' , '" . addslashes($data['name']) . "', '" . addslashes($data['surname']) . "', '" . addslashes($data['cf']) . "' , NOW())";
			$this->cn->Q($q);
			$id_customer = $this->cn->last_id;
			$q = "INSERT INTO `^customers_reg_code` (id_customer,reg_code) VALUES ({$id_customer},'{$reg_code}')";
			$this->cn->Q($q);

			$q = "INSERT INTO `^customers_shipping_info`
			(id_customer,id_country,`type`,`city`,`address`,`address_number`,`zip`,`tel`,`tel2`)
			VALUES
			({$id_customer},{$data['id_country-bill']},'billing','" . addslashes($data['city-bill']) . "','" . addslashes($data['address-bill']) . "','" . addslashes($data['address_number-bill']) . "','" . addslashes($data['zip-bill']) . "','" . addslashes($data['tel-bill']) . "','" . addslashes($data['tel2-bill']) . "')";
			$this->cn->Q($q);
			
			$q = "INSERT INTO `^customers_shipping_info`
			(id_customer,id_country,`type`)
			VALUES
			({$id_customer},{$data['id_country-bill']},'shipping')";
			$this->cn->Q($q);
			
			if($send_mail_reg){
				$this->sendMailRegisterCustomer($id_customer);
			}

			if( isset($data['newsletter_subscribe']) && $data['newsletter_subscribe']==1){
				$mc_group = null;
				if( isset($data['mc-group-name']) && count($data['mc-group-name'])>0 ) {
					$mc_group = array(
						"name" => $data['mc-group-name'],//array di name
						"groups" => $data['mc-group-groups'] //array di groups
					);
				}
				$this->signupNewsletter($data['email'],$this->id_lang,$id_customer,$data['ip'],$data['ua'], $mc_group);
			}
			
			return $id_customer;
		}else{
			return false;	
		}
	}
	
	public function idCustomerFromSha1($id_customer_sha1){
		$q = "SELECT id FROM `^customers` WHERE SHA1(id) = '{$id_customer_sha1}'";
		return $this->cn->OF($q);
	}

	public function sendMailRegisterCustomer($id_customer){
		$C = $this->getCustomer($id_customer);
		if( !$C->enable ){
			//INVIO E-MAIL
			$mail = $this->getMailTemplate('registrazione.html');
			$mail = str_replace("#*company*#" , $C->company , $mail);				
			$mail = str_replace("#*url_attivazione*#" , $this->DomainUrl() . $this->Url('products') . "?reg_code=" . sha1(sha1($C->reg_code)), $mail);
			$mail = str_replace("#*email*#" , $C->email , $mail);
		
			$mail = $this->footerMail($mail);
			
			$subject = $this->W('Benvenuto su Accademia');
			$this->sendMail($C->email, $C->company, "no-reply@accademiaweb.com","Accademia", $subject , $mail);
			unset($C,$subject,$mail);
			return true;
		}else{
			return false;
		}
	}
	
	protected function newCustomerActnum(){
		$flag = false;
		do{
			$reg_code = $this->RandomCode(12,"n");
			$reg_code = trim($reg_code);
			$q = "SELECT id_customer FROM `^customers_reg_code` WHERE reg_code='{$reg_code}'";
			$id = $this->cn->OF($q);
			if(!$id){
				$flag = true; 
			}
		}while(!$flag);
		return $reg_code;
	}
	
	protected function newRefCode(){
		$flag = false;
		do{
			$refcode = $this->RandomCode(9,"n");
			$q = "SELECT id FROM `^customer` WHERE refcode='{$refcode}'";
			$id = $this->cn->OF($q);
			if(!$id){
				$flag = true; 
			}
		}while(!$flag);
		return $refcode;
	}
	
	public function uniqueVatNumber($vat,$id_customer=false,$id_country){//torna true se il vat number non è assegnato ad altri utenti per quel paese, altrimenti FALSE
		$id_customer = $id_customer ? $id_customer : 0;
		$q = "SELECT c.id FROM `^customers` c INNER JOIN `^customers_shipping_info` csi ON c.id = csi.id_customer AND csi.type='billing' WHERE c.`vat`='{$vat}' AND c.id != {$id_customer} AND csi.id_country = {$id_country}";
		$this->cn->Q($q);
		return $this->cn->n>0 ? false : true;
	}
	
	protected $checkVatNumber_index_SB = 0;
	public function checkVatNumber($country_code,$vat){
		$client = new SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");
		
		if($client){
		    $params = array('countryCode' => $country_code, 'vatNumber' => $vat);
		    try{
		        $r = $client->checkVat($params);
				//$this->pr($r);
		        if($r->valid == true){
		            return true;
		        } else {
		            return false;
		        }
				
				/*
				// This foreach shows every single line of the returned information
		        foreach($r as $k=>$prop){
		            echo $k . ': ' . $prop;
		        }
				*/
		    } catch(SoapFault $e) {
		    	if( $e->faultstring=="SERVER_BUSY" && $this->checkVatNumber_index_SB<100 ){
		    		$this->checkVatNumber_index_SB++;
		    		usleep( 500000 );
					return $this->checkVatNumber($country_code,$vat);
				}else{
			        if( _DEBUG ){
			        	echo 'Error, see message: '.$e->faultstring;
					}
					mail(ALERT_EMAIL,"SigmaGi error","Impossibile verificare la partita iva: {$vat}. Errore server UE: {$e->faultstring}");
				}
		    }
		} else {
		    // Connection to host not possible, europe.eu down?
		    return false;
		}
	}
	
	public function rememberCustomerPW($email){
		$customer = $this->getCustomerByMail($email);
		if($customer==-1){
			$q = "INSERT INTO `^customers_log` (id_customer,`session`,`ip`,`ua`,`action`) VALUES (0,'" . session_id() . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . $_SERVER['HTTP_USER_AGENT'] . "', 'recovery-pw')";
			$this->cn->Q($q);
			return false;
		}else{
			$new_pw = $this->RandomCode(8,'aAn');
			$q = "UPDATE `^customers` SET `pw` = SHA1('" . $this->customer_pw_key . addslashes($new_pw)  . "') WHERE id = {$customer->id}";
			$this->cn->Q($q);
			
			//INVIO E-MAIL
			if( strlen($customer->name)>0 ){
				$name = $customer->name;
			}else{
				list($name) = explode("@",$customer->email); 
			}
			$mail = file_get_contents( $this->_path->mail_lang .  'recupero_pass.html' );
			$mail = str_replace("#*name*#" , $name , $mail);
			$mail = str_replace("#*password*#" , $new_pw , $mail);
			
			$mail = $this->footerMail($mail);

			$subject = $this->W('Recupera Password');
			$this->sendMail($customer->email, $name, "no-reply@accademiaweb.com","Accademia", $subject , $mail);
			return $new_pw;
		}
	}

	public function setDataCustomer($id_customer,$type="fatturazione",$data){//$type => fatturazione|spedizione
		$_sped = $type=='fatturazione' ? '' : '_sped';
		$field_name = $type=='fatturazione' ? 'name_surname' : 'name_sped';
		
		$q = "UPDATE `^customer` SET ";
		
		$arr = array();
		if( $type=="fatturazione" && !empty($data['name_surname']) ){
			$arr[] = "`name_surname` = '" . addslashes($data['name_surname']) . "'";
		}else if($type=="spedizione" && !empty($data['name_sped']) ){
			$arr[] = "`name_sped` = '" . addslashes($data['name_sped']) . "'";
		}
		$fields = array("surname","co","address","cap","city","province","phone");
		foreach($fields as $field){
			if( !empty($data[$field . $_sped]) ){
				$arr[] = "`{$field}{$_sped}` = '" . addslashes($data[$field . $_sped]) . "'";
			}
		}
		$q .= implode(" , ",$arr);
		$q .= "	WHERE id = {$id_customer}";
		$this->cn->Q($q);
		
		return true;
	}

	public function verifyCustomerEmailReg($email){//Verifica che non ci siano altri utenti con l'indirizzo e-mail registrato
		$q = "SELECT id, enable AS activated FROM `^customers` WHERE email='{$email}'";
		$this->cn->Q($q);
		return $this->cn->n==0 ? true : false;
	}

	public function verifyCustomerPW($id_customer,$pw){
		$pw = $this->customer_pw_key . addslashes($pw);
		$q = "SELECT id FROM `^customers` WHERE id={$id_customer} AND `pw` = SHA1('{$pw}')";
		$x = $this->cn->OQ($q);
		return $x==-1 ? false : true;
	}

	public function setCustomerNameAndPassword($id_customer , $name=false , $pw=false){
		$q = "UPDATE `^customers` SET ";
		
		$arr = array();
		if( $name ){
			$arr[] = "`name` = '" . addslashes($name) . "'";
		}
		if( $pw ){
			$arr[] = "`pw` = SHA1('" . $this->customer_pw_key . addslashes($pw) . "')";
		}
		$q .= implode(" , ",$arr);
		$q .= " WHERE id = {$id_customer}";
		$this->cn->Q($q);
	}
	
	public function getCustomerByMail($email){
		$q = "SELECT * FROM `^customers` WHERE email='{$email}'";
		$customer = $this->cn->OQ($q);
		return $customer;
	}
	
	public function activeCustomer($reg_code_sha1){
		$q = "SELECT id_customer FROM `^customers_reg_code` WHERE SHA1( SHA1( reg_code ) ) = '{$reg_code_sha1}'";
		$id_customer = $this->cn->OF($q);

		if( $id_customer>0 ){
			$q = "UPDATE `^customers` SET active_data = NOW(), enable=1 WHERE id = {$id_customer}";
			$this->cn->Q($q);
			$q = "DELETE FROM `^customers_reg_code` WHERE id_customer = {$id_customer}";
			$this->cn->Q($q);
			
			$this->id_customer = $_SESSION['id_customer'] = $id_customer;
			return $id_customer;
		}else{
			return false;
		}
	}
		
	public function LoginCustomer($email,$pw){
		$q = "SELECT * FROM `^customers` WHERE email='{$email}' ORDER BY id DESC LIMIT 1";
		$user = $this->cn->OQ($q);
		if($user==-1){
			return -1;//Login non valido
		}else{
			if( $user->pw==sha1( $this->customer_pw_key . $pw ) ){
				if( $user->enable ){
					$q = "INSERT INTO `^customers_log` (id_customer,`session`,`ip`,`ua`,`action`) VALUES ({$user->id},'" . session_id() . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . $_SERVER['HTTP_USER_AGENT'] . "', 'login')";
					$this->cn->Q($q);
					
					return $user->id;
				}else{
					return -2;//Account non attivo
				}
			}else{
				return -1;//Login non valido
			}
		}
	}
	
	public function NL_isSubscribed($email){
		$mc = new Mailchimp( mailchimp_key , array() );
		
		try{
			$flag = false;
			do{
				$result = $mc->call('lists/members',
					array(
						'id' => mailchimp_list_id,
						'status' => 'subscribed',
						'opts' => array(
							'start' => 0,
							'limit' => 100
						)
					)
				);
				if( count($data)>0 ){
					foreach( $result->data as $row){
						if($data['email']==$email){//E-mail trovata: è iscritto
							return true;
						}
					}
				}else{
					$flag = true;
				}
			}while(!$flag);
			return false;//E-mail non trovata: non è iscritto
		}catch(Exception $e){
			return false;
		}
	}
	
	public function NL_subscribe($email , $groupings = array() ){
		$mc = new Mailchimp( mailchimp_key , array() );
		
		try{
			$result = $mc->call('lists/subscribe',
				array(
					'id' => mailchimp_list_id,
					'email' => array('email'=>$email),
					'merge_vars'        => array(
						//'MERGE2' => $_POST['name'] // MERGE name from list settings
						// there MERGE fields must be set if required in list settings
						"groupings" => $groupings
					),
					'double_optin'      => false,
					'update_existing'   => true,
					'replace_interests' => false
				)
			);

			if( !empty($result['email']) ){
				return true;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	public function NL_unsubscribe($email){
		$mc = new Mailchimp( mailchimp_key , array() );
		
		try{
			$result = $mc->call('lists/unsubscribe',
				array(
					'id' => mailchimp_list_id,
					'email' => array('email'=>$email),
					'delete_member' => true,
					'send_goodbye' => false,
					'send_notify' => false
				)
			);

			if( $result["complete"]==1 ){
				return true;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	public function DisableAccount($id_customer){
		$q = "UPDATE `^customer` SET `active`=0 WHERE id = {$id_customer}";
		$this->cn->Q($q);
		return true;
	}
}