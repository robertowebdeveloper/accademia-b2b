<?php
class Synch{
    private $records_4_page = 100;
    private $frontier = null;

    public function __construct(){
        set_time_limit(0);
        ini_set('memory_limit','256M');

        global $cn;
        global $NE_db_data , $NE_db_data_frontiera;

        if( empty($NE_db_data_frontiera) ){
            include( __DIR__ . '/../config.inc.php' );
        }

        $this->cn = &$cn;

        $this->frontier = new NE_mysql(true,$NE_db_data_frontiera);
        $this->frontier->query_error = true;
    }

    public function __destruct(){
        unset($this->frontier);
    }

    private function json($arr){
        return json_decode($arr , false);
    }

    private function O($arr){
        return json_decode( json_encode($arr) , false);
    }

    public function ExecutionTime( $w='start' ){
        if($w=='start'){
            $this->et_start = microtime(true);
        }else if($w=='end'){
            $this->et_end = microtime(true);

            $time = $this->et_end - $this->et_start;

            return gmdate("H:i:s", $time);
        }
    }

    private function nullField($values){
        if( !is_array($values) ){
            $new = array();
            $new[] = $values;
            $values = $new;
            unset($new);
        }

        $return = array();
        foreach($values as $v){
            $return[] = empty($v) ? "NULL" : "'{$v}'";
        }
        return $return;
    }

    public function Articoli(){
        $q = "SELECT * FROM Ana_Articoli WHERE 1 ORDER BY ID_Art ASC";
        $ris = $this->frontier->Q($q,false);
        $nRis = $this->frontier->n;
        //echo $nRis . "<hr>";

        $q = "UPDATE `^ext_articoli` SET status_synch = 1 WHERE status_synch=0 AND deleted IS NULL";
        $this->cn->Q($q);

        $iList = 0;
        if( $nRis>0 ) {
            while( $row = $this->frontier->R($ris) ){
                $rs = $this->O( array_map('addslashes', $row) );
                //print_r($rs);exit;

                if( $rs->ID_Art>0 ) {
                    list($art_alternativo, $art_cat_stat, $art_linea, $art_marchio, $art_catalogo, $art_umv, $art_confezione, $art_tipo, $verifica, $annullato, $promozione, $club) = $this->nullField(array(
                        $rs->Art_Alternativo, $rs->Art_Cat_Stat, $rs->Art_Linea, $rs->Art_Marchio, $rs->Art_Catalogo, $rs->Art_Umv, $rs->Art_Confezione, $rs->Art_Tipo, $rs->Art_Verifica, $rs->Art_Annullato, $rs->Art_Promozione, $rs->Art_Club
                    ));
                    $q = "INSERT INTO `^ext_articoli`
                      (id_art , codice , descrizione , art_alternativo , art_cat_stat , prezzo , art_campagna , art_linea , art_marchio , art_catalogo , art_umv , art_confezione , art_tipo , giacenza , verifica , annullato , pvp , dimensioni , ctrl_inner , promozione , sconto , punti , club , data_synch , status_synch )
                      VALUES
                      ({$rs->ID_Art} , '{$rs->Art_Codice}' , '{$rs->Art_Descrizione}' , {$art_alternativo} , {$art_cat_stat} , '{$rs->Art_Listino}' ,  '{$rs->Art_Campagna}' , {$art_linea} , {$art_marchio} , {$art_catalogo} , {$art_umv} , {$art_confezione} , {$art_tipo}, {$rs->Art_Giacenza} , {$verifica} , {$annullato} , {$rs->Art_Pvp} , '{$rs->Art_Dimensioni}' , {$rs->Art_Ctrl_Inner} , {$promozione} , '{$rs->Art_Sconto}' , '{$rs->Art_Punti}' , {$club} , NOW() , 0 )
                      ON DUPLICATE KEY
                        UPDATE
                        codice = '{$rs->Art_Codice}' , descrizione = '{$rs->Art_Descrizione}' , art_alternativo = {$art_alternativo} , art_cat_stat = {$art_cat_stat} , prezzo = '{$rs->Art_Listino}' , art_campagna = '{$rs->Art_Campagna}' , art_linea = {$art_linea} , art_marchio = {$art_marchio} , art_catalogo = {$art_catalogo} , art_umv = {$art_umv} , art_confezione = {$art_confezione} , art_tipo = {$art_tipo} , giacenza = {$rs->Art_Giacenza} , verifica = {$verifica} , annullato = {$annullato} , pvp = {$rs->Art_Pvp} , dimensioni = '{$rs->Art_Dimensioni}' , ctrl_inner = '{$rs->Art_Ctrl_Inner}' , promozione = {$promozione} , sconto = '{$rs->Art_Sconto}' , punti = '{$rs->Art_Punti}' , club = {$club} , data_synch = NOW() , status_synch = 0
                      ";
                    $this->cn->Q($q);
                    /*if($rs->ID_Art==73360811){
                        echo "<pre>";print_r($row);
                        echo "<hr>";print_r($rs);
                        echo "ARTLINEA: $rs->Art_Linea {$art_linea}<br>";
                        echo $q;
                        $this->cn->Q($q);
                        exit;
                    }*/
                    //echo $q."<hr>";ob_flush();
                    $iList++;
                }
            }
            $this->frontier->F($ris);
        }
        echo "IMPORTATI: {$iList}";
    }

    public function Categorie_Articoli(){
        $q = "SELECT * FROM Ana_Categorie_Articoli WHERE 1";
        $ris = $this->frontier->Q($q, false);

        while ($row = $this->frontier->R($ris)) {
            $rs = $this->O($row);
            if( !empty($rs->CSta_Codice) ) {
                $q = "INSERT INTO `^ext_categorie_articoli` (codice,descrizione) VALUES ('{$rs->CSta_Codice}' , '" . addslashes($rs->CSta_Descrizione) . "')
                    ON DUPLICATE KEY
                    UPDATE descrizione = '" . addslashes($rs->CSta_Descrizione) . "'
                ";
                $this->cn->Q($q);
            }
        }
        $this->frontier->F($ris);
    }
    public function Famiglie(){//NON UTILIZZATO
        $q = "SELECT * FROM Ana_Cli_Famiglie WHERE 1";
        $ris = $this->frontier->Q($q, false);

        while ($row = $this->frontier->R($ris)) {
            $rs = $this->O($row);
            if( !empty($rs->Cod_Famiglia) ) {
                $q = "INSERT INTO `^ext_categorie_articoli` (codice,descrizione) VALUES ('{$rs->Cod_Famiglia}' , '" . addslashes($rs->Des_Famiglia) . "')
                    ON DUPLICATE KEY
                    UPDATE descrizione = '" . addslashes($rs->Des_Famiglia) . "'
                ";
                $this->cn->Q($q);
            }
        }
        $this->frontier->F($ris);
    }
    public function Campagne(){
        $q = "SELECT * FROM Ana_Art_Campagne WHERE 1";
        $ris = $this->frontier->Q($q, false);

        while ($row = $this->frontier->R($ris)) {
            $rs = $this->O( array_map('addslashes',$row) );
            if( !empty($rs->Cod_Campagna) ) {
                $q = "INSERT INTO `^ext_campagne` (codice,descrizione,codice_group,ord_infinity,descr_scadenza,data_uscita,ctrl_inner,data_consegna,codice_ap1,codice_ap2,descr_pvp,codice_coll)
                      VALUES
                      ('{$rs->Cod_Campagna}' , '{$rs->Des_Campagna}' , '{$rs->Cod_RagCamp}' , '{$rs->Ord_Infinity}' , '{$rs->Des_Scadenza}' , " . $this->toDate($rs->DataUscita) . " , {$rs->Ctrl_Inner} , " . $this->toDate($rs->Data_ConsegnaCamp) . " , '{$rs->Cod_CampagnaAP1}' , '{$rs->Cod_CampagnaAP2}' , '{$rs->Des_PVP}' , '{$rs->Cod_CampagnaColl}')

                      ON DUPLICATE KEY
                      UPDATE descrizione = '{$rs->Des_Campagna}' , codice_group = '{$rs->Cod_RagCamp}' , ord_infinity = '{$rs->Ord_Infinity}' , descr_scadenza = '{$rs->Des_Scadenza}' , data_uscita = " . $this->toDate($rs->DataUscita) . " , ctrl_inner = {$rs->Ctrl_Inner} , data_consegna = " . $this->toDate($rs->Data_ConsegnaCamp) . " , codice_ap1 = '{$rs->Cod_CampagnaAP1}' , codice_ap2 = '{$rs->Cod_CampagnaAP2}' , descr_pvp = '{$rs->Des_PVP}' , codice_coll = '{$rs->Cod_CampagnaColl}'
                ";
                $this->cn->Q($q);
            }
        }
        $this->frontier->F($ris);
    }
    public function Linee(){
        $q = "SELECT * FROM Ana_Art_Linea WHERE 1";
        $ris = $this->frontier->Q($q, false);

        while ($row = $this->frontier->R($ris)) {
            $rs = $this->O($row);
            if( !empty($rs->Cod_Linea) ) {
                $q = "INSERT INTO `^ext_linea` (codice,descrizione) VALUES ('{$rs->Cod_Linea}' , '" . addslashes($rs->Des_Linea) . "')
                    ON DUPLICATE KEY
                    UPDATE descrizione = '" . addslashes($rs->Des_Linea) . "'
                ";
                $this->cn->Q($q);
            }
        }
        $this->frontier->F($ris);
    }
    public function Marchi(){
        $q = "SELECT * FROM Ana_Art_Marchio WHERE 1";
        $ris = $this->frontier->Q($q, false);

        while ($row = $this->frontier->R($ris)) {
            $rs = $this->O($row);
            if( !empty($rs->Cod_Marchio) ) {
                $q = "INSERT INTO `^ext_marchi` (codice,descrizione) VALUES ('{$rs->Cod_Marchio}' , '" . addslashes($rs->Des_Marchio) . "')
                    ON DUPLICATE KEY
                    UPDATE descrizione = '" . addslashes($rs->Des_Marchio) . "'
                ";
                $this->cn->Q($q);
            }
        }
        $this->frontier->F($ris);
    }

    protected function toDate($date,$null=true){
        if( empty($date) ) {
            if ($null) {
                $ret = "NULL";
            } else {
                $ret = "NOW()";
            }
        }else {
            list($d, $m, $Y) = explode("/", $date);
            $d_txt = str_pad($d,2,"0",STR_PAD_LEFT);
            $m_txt = str_pad($m,2,"0",STR_PAD_LEFT);
            $ret = "'{$Y}-{$m_txt}-{$d_txt}'";
        }
        return $ret;
    }
}
?>