<?php
class Orders extends Products{
    public $id_cart_ses=NULL;
    public function Order_addBasket($idart,$qty="+1"){
        unset($this->Order_getBasket_tmp);
        $ses = session_id();

        $this->id_basket = $this->Order_getIDBasket();
        if( !$this->id_basket ){
            $id_customer = $this->id_customer>0 ? $this->id_customer : "NULL";
            $q = "INSERT INTO `^basket` (`session`,id_customer,`date`,`created`) VALUES ('{$ses}',{$id_customer},NOW(),NOW() )";
            $this->cn->Q($q);
            $this->id_basket = $this->cn->last_id;
        }else{
            $q = "UPDATE `^basket` SET updated = NOW() WHERE id = {$this->id_basket}";
            $this->cn->Q($q);
        }

        if( !($this->id_basket>0 ) ){
            return false;
        }

        $Prod = $this->Product( $idart );
        $q = "SELECT * FROM `^basket_row` WHERE id_basket = {$this->id_basket} AND idart = {$idart} AND price='{$Prod->prezzo}' LIMIT 1";
        $basket_row = $this->cn->OQ($q);
        if( $basket_row!="-1" ){
            $ret = $this->Order_updateQty($this->id_basket, $idart ,$qty);
            if($ret<0){
                return $ret;
            }
        }else{//Non è presente il prodotto nel carrello
            $qty = intval($qty);

            $price_full = $Prod->prezzo_full>0 ? "'{$Prod->prezzo_full}'" : "NULL";
            $sconto = $Prod->sconto>0 ? $Prod->sconto : 0;
            $q = "INSERT INTO `^basket_row` (id_basket,idart,qty,price,price_full,sconto) VALUES ({$this->id_basket},{$idart},{$qty},'{$Prod->prezzo}' , {$price_full} , {$sconto} )";
            $this->cn->Q($q);
        }

        return true;
    }

    public function Order_setBasketCustomer(){
        if( $this->id_customer>0 ) {
            $ses = session_id();
            $id_basket = $this->Order_getIDBasket();

            $q = "UPDATE `^basket` SET id_customer = {$this->id_customer} WHERE `session`='{$ses}' AND id_customer IS NULL";
            $this->cn->Q($q);
        }else{
            return false;
        }
    }

    public function Order_updateQty($id_basket,$idart,$qty){ //$qty può essere un numero oppure indicato con la forma "+1", "+4" e "-1", "-4" per aggiungere o togliere prodotti
        unset($this->Order_getBasket_tmp);
        if( $qty<=0 ){
            $this->Order_removeBasket($id_basket,$id_ps);
        }

        $q = "SELECT * FROM `^basket_row` WHERE id_basket = {$id_basket} AND idart = {$idart} LIMIT 1";
        $basket_row = $this->cn->OQ($q);
        $actual_qty = $basket_row->qty;

        $q = "UPDATE `^basket_row` SET qty=";

        $first_char = substr($qty,0,1);
        if( $first_char=="+" || $first_char=="-"){
            $number = intval( substr($qty,1) );

            if( $first_char=='-' ){
                $new_qty = $actual_qty-$number;
                if( $new_qty<=0 ){
                    $this->Order_removeBasket($id_basket,$idart);
                    return true;
                }
            }else if($first_char=="+"){
                $new_qty = $actual_qty+$number;
            }

            $q .= "qty" . $qty;

        }else{
            $new_qty = $qty;
            $q .= $qty;
        }

        $q .= " WHERE id_basket={$id_basket} AND idart = {$idart}";
        if( $new_qty<=0 ){
            $this->Order_removeBasket($id_basket,$idart);
            return true;
        }else{
            $this->cn->Q($q);
            return true;
        }
    }

    public function Order_removeBasket($id_basket,$idart){
        unset($this->Order_getBasket_tmp);
        $q = "DELETE FROM `^basket_row` WHERE id_basket = {$id_basket} AND idart = {$idart}";
        $this->cn->Q($q);
        return $q;
    }

    public function Order_getBasket($id_basket=false,$cache_on=true){
        if( !isset($this->Order_getBasket_tmp) || !$cache_on ){
            $basket_lifetime = $this->P('basket_lifetime');
            $giorni_reso = $this->P('giorni_reso');

            $q = "SELECT *, IF( `session` IS NULL AND DATE_ADD(`date`, INTERVAL {$giorni_reso} DAY)>NOW() , 1 , 0 ) AS return_available
            FROM `^basket` WHERE 1";
            if( $id_basket>0 ){ //Ordine
                $q .= " AND id = {$id_basket}";
            }else{
                $ses = session_id();
                $q .= " AND `updated` > DATE_ADD(NOW(), INTERVAL -{$basket_lifetime} MINUTE) AND `session`='{$ses}' ORDER BY `updated` DESC, `created` DESC LIMIT 1";
            }
            $Cart = $this->cn->OQ($q);

            if( $Cart=="-1" ){
                $id_recover = $this->Order_recoverBasket();
                if( $id_recover>0 ){
                    $q = "SELECT * FROM `^basket` WHERE id = {$id_recover}";
                    $Cart = $this->cn->OQ($q);
                    $setCart = true;
                }else{
                    $Cart = new stdClass();
                    $Cart->list = array();
                    $Cart->test = 0;
                    $setCart = false;
                }
            }else{
                $setCart = true;
            }

            if( $setCart ){
                $cart_test = 0;
                $q = "SELECT * FROM `^basket_row` WHERE id_basket = {$Cart->id}";
                $Cart->list = $this->cn->Q($q);

                $Cart->totPrice = 0;//Totale del carrello esclusa tasse e sconti
                $Cart->totQty = 0;
                $opts_prod = new stdClass();
                $opts_prod->all=true;//Prende anche prodotti eliminati o disattivi
                for($i=0;$i<count($Cart->list);$i++){
                    $item = $Cart->list[$i];
                    $price = $item->price * $item->qty;

                    $Cart->list[$i]->totPriceRow = $price;
                    $Cart->list[$i]->Product = $this->Product( $Cart->list[$i]->idart, array("no-status"=>1) );
                    $Cart->totPrice += $price;
                    $Cart->totQty += $item->qty;

                    if( $Cart->list[$i]->Product->brand_test ){
                        $cart_test = 1;
                    }
                }

                if( $cart_test != $Cart->test ){//Esegue la query solo se necessario
                    $q = "UPDATE `^basket` SET `test` = {$cart_test} WHERE id = {$Cart->id}";
                    $this->cn->Q( $q );
                    $Cart->test = $cart_test;
                }

                //BUONO
                $Cart->buono_dati = false;
                $Cart->buono_cost = 0;
                $Cart->buono_cost_txt = '-';
                if( $Cart->id_coupon>0 ){
                    $Cart->buono_dati = $this->Order_getCoupon( $Cart->id_coupon );
                    $Cart->buono_cost = -round( $Cart->totPrice * $Cart->buono_dati->discount / 100, 2 );
                    $Cart->buono_cost_txt = '&euro; ' . $this->Money($Cart->buono_cost);
                }

                //TOTALE
                $Cart->totPrice_txt = '&euro; ' . $this->Money($Cart->totPrice);
                $Cart->totBasket = $Cart->totPrice + $Cart->buono_cost;
                $Cart->totBasket = $Cart->totBasket<0 ? 0 : $Cart->totBasket;
                $Cart->totBasketIncome = $Cart->totBasket; //Prezzo totale, escluso iva
                $Cart->totBasketIncome_txt = '&euro; ' . $this->Money($Cart->totBasketIncome);

                //CUSTOMER
                if( $Cart->id_customer>0 ) {
                    $Customer = $this->getCustomer($Cart->id_customer,false);
                }

                //VAT
                //$vat = $this->Vat('vat',$Customer->BillingInfo->id_country);
                $vat = $this->Vat('vat', 110);
                $Cart->vat = $vat;
                if( $vat ){ //Scorporare iva
                    $fattore_scorporo = 1 + $vat/100;
                    $Cart->imponibile = round( $Cart->totBasket / $fattore_scorporo , 2);
                    $Cart->vat_cost = $Cart->totBasket - $Cart->imponibile;
                    //$Cart->totBasket += $Cart->vat_cost;
                }else{
                    $Cart->vat_cost = 0;
                }
                $Cart->vat_cost_txt = '&euro; ' . $this->Money($Cart->vat_cost);

                //Eventuale anticipo
                $Cart->advance_payment = $this->P('advance_payment');
                $p = round( $Cart->advance_payment*$Cart->totBasket/100 , 2);
                $Cart->advance_payment_cost = $p;
                $Cart->advance_payment_cost_txt = '&euro; ' . $this->Money( $p );

                $Cart->totBasket_txt = '&euro; ' . $this->Money($Cart->totBasket);
            }
            $this->Order_getBasket_tmp = $Cart;
            unset($Cart);
        }

        return $this->Order_getBasket_tmp;
    }

    protected function Order_recoverBasket(){
        //Prova a recuperare un carrello di almeno 15 giorni
        if( isset($_SESSION['id_customer']) && $_SESSION['id_customer']>0){
            $ses = session_id();
            $q = "SELECT id FROM `^basket` WHERE id_customer = {$_SESSION['id_customer']} AND `session` IS NOT NULL AND `updated` > DATE_ADD(NOW(), INTERVAL -15 DAY) AND `bank_status` IS NULL ORDER BY `updated` DESC LIMIT 1";
            $id_recover = $this->cn->OF($q);
            if( $id_recover>0 ){
                $q = "UPDATE `^basket` SET `session` = '{$ses}', `updated` = NOW() WHERE id = {$id_recover}";
                $this->cn->Q($q);

                $q = "SELECT * FROM `^basket_row` WHERE id_basket = {$id_recover}";
                $list_art = $this->cn->Q($q);
                //VERIFICA LA DISPONIBILITA' DEI SINGOLI PRODOTTI
                foreach($list_art as $art){
                    $qty = $this->Qty($art->idart,true);
                    if( $art->giacenza > $qty ){
                        $qty = $qty<0 ? 0 : $qty;
                        $q = "UPDATE `^basket_row` SET qty = {$qty}, qty_changed=1 WHERE id_basket = {$art->id_basket} AND idart = {$art->idart}";
                        $this->cn->Q( $q );
                    }
                }

                return $id_recover;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function Order_saveOrderData($id_basket,$data){
        $data = array_map("trim", $data);
        $data_slashes = array_map("addslashes", $data);

        $shipping_order_info = array();
        $arr = array("name","surname","id_country","city","address","address_number","zip","tel","tel2","email");
        foreach($arr as $r){
            $shipping_order_info[$r] = htmlentities( $_POST[$r . "-shipping" ] );
        }
        $shipping_order_info = json_encode($shipping_order_info);

        $Basket = $this->Order_getBasket($id_basket);

        $this->saveCustomer($Basket->id_customer, $data);

        $vat_perc = $data['vat_perc']>0 ? $data['vat_perc'] : 0;
        $vat_cost = $data['vat_cost']>0 ? $data['vat_cost'] : 0;
        $q = "UPDATE `^basket` SET
			id_lang = {$this->id_lang},
			company = '{$data_slashes['company']}',
			`name` = '{$data_slashes['name-bill']}',
			`surname` = '{$data_slashes['surname-bill']}',
			vat_company = '{$data_slashes['vat']}',
			vat_perc = {$data['vat_perc']},
			vat_cost = {$data['vat_cost']},
			shipping_data = '" . str_replace("'","\'",$shipping_order_info) . "',
			`payment` = '{$data_slashes['payment']}'

			WHERE id = {$id_basket}";
        $this->cn->Q($q);
        mail('parrella@inol3.com','sottosotto',$q);
    }

    public function Order_IntegrityBasket($id_basket){
        $q = "SELECT * FROM `^basket` WHERE id = {$id_basket}";
        $B = $this->cn->OQ($q);
        if( $B->id_customer>0 &&
            $B->id_lang>0 &&
            //strlen($B->company)>0 &&
            strlen($B->name)>0 &&
            strlen($B->surname)>0 &&
            !empty($B->payment) &&
            //($B->advance_payment==0 || ($B->advance_payment==1 && $B->advance_payment_perc>0 && $B->advance_payment_cost) ) &&
            ($B->vat_perc==0 || ($B->vat_perc>0 && $B->vat_cost>0) ) &&
            !empty($B->shipping_data)
        ){
            $ck = true;
        }else{
            $ck = false;
        }
        return $ck;
    }

    public function Order_codeFromId($id_order){
        $q = "SELECT *, DATE_FORMAT(`date`, '%y') AS y FROM `^basket` WHERE id = {$id_order}";
        $order = $this->cn->OQ($q);
        $id_order_pad = str_pad($id_order,7,"0",STR_PAD_LEFT);
        $prefix = $order->surname . $order->company;
        if( strlen($prefix)<2 ){
            $prefix = md5($id_order);
        }
        $prefix = strtoupper( substr( $prefix,0,2) );

        $code = "{$prefix}.{$id_order_pad}/{$order->y}";
        unset($q,$order,$id_order_pad,$prefix);
        return $code;
    }

    public function Order_getIDBasket(){
        $ses = session_id();
        $basket_lifetime = $this->P('basket_lifetime');

        $q = "SELECT id FROM `^basket` WHERE `session`='{$ses}' AND id_customer = {$_SESSION['id_customer']} AND `updated` > DATE_ADD(NOW(), INTERVAL -{$basket_lifetime} HOUR) ORDER BY `updated` DESC LIMIT 1";
        $q = "SELECT id FROM `^basket` WHERE `session`='{$ses}'";
        $q .= $this->id_customer>0 ? " AND (id_customer = {$this->id_customer} OR id_customer IS NULL)" : "";
        $q .= " AND `updated` > DATE_ADD(NOW(), INTERVAL -{$basket_lifetime} HOUR) ORDER BY `updated` DESC LIMIT 1";

        $id = $this->cn->OF($q);

        if( !$id ){
            $id_tmp = $this->Order_recoverBasket();
            $id = $id_tmp>0 ? $id_tmp : false;
        }

        return $id;
    }

    protected function Order_parseSAM( $str ){
        $str = str_replace('"', "'", $str);
        $str = str_replace("\t","",$str);
        //$str = utf8_decode( $str );

        return $str;
    }

    public function Order_getOrderSAM( $sam_status=1 ){
        $q = "SELECT b.*, c.email FROM `^basket` AS b INNER JOIN `^customers` AS c ON b.id_customer = c.id WHERE b.sam_status = {$sam_status} AND b.`session` IS NULL";
        $list = $this->cn->Q($q);
        return $list;
    }

    public function Order_setCustomerSamId( $id_customer , $sam_id ){
        $q = "UPDATE `^customers` SET sam_id = {$sam_id} WHERE id = {$id_customer}";
        $this->cn->Q( $q );
    }

    public function Order_setOrderSAM( $id_basket , $print_xml = false ){
        /*
         * Questa funziona si occupa di generare i 2 XML necessari all'esportazione dei dati verso SAM
         * Nel file /services/check_order_to_xml.php vi sono le chiamate necessarie al cronjob che verifica gli ordini com SAM_STATUS=1 ed esegue la verifica dell'esistenza del cliente
         */
        $_path = __DIR__ . "/../../../services/xml_ordini/";
        $_files_to_upload = array();

        require_once( __DIR__ . '/Synch.class.php');
        $Synch = new Synch();

        $Cart = $this->Order_getBasket($id_basket);
        //$this->pr($Cart);
        $shipping_data = json_decode($Cart->shipping_data,false);
        $Customer = $this->getCustomer( $Cart->id_customer );
        //$this->pr($Customer);exit;

        //Nel caso lo status sia 2 (ordine già processato) ma si sta richiamando la funzione significa che si vuole ricaricare lo XML, quindi viene riportato a 0
        if( $Cart->sam_status==2 ){
            $Cart->sam_status = 0;
        }

        //1) CERCO CLIENTE
        $CODPERSONA = ''; //rimane vuoto se non trova il cliente
        $sam_id = false;
        if ($Customer->sam_id > 0) {
            $CODPERSONA = $sam_id = $Customer->sam_id;
        } else {//Cerco cliente
            $sam_id = $Synch->getCliente('email', $Customer->email);
            if (!$sam_id) {
                $sam_id = $Synch->getCliente('name', $Customer->name, $Customer->surname);
            }

            $CODPERSONA = $sam_id > 0 ? $sam_id : '';
        }

        if($CODPERSONA){
            $CODPERSONA = trim($CODPERSONA);
        }

        //2) GENERO XML CLIENTE
        if($Cart->sam_status==0){
            //GENERO XML CLIENTE
            $xml_customer = array();
            $xml_customer[] = '<?xml version="1.0" encoding="UTF-8"?>';
            $xml_customer[] = '<CSXRoot  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation=".\CSXml.xsd">';
            $xml_customer[] = '
                <CSXInfo>
                    <CSXDataGen>' . date("d/m/Y") . '</CSXDataGen>
                    <CSXAuthor>Sotto Sotto</CSXAuthor>
                </CSXInfo>';
            $xml_customer[] = '<CSXCliente
        ISCLI="-1"
        AggContatto="-1"
        INIRAP=""
        IDPERSONA=""
        CODPERSONA="' . $CODPERSONA . '"
        IDCONS=""
        CODCONS="PA"
        IDDIPEND=""
        CODDIPEND=""
        IDIVA=""
        CODIVA=""
        IDANAPAG=""
        CODANAPAG="CC"
        IDCAT=""
        CODCAT=""
        IDAGENTE=""
        CODAGENTE=""
        IDVALUTA=""
        CODVALUTA=""
        IDVETTORE=""
        CODVETTORE=""
        CCCLI=""
        FATRAG=""
        IDMTRASP=""
        CODMTRASP="V"
        SOSPIVA=""
        IDCONTROP=""
        CODCONTROP=""
        IDNSBANCA=""
        CODNSBANCA=""
        IDTRIBUTO=""
        CODTRIBUTO=""
        IDCLIFATT=""
        CODCLIFATT=""
        CODEST=""
        STATIS=""
        INTRA=""
        CSXTipoOp=""
        IDBANCA=""
        CODBANCA=""
        CreaPDC="1"
        Mastro="12"
        Gruppo="12"
        DESCR1="' . $this->Order_parseSAM($Customer->BillingInfo->name . " " . $Customer->BillingInfo->surname) . '"
        DESCR2=""
        INDIRI="' . $this->Order_parseSAM($Customer->BillingInfo->address . ", " . $Customer->BillingInfo->address_number) . '"
        CAP="' . $this->Order_parseSAM($Customer->BillingInfo->zip) . '"
        LOCALITA="' . $this->Order_parseSAM($Customer->BillingInfo->city) . '"
        PROV="' . $this->Order_parseSAM($Customer->BillingInfo->province) . '"
        IDZONA=""
        CODZONA=""
        IDNAZIONI=""
        CODNAZIONI="' . $Customer->BillingInfo->country_code . '"
        ISDIP=""
        TEL="' . $Customer->BillingInfo->tel . '"
        FAX="' . $Customer->BillingInfo->fax . '"
        PARIVA="' . $Customer->vat . '"
        CODFIS="' . $Customer->cf . '"
        EMAIL="' . $Customer->email . '"
        INTERNET="' . $Customer->BillingInfo->web . '"
        DATINS="value"
        NOME="' . $Customer->BillingInfo->name . '"
        COGNOME="' . $Customer->BillingInfo->surname . '"
        LOCNASC=""
        PROVNASC=""
        DATANASC=""
        SESSO=""
        TIPO=""
        TIPSOC=""
        NOTE="' . $Customer->BillingInfo->note . '"
    />
    ';
            $xml_customer[] = '</CSXRoot>';

            if (!file_put_contents($_path . 'cliente' . $Customer->id . '.xml', implode("\r\n", $xml_customer))) {
                $this->sendMail('parrella@inol3.com', 'no-reply@sottosotto.it', 'Errore generazione file cliente ' . $Customer->id . ' su Ordine ' . $id_basket, '');
            }else{
                if( !$sam_id ) {//mette il record a sam_status = 1 solo se l'utente non ha $sam_id=1
                    $q = "UPDATE `^basket` SET sam_send = NOW(), sam_status=1 WHERE id = {$id_basket}";
                    $this->cn->Q($q);
                }
                $Cart->sam_status = 1;
            }
        }


        //3) GENERO XML ORDINE
        if( $Cart->sam_status==1 && $sam_id ){
            $Cart->shipping_data_obj = json_decode( $Cart->shipping_data , false);
            $xml_order = array();
            $xml_order[] = '<?xml version="1.0" encoding="UTF-8"?>';
            $xml_order[] = '<CSXRoot  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation=".\CSXml.xsd">';
            $xml_order[] = '<CSXInfo>
                <CSXDataGen>' . date("d/m/Y", strtotime($Cart->date)) . '</CSXDataGen>
                <CSXAuthor>Sotto Sotto</CSXAuthor>
            </CSXInfo>';

            $xml_order[] = '<CSXOV>';

            switch ($Cart->payment) {
                case 'paypal':
                    $codcodpag = 'PP';
                    break;
                case 'cc':
                    $codcodpag = 'CC';
                    break;
                case 'cs':
                    $codcodpag = 'CS';
                    break;
                default:
                    $codcodpag = '';
                    break;
            }

            //SPEINC => Spese addizionali?
            $speinc = 0;
            //SPETRA => Spese trasporto
            $spetra = 0;

            $xml_order[] = '<CSXTestaOV IDTIPIDOC="65" ANNDOC="' . date("Y", strtotime($Cart->date)) . '" DATDOC="' . date("Ymd", strtotime($Cart->date)) . '" STATO="O" CODCLIDEST="' . $CODPERSONA . '" CODCLIENTE="' . $CODPERSONA .'" CODCAUMOV="WEB"
            CODIVA="" CODCODPAG="' . $codcodpag . '" CODVALUTA="EUR" CAMBIO="1" ORDCLI="' . $this->Order_codeFromId( $Cart->id ) . '" DATORDCLI="' . date("Ymd", strtotime($Cart->date)) . '"
            NOTA="' . $this->Order_parseSAM($Cart->note) . '" SPEINC="' . number_format($speinc, 2, ",", '') . '" SPETRA="' . number_format($spetra, 2, ",", '') . '" SPEVAR="" CODPORTO="PF" CODMTRASP="V" CODVETTORE="302408" NOTE="" DescrDest="Spedizione" IndDest="' . $Cart->shipping_data_obj->address . '" CapDest="' . $Cart->shipping_data_obj->zip . '" LocDest="' . $Cart->shipping_data_obj->city . '" ComuneDest="' . $Cart->shipping_data_obj->city . '" ProvDest="" CodNazDest="" />';

            $xml_order[] = '<CSXRIGHE>';

            foreach ($Cart->list as $row) {
                $price = $row->price_full > 0 ? $row->price_full : $row->price;

                $q = "SELECT * FROM `^e_articoli` WHERE id_parent = {$row->idart}";
                $children = $this->cn->Q($q);

                $children[] = $row->Product;

                $qty_articolo_rimasta = $row->qty;
                $idart_qty = array();
                foreach($children as $child){
                    //Controllo di sicurezza che l'articolo non sia già in un altro basket_row
                    $q = "SELECT * FROM `^basket_row` WHERE {$child->idart} IN (SELECT idart_qty FROM `^basket_row` WHERE id_basket!={$row->id_basket} AND idart!={$row->idart} )";
                    $this->cn->Q($q);

                    if( $this->cn->n==0 && $qty_articolo_rimasta>0) {
                        $idart_qty[] = $child->idart;
                        if( $child->giacenza >= $qty_articolo_rimasta ){
                            $QTA1 = $qty_articolo_rimasta;
                        }else{
                            $QTA1 = $qty_articolo_rimasta - $child->giacenza;
                        }
                        $qty_articolo_rimasta -= $QTA1;

                        $xml_order[] = '<CSXRigaOV CODARTICO="' . $child->codice . '" DATRIC="' . date("Ymd", strtotime($Cart->date)) . '" QTA1="' . $QTA1 . '" PREZZO="' . number_format($price, 2, ',', "") . '" IVACOMP="-1" SCONTO1="' . $row->sconto . '" SCONTO2="0" SCONTO3="0" SCONTO4="0" SCOQUA="" SCONTOL="0" />';
                    }
                }
                $q = "UPDATE `^basket_row` SET idart_qty = '". implode("," , $idart_qty ) . "' WHERE idart = {$row->idart} AND id_basket = {$row->id_basket}";
                $this->cn->Q($q);

                //$xml_order[] = '<CSXRigaOV CODARTICO="' . $row->Product->codice . '" DATRIC="' . date("Ymd", strtotime($Cart->date)) . '" QTA1="' . $row->qty . '" PREZZO="' . number_format($price, 2, ',', "") . '" IVACOMP="-1" SCONTO1="' . $row->sconto . '" SCONTO2="0" SCONTO3="0" SCONTO4="0" SCOQUA="" SCONTOL="0" />';
            }

            $xml_order[] = '</CSXRIGHE>';

            $xml_order[] = '</CSXOV>';
            $xml_order[] = '</CSXRoot>';

            if( $print_xml ) {
                $this->pr($xml_customer,false);
                $this->pr($xml_order);
            }

            if (!file_put_contents($_path . 'ordine' . $Cart->id . '.xml', implode("\r\n", $xml_order))) {
                $this->sendMail('parrella@inol3.com', 'no-reply@sottosotto.it', 'Errore generazione file Ordine ' . $id_basket, '');
            }else{
                $q = "UPDATE `^basket` SET sam_send = NOW(), sam_status=2 WHERE id = {$id_basket}";
                $this->cn->Q($q);
                $q = "UPDATE `^customers` SET sam_id = {$sam_id} WHERE id = {$Cart->id_customer}";
                $this->cn->Q($q);
            }
        }


        unset($Synch);
    }

    public function Order_resetBasketAlert($id_basket=null){
        if(!$id_basket){
            $id_basket = $this->Order_getIDBasket();
        }
        if( $id_basket>0 ) {
            $q = "UPDATE `^basket_row` SET qty_changed=0 WHERE id_basket={$id_basket}";
            $this->cn->Q($q);
        }
    }

    public function BasketMin(){
        $min = $this->P('minimum_basket');
        $basket = $this->Order_getBasket();
        //return $basket->totBasket >= $min;
        return $basket->totPrice >= $min;//totPrice è il prezzo esente IVA
    }

    public function Order_removeEmptyBasketRows($id_basket=null){
        if(!$id_basket){
            $id_basket = $this->Order_getIDBasket();
        }
        $q = "DELETE br.* FROM
		`^basket_row` br INNER JOIN `^basket` b ON br.id_basket = b.id

		WHERE b.id = {$id_basket} AND br.qty=0 AND b.`session` IS NOT NULL";
        //echo "{$q}<hr>";
        $this->cn->Q($q);

        $Basket = $this->Order_getBasket($id_basket);
        if($Basket->test){
            //echo "<pre>";print_r($Basket);
            foreach($Basket->list as $p){
                if( !$p->Product->brand_test ){
                    $q = "DELETE FROM `^basket_row` WHERE id_basket = {$id_basket} AND id_product_simple = {$p->ProductSimple->id}";
                    $this->cn->Q( $q );
                }
            }
        }
    }

    public function Order_getCustomerOrders($id_customer=false){
        $id_customer = $id_customer>0 ? $id_customer : $this->id_customer;

        $q = "SELECT * FROM `^cart` WHERE customer_id={$id_customer}";
        $list = $this->cn->Q($q);
        return $list;
    }

    public function Order_getOrderByCustomer($id_customer,$md5_id_order){
        $q = "SELECT id FROM `^basket` WHERE id_customer={$id_customer} AND MD5(id) = '{$md5_id_order}' AND `session` IS NULL";
        $id = $this->cn->OF($q);

        if( $id ){
            return $this->Order_getBasket($id);
        }else{
            return false;
        }
    }

    public function Order_orderList($id_customer){
        $giorni_reso = $this->P('giorni_reso');
        $q = "SELECT
		  *,
		  IF( DATE_ADD(`date`, INTERVAL {$giorni_reso} DAY)>NOW() , 1 , 0 ) AS return_available
		FROM `^basket` WHERE id_customer = {$id_customer} AND `session` IS NULL ORDER BY `date` DESC";
        $list = $this->cn->Q($q);
        return $list;
    }

    public function Order_getCouponByCode($code,$id_country){
        $code = trim(addslashes($code));
        $q = "SELECT c.*, cl.name FROM `^coupons` c INNER JOIN `^coupons_ln` cl ON c.id = cl.id_coupon AND cl.id_lang = {$this->id_lang}
			WHERE
			c.`code` = '{$code}'
			AND
			(c.id_country={$id_country} OR c.id_country IS NULL)
			AND
			(
				c.`from` <= NOW() OR c.`from` IS NULL
			)
			AND
			(
				NOW() <= c.`to` OR c.`to` IS NULL
			)
			AND c.enable=1
			";
        $data = $this->cn->Q($q);

        $C = false;
        if( $this->cn->n>1 ){
            foreach($data as $row){
                if($row->id_country = $id_country){
                    $C = $row;
                }
            }
        }else if( $this->cn->n==1 ){
            $C = $data[0];
        }

        return $C;
    }
    public function Order_getCoupon($id){
        $q = "SELECT c.*, cl.name FROM `^coupons` c INNER JOIN `^coupons_ln` cl ON c.id = cl.id_coupon AND cl.id_lang = {$this->id_lang} WHERE c.id = {$id}";
        $coupon = $this->cn->OQ($q);
        return $coupon=="-1" ? false : $coupon;
    }
    public function Order_checkCoupon($id_customer,$coupon,$id_country){//torna -1 se il codice non esiste, -2 se il codice è già stato usato, 1 se è valido
        $C = $this->Order_getCouponByCode($coupon, $id_country);
        if( !$C ){
            return -1;
        }else{
            $q = "SELECT * FROM `^basket` WHERE id_customer={$id_customer} AND id_coupon = {$C->id}";
            $this->cn->Q($q);
            return $this->cn->n>0 ? -2 : 1;
        }
    }
    public function Order_addCoupon($id_basket,$coupon,$id_country){
        $C = $this->Order_getCouponByCode($coupon,$id_country);
        if( $C ){
            $q = "UPDATE `^basket` SET id_coupon = {$C->id} WHERE id={$id_basket} AND id_coupon IS NULL";
            $this->cn->Q($q);
            return true;
        }else{
            return false;
        }
    }
    public function Order_removeCoupon($id_basket){
        $q = "UPDATE `^basket` SET id_coupon = NULL WHERE id={$id_basket}";
        $this->cn->Q($q);
    }

    public function Order_deleteBasketGift($id_cart_ses){
        $q = "DELETE FROM `^regalo_cart_ses` WHERE id_cart = {$id_cart_ses}";
        $this->cn->Q($q);
        return true;
    }

    public function Order_setBasketGift($id_cart_ses,$destinatario,$mittente,$messaggio){
        $q = "SELECT id FROM `^regalo_cart_ses` WHERE id_cart = {$id_cart_ses}";
        $regalo = $this->cn->OQ($q);
        if( $regalo==-1 ){
            $q = "INSERT INTO `^regalo_cart_ses` (id_cart) VALUES ({$id_cart_ses})";
            $this->cn->Q($q);
        }
        $q = "UPDATE `^regalo_cart_ses` SET destinatario = '" . addslashes($destinatario) . "', mittente = '" . addslashes($mittente) . "', messaggio = '" . addslashes($messaggio) . "' WHERE id_cart = {$id_cart_ses}";
        $this->cn->Q($q);
        return true;
    }

    public function Order_setBasketBuono($id_buono=false, $id_giftcard=false, $id_voucher=false){
        $ses = session_id();
        if( $id_buono>0 ){
            $q = "UPDATE `^cart_ses` SET id_buono = {$id_buono} WHERE `session`='{$ses}'";
        }else if($id_giftcard>0){
            $q = "UPDATE `^cart_ses` SET id_giftcard = {$id_giftcard} WHERE `session`='{$ses}'";
        }else if($id_voucher>0){
            $q = "UPDATE `^cart_ses` SET id_voucher = {$id_voucher} WHERE `session`='{$ses}'";
        }else{
            $q = "UPDATE `^cart_ses` SET id_buono = NULL, id_giftcard = NULL, id_voucher = NULL WHERE `session`='{$ses}'";
        }
        $this->cn->Q($q);
        return true;
    }

    public function Order_getInvitation( $id_cart=false, $id_invitation=false ){
        if( $id_cart>0 ){
            $q = "SELECT i.* FROM `^cart` c INNER JOIN `^invitation` i ON c.customer_id = i.guest_id WHERE c.id = {$id_cart} LIMIT 1";
        }else{
            $q = "SELECT * FROM `^invitation` WHERE id = {$id}";
        }
        $r = $this->cn->OQ($q);
        return $r==-1 ? false : $r;
    }
    public function Order_getGiftcard($id){
        $q = "SELECT * FROM `^giftcard` WHERE id = {$id}";
        $r = $this->cn->OQ($q);
        return $r==-1 ? false : $r;
    }
    public function Order_getGiftcardFromCode($code){
        $q = "SELECT * FROM `^giftcard` WHERE `codice`='{$code}' LIMIT 1";
        $giftcard = $this->cn->OQ($q);
        $q = "SELECT * FROM `^cart` WHERE id_giftcard = {$giftcard->id}";
        $tmp = $this->cn->OQ($q);
        $giftcard = $giftcard==-1 || $tmp!=-1 ? false : $giftcard;
        return $giftcard;
    }
    public function Order_getBuono( $id ){
        $q = "SELECT * FROM `^buono` WHERE id = {$id}";
        $r = $this->cn->OQ($q);
        return $r==-1 ? false : $r;
    }
    public function Order_getBuonoFromCode($code){
        $q = "SELECT * FROM `^buono` WHERE `code`='{$code}' AND date_end>NOW() LIMIT 1";
        $buono = $this->cn->OQ($q);
        $q = "SELECT * FROM `^cart` WHERE id_buono = {$buono->id}";
        $tmp = $this->cn->OQ($q);
        $buono = $buono==-1 || $tmp!=-1 ? false : $buono;
        return $buono;
    }
    public function Order_getBuoni($id_customer=false){
        $id_customer = $id_customer>0 ? $id_customer : $this->id_customer;
        $q = "SELECT b.* FROM `^buono` b INNER JOIN `^buono_customer` bc ON b.id = bc.id_buono WHERE bc.id_customer = {$id_customer}";
        $list = $this->cn->Q($q);
        return $list;
    }

    public function Order_getInvitations(){
        $q = "SELECT i.*, CONCAT(c.name_surname,' ',c.surname) AS customer_name FROM `^invitation` i INNER JOIN `^customer` c ON i.customer_id = c.id WHERE i.guest_id = {$this->id_customer}";
        $list = $this->cn->Q($q);
        return $list;
    }

    public function Order_existsCartSes($id){//Verifica esistenza carrello e la sua validità
        $q = "SELECT id FROM `^basket` WHERE id = {$id} AND `session` IS NOT NULL";
        $c = $this->cn->OF($q);
        return $c ? true : false;
    }

    public function Order_setDonazione($customer_id,$importo_meyer){
        $q = "INSERT INTO `^donazione` (customer_id,importo,`date`) VALUES ({$customer_id},'" . number_format($importo_meyer,2,".","") . "',NOW())";
        $this->cn->Q($q);
        $id = $this->cn->last_id;
        return $id;
    }

    protected function Order_newCode($length=10){
        $flag = false;
        do{
            $code = $this->RandomCode($length,"an");
            $q = "SELECT id FROM `^cart` WHERE code='{$code}'";
            $id = $this->cn->OF($q);
            if(!$id){
                $flag = true;
            }
        }while(!$flag);
        return $code;
    }

    public function Order_setOrder($id_basket,$type='bank',$data_response,$send_mail=false){ //$type => bank / paypal
        $q = "SELECT * FROM `^basket` WHERE id = {$id_basket} AND `session` IS NOT NULL";
        $cart_ses = $this->cn->OQ($q);

        if( $cart_ses==-1 ){
            return false;
        }

        $data_response_str = json_encode($data_response);
        if( $type=='bank' ){
            $bank_status = $data_response['esito'];
            $ipn = '';
        }else if($type=='paypal'){
            $bank_status = $data_response['payment_status'];
            $ipn = $_POST['txn_id'];
        }

        $q = "UPDATE `^basket` SET `session` = NULL, bank_data = '{$data_response_str}', bank_status = '{$bank_status}', bank_timestamp = NOW(), `ipn` = '{$ipn}', `date` = NOW() WHERE id = {$id_basket}";
        $this->cn->Q($q);

        if(
            ($type=='bank' && $data_response['esito']=='OK')
            ||
            ($type=='paypal' && $data_response['payment_status']=='Completed')
        ){
            if( !$cart_ses->test ){ //Generazione XML per SAM
                $this->Order_setOrderSAM( $id_basket );
            }
            if( $send_mail ){
                $mail_test = $cart_ses->test ? 'parrella@inol3.com' : false;
                $this->Order_sendOrderMail($id_basket,$mail_test);
            }
        }

        return $id_basket;
    }

    public function Order_sendOrderMail($id_basket,$test_mail=NULL){
        $Cart = $this->Order_getBasket($id_basket);
        $shipping_data = json_decode($Cart->shipping_data,false);
        $Customer = $this->getCustomer( $Cart->id_customer );

        //INVIO E-MAIL
        $mail = $this->getMailTemplate('ordine.html');
        $mail = str_replace("#*company*#" , $Customer->company , $mail);
        $mail = str_replace("#*name*#" , $Customer->name , $mail);
        $mail = str_replace("#*surname*#" , $Customer->surname , $mail);
        $mail = str_replace("#*vat*#" , $Customer->vat , $mail);
        $mail = str_replace("#*n_ordine*#" , $this->Order_codeFromId($id_basket) , $mail);
        $mail = str_replace("#*e_mail*#" , $Customer->email , $mail);

        $mail = str_replace("#*name_sped*#" , $shipping_data->name , $mail);
        $mail = str_replace("#*surname_sped*#" , $shipping_data->surname , $mail);
        $mail = str_replace("#*address_sped*#" , $shipping_data->address . ", " . $shipping_data->address_number , $mail);
        $mail = str_replace("#*cap_sped*#" , $shipping_data->zip, $mail);
        $mail = str_replace("#*city_sped*#" , $shipping_data->city , $mail);

        $nation = $this->getCountry( $shipping_data->id_country );
        $mail = str_replace("#*nation_sped*#" , $nation->name , $mail);

        $mail = str_replace("#*address*#" , $Customer->BillingInfo->address . ", " . $Customer->BillingInfo->address_number , $mail);
        $mail = str_replace("#*cap*#" , $Customer->BillingInfo->zip , $mail);
        $mail = str_replace("#*city*#" , $Customer->BillingInfo->city , $mail);

        $nation = $this->getCountry( $Customer->BillingInfo->id_country );
        $mail = str_replace("#*nation*#" , $nation->name , $mail);

        if( $Cart->advance_payment ){
            $txt = $this->W('Advance Payment');
            $paid = $Cart->advance_payment_cost_txt;
            $payment_final_balance = $Cart->totBasket - $Cart->advance_payment_cost;
            $payment_final_balance = '&euro; ' . $this->Money( $payment_final_balance );
        }else{
            $txt = $this->W('Full payment');
            $paid = $Cart->totBasket_txt;
            $payment_final_balance = '&euro; ' . $this->Money(0);
        }
        $mail = str_replace("#*payment_method*#", $txt, $mail);
        $mail = str_replace("#*paid*#", $paid, $mail);
        $mail = str_replace("#*payment_final_balance*#", $payment_final_balance, $mail);


        $table = array();
        $table[] = '<table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-size: 12px;" bgcolor="#ffffff">';
        $table[] = '<tr>';
        $table[] = '<td width="20" height="30" bgcolor="#00901e">&nbsp;</td>';
        $table[] = '<td colspan="2" align="left" bgcolor="#00901e"><p>' . $this->W('Prodotto') . '</p></td>';
        $table[] = '<td bgcolor="#00901e"><p>' . $this->W('Q.tà') . '</p></td>';
        $table[] = '<td bgcolor="#00901e" align="center"><p>' . $this->W('Prezzo') . '</p></td>';
        $table[] = '<td width="20" bgcolor="#00901e">&nbsp;</td>';
        $table[] = '</tr>';

        $i=0;
        $border_style = "border-top: 1px solid #dddddd;";
        $style = ' style="' . $border_style . '"';
        foreach($Cart->list as $item){
            $img = $this->DomainUrl();

            $img_found = false;
            if( count($item->Product->foto)>0 ) {
                foreach ($item->Product->foto as $f) {
                    if ($f->cod_colore == $item->Product->colore && !$img_found) {
                        $img .= $this->Img($f->path, 'prod_thumb');
                        $img_found = true;
                    }
                }
            }
            if( !$img_found ){
                $img .= path_webroot . '_ext/img/foto_def.png';
            }

            //$style = $i==0 ? ' style="' . $border_style . '"' : '';
            $table[] = '<tr>';
            $table[] = '<td width="20">&nbsp;</td>';
            $table[] = "<td align=\"center\"{$style}><br><img src=\"{$img}\" width=\"60\"></td>";

            $descrbase = empty( trim( $item->Product->descrbase ) ) ? $item->Product->categoria : $item->Product->descrbase;
			
			$colore = $item->Product->colore;
			foreach($item->Product->colori as $c){
				if($c->codice==$item->Product->colore){
					$colore = $c->descr;	
				}
			}
			$taglia = $item->Product->taglia;
			foreach($item->Product->taglie as $t){
				if($t->codice==$item->Product->taglia){
					$taglia = $t->descr;	
				}
			}
			
			
            $table[] = "<td{$style}>
					<br>
					{$descrbase} - {$colore} - {$taglia}
					<br>
				</td>";
            $table[] = "<td{$style} align=\"center\">{$item->qty}</td>";
            $p = $this->Money( $item->price * $item->qty );
            $table[] = "<td width=\"100\" align=\"center\"{$style}>&euro; {$p}</td>";
            $table[] = '<td width="20">&nbsp;</td>';
            $table[] = '</tr>';
            $i++;
        }
		
        //IVA
        if( $Cart->vat_perc>0 ){
            $table[] = '<tr><td colspan="6">&nbsp;</td></tr>';
            $table[] = '<tr><td>&nbsp;</td><td colspan="3" align="right"' . $style . '><br>' . $this->W("IVA") . ' ' . $Cart->vat_perc . '%:</td><td align="center"' . $style . '><br>' . $Cart->vat_cost_txt . '</td><td>&nbsp;</td></tr>';
        }

        //BUONO / GIFTCARD / INVITO
        if( $Cart->id_coupon>0 ){
            $table[] = '<tr><td colspan="6">&nbsp;</td></tr>';
            $table[] = '<tr><td>&nbsp;</td><td colspan="3" align="right">' . $this->W("Coupon") . ':</td><td align="right">' . $Cart->buono_cost_txt . '</td><td>&nbsp;</td></tr>';
        }

        //TOTALE
        $table[] = '<tr><td colspan="6">&nbsp;</td></tr>';
        $table[] = '<tr>
			<td>&nbsp;</td>
			<td colspan="3" align="right" style="' . $border_style . 'font-size: 16px;"><b><br>' . $this->W('TOTALE') . ':</b></td>
			<td align="center" style="' . $border_style . 'font-size: 16px;"><b><br>' . $Cart->totBasket_txt . '</b></td>
			<td>&nbsp;</td>
		</tr>';
        $table[] = '<tr><td colspan="6">&nbsp;</td></tr>';

        $table[] = '</table>';

        $table = implode("\n",$table);
        $mail = str_replace("#*table_order*#" , $table , $mail);

        $mail = $this->footerMail($mail);

        $subject = $this->W("Conferma d'ordine");
        $to = $Customer->email;
        if( server=='local' ){
            $to = "parrella@inol3.com";
            $to = "domini@inol3.com";
        }else if( server=='live' ){
            $bcc = 'nadia.sarti@sottosotto.it,valentina@sottosotto.it,sottosotto@inol3.com';
        }

        if( $test_mail ){
            $to = $test_mail;
            $bcc = false;
            $this->noBccMain = true;
        }

        //$this->noBccMain = true;
        return $this->sendMail($to,$Customer->name,"no-reply@sottosotto.it","Sotto Sotto",$subject,$mail,null,$bcc);
    }

    public function Order_setBasketReturn($_postdata,$sendmail=false){
        $_postdata_map = $_postdata;
        //$_postdata_map = array_map('trim',$_postdata_map);
        //$_postdata_map = array_map('addslashes',$_postdata_map);

        $q = "SELECT id FROM `^basket_return` WHERE id_basket = {$_postdata['id']}";
        $id = $this->cn->OF($q);

        if(!$id){
            $q = "INSERT INTO `^basket_return` (id_basket) VALUES ({$_postdata['id']})";
            $this->cn->Q($q);
            $id = $this->cn->last_id;
        }
        $q = "UPDATE `^basket_return`
            SET
            `date` = NOW(),
            `note` = '{$_postdata_map['note']}'
            WHERE id = {$id}";
        $this->cn->Q($q);

        foreach($_postdata['prod_return'] as $idart=>$qty){
            if($qty>0){
                $q = "UPDATE `^basket_row` SET id_basket_return = {$id}, qty_return = {$qty} WHERE id_basket = {$_postdata['id']} AND idart = {$idart}";
                $this->cn->Q($q);
            }
        }

        if($sendmail){
            $this->Order_sendReturnMail($id);
        }
        return $id;
    }

    public function Order_sendReturnMail($id_basket_return){
        $q = "SELECT * FROM `^basket_return` WHERE id = {$id_basket_return}";
        $BasketReturn = $this->cn->OQ($q);

        $Basket = $this->Order_getBasket( $BasketReturn->id_basket );
        //Righe
        $q = "SELECT * FROM `^basket_row` WHERE id_basket_return = {$id_basket_return}";
        $rows = $this->cn->Q($q);

        $Customer = $this->getCustomer( $Basket->id_customer );

        $mail = $this->getMailTemplate('richiesta-reso.html');
        $mail = str_replace("#*company*#" , $Customer->company , $mail);
        $mail = str_replace("#*name*#" , $Customer->name , $mail);
        $mail = str_replace("#*surname*#" , $Customer->surname , $mail);
        $mail = str_replace("#*vat*#" , $Customer->vat , $mail);
        $mail = str_replace("#*n_ordine*#" , $this->Order_codeFromId($Basket->id) , $mail);
        $mail = str_replace("#*e_mail*#" , $Customer->email , $mail);

        $table = array();
        $table[] = '<table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-size: 12px;" bgcolor="#ffffff">';
        $table[] = '<tr>';
        $table[] = '<td width="20" height="30" bgcolor="#00901e">&nbsp;</td>';
        $table[] = '<td colspan="2" align="left" bgcolor="#00901e"><p>' . $this->W('Prodotto') . '</p></td>';
        $table[] = '<td bgcolor="#00901e"><p>' . $this->W('Q.tà') . '</p></td>';
        $table[] = '<td bgcolor="#00901e" align="center"><p>' . $this->W('Prezzo') . '</p></td>';
        $table[] = '<td width="20" bgcolor="#00901e">&nbsp;</td>';
        $table[] = '</tr>';

        $i=0;
        $border_style = "border-top: 1px solid #dddddd;";
        $style = ' style="' . $border_style . '"';

        $img_base = $this->DomainUrl() . path_webroot;
        foreach($rows as $item){
            $prod = $this->Product($item->idart,array('no-status'=>1));
            //$this->pr($prod);

            $img_found = false;
            if( count($prod->foto)>0 ) {
                foreach ($prod->foto as $f) {
                    if ($f->cod_colore == $prod->colore) {
                        $img = $this->Img($f->path, 'prod_thumb');
                        $img_found = true;
                    }
                }
            }
            if( !$img_found ){
                $img = '_ext/img/foto_def.png';
            }

            $img = $img_base . $img;

            //$style = $i==0 ? ' style="' . $border_style . '"' : '';
            $table[] = '<tr>';
            $table[] = '<td width="20">&nbsp;</td>';
            $table[] = "<td align=\"center\"{$style}><br><img src=\"{$img}\" width=\"60\"></td>";
            $descrbase = empty( trim( $prod->descrbase ) ) ? $prod->categoria : $prod->descrbase;
            $table[] = "<td{$style}>
					<br>
					{$descrbase} - {$prod->colore} - {$prod->taglia}
					<br>
				</td>";
            $table[] = "<td{$style} align=\"center\">{$item->qty_return}</td>";
            $p = $this->Money( $item->price * $item->qty );
            $table[] = "<td width=\"100\" align=\"center\"{$style}>&euro; {$p}</td>";
            $table[] = '<td width="20">&nbsp;</td>';
            $table[] = '</tr>';
            $i++;
        }

        $table[] = '</table>';

        $table = implode("\n",$table);
        $mail = str_replace("#*table_order*#" , $table , $mail);

        $mail = $this->footerMail($mail);

        $subject = $this->W("Richiesta di reso");

        $bcc = null;
        if( server=='local' ){
            $to = "parrella@inol3.com";
            //$to = "domini@inol3.com";
        }else if( server=='live' ){
            $to = 'nadia.sarti@sottosotto.it,valentina@sottosotto.it';
            $to = 'parrella@inol3.com';
            //$bcc = 'parrella@inol3.com';
        }

        if( $test_mail ){
            $to = $test_mail;
            $bcc = false;
            $this->noBccMain = true;
        }

        //$this->noBccMain = true;
        return $this->sendMail($to,$Customer->name,"no-reply@sottosotto.it","Sotto Sotto",$subject,$mail,null,$bcc);
    }

    public function Order_BankShift($id_basket,$shift){
        $shift++;
        $q = "UPDATE `^basket` SET bank_shift = {$shift} WHERE id = {$id_basket}";
        $this->cn->Q( $q );
        return $shift;
    }
}