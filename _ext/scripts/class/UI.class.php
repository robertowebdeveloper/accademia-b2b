<?php
class UI{
	private $src_tag = 'src';

	public function __construct($S){
		$this->S = &$S;

		$lazy_load = $this->S->P('lazy_load_on');
		$this->src_tag = $lazy_load ? 'data-original' : 'src';
	}

	public function __destruct(){}

	private function Parse($code){
		return implode("\n", $code);
	}

	public function ProductItem($id,$opts=NULL){
		$P = $this->S->Product($id);
		//$this->S->pr($P);

		return $this->ProductItemView($P,$opts);

	}

	public function ProductItemView($P,$opts=NULL){
		$id = $P->id;
		$code = array();
		if( $opts->col ){
			$col = $opts->col;
		}else{
			$col = "col-md-3 col-sm-6 col-xs-6";
		}

		if( $opts && $opts->ajax ){
			$img_responsive_class = 'pre-img-responsive';
		}else{
			$img_responsive_class = 'img-responsive';
		}

		$opts_url = new stdClass();
		$opts_url->id = $id;
		$opts_url->type = 'product-item';
		//$url = !$this->S->isAuth() ? "#" : $this->S->Url('product-item',$opts_url);
		$url = $this->S->Url( 'product-item' , $opts_url );
		$code[] = "<div class=\"{$col}\"><a href=\"{$url}\" class=\"productItem\" data-id=\"{$id}\">";

		//Immagine principale
		if( $P->foto && count($P->foto)>0 ){
			$img = $P->foto[0]->path;
		}else if( $P->main_foto ){
			$img = $this->S->ProductPathFoto( $P->main_foto , true);
		}else{
			$img = false;
		}

		if( !$img ){
			$img = '_ext/img/foto_def.png';
		}
		$code[] =  '<img alt="' . $this->S->AltParse( $P->griffe . " " . $P->descrbase ) . '" src="' . $this->S->Img($img , 'prod_list' ) . '" class="lazy ' . $img_responsive_class . '">';

		//Immagini retro
		if( $P->foto && count($P->foto)>1 ){
			for($foto2=null,$i=1;$i<count($P->foto);$i++){
				if( !$foto2 && $P->foto[$i]->tipo!=1 && $P->foto[$i]->barcode==$P->foto[0]->barcode && $P->foto[$i]->cod_colore==$P->foto[0]->cod_colore ){
					$foto2 = $P->foto[$i]->path;
				}
			}
		}else if( $P->retro_foto && 0 ){
			$foto2 = $this->S->ProductPathFoto( $P->retro_foto , true);
		}else{
			$foto2 = false;
		}

		if( $foto2 ){
			$code[] = '<img alt="" src="' . $this->S->Img( $foto2 , 'prod_list') . '" width="189" height="286" class="img-responsive back">';
		}
		$code[] = '<div class="info">';

		$code[] = '	<div class="title">' . $P->griffe . '</div>';

		$code[] = '	<div class="descr">' . $P->descrbase . '</div>';

		$code[] = '</div>';

		$code[] = '<div class="priceLine">';

		$code[] = '	<button class="Btn btn btn-sm">'. $this->S->W('Visualizza') . '</button>';

		$code[] = '		<span class="price">&euro; ' . number_format($P->prezzo,0,",",".") . '</span>';
		if( $P->prezzo_full>0 ){
			$code[] = '		<span class="price_full">&euro; ' . number_format($P->prezzo_full,0,",",".") . '</span>';
		}
		$code[] = '</div>';//priceLine end


		$code[] = '</a></div>';

		return $this->Parse($code);
	}
}
?>