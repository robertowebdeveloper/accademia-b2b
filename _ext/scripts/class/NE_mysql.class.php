<?php
class NE_mysql{
	private $Version = "2.1.0";//11-11-2015
	public $db = false; //nome database selezionato
	public $db_default = false; //nome database di default
	public $db_table_prefix = NULL; //prefisso delle tabelle
	public $cn = false; //identificatore della connessione
	public $n = 0; //record trovati dell'ultima select eseguita;
	public $last_id = 0; //ultimo id inserito da un insert;
	public $ris = NULL;
	public $query_error = false; //impostare a true se si vuole attivare die(mysql_error()) sulle chiamate a query
	public $default_charset = false; //acquisisce il charset di default da config.inc.php
	public $permanentConnect = false; //solo per connessioni di tipo mysql_
	public $last_query_exec = NULL; //Ultima query eseguita

	private $db_data = NULL;

	public function __construct($database=true,$db_data=null){//true per passare db di default, false per non aprire db, nome per aprire un db col nome
		if( !$db_data ){
			global $NE_db_data;
			if( isset($NE_db_data) ){
				$db_data = $NE_db_data;
			}
		}

		if( $db_data ){
			$this->db_data = $this->toObj($db_data);
			if( isset( $this->db_data->default_charset ) ){
				$this->default_charset = $this->db_data->default_charset;
			}
			if( isset($this->db_data->db_table_prefix) ){
				$this->db_table_prefix = $this->db_data->db_table_prefix;
			}
		}

		if( $database ){
			$this->OpenDf($database);
		}
	}

	public function __destruct(){
		$this->Close();
		unset($this->db,$this->db_default,$this->cn,$this->n,$this->last_id,$this->ris,$this->query_error);
	}

	public function OpenDf($database=NULL){ //apre la connessione e seleziona il db scelto (con null seleziona il db di default);
		$this->Open();
		$this->Sel($database);
	}

	public function Open(){
		//apre la connessione al database con l'utente di default
		//global $NM_Path;
		//require_once($NM_Path."config.inc.php");
		$this->db_default = $this->db_data->db;
		//echo "<pre>";print_r($this->db_data);
		$port = isset($this->db_data->port) && $this->db_data->port ? $this->db_data->port : ini_get("mysqli.default_port");
		$socket = isset($this->db_data->socket) && $this->db_data->socket ? $this->db_data->socket : ini_get("mysqli.default_socket");
		$this->cn = new mysqli($this->db_data->hostname, $this->db_data->username, $this->db_data->password,"", $port, $socket);
		if( $this->cn->connect_error ){
			echo 'Connect Error (' . $this->cn->connect_errno . ') ' . $this->cn->connect_error . '<hr>';
			return false;
		}
		unset($port,$socket);

		return $this->cn;
	}

	public function OpenSt($host,$user,$pw,$port=false,$socket=false){
		$port = $port ? $port : ini_get("mysqli.default_port");
		$socket = $socket ? $socket : ini_get("mysqli.default_socket");
		$this->cn = new mysqli($host, $user, $pw,"", $port, $socket);
		return $this->cn;
	}

	public function Close(){
		if($this->cn){
			$this->cn->close();
			unset($this->cn);
			return true;
		}
		return false;
	}

	public function Sel($database=NULL,$timezone="Europe/Rome"){//seleziona il database. Con $database = 0 o NULL seleziona il database di default
		if($this->cn){
			$this->db = is_string($database) ? $database : $this->db_default;
			$status = $this->cn->select_db($this->db);

			if($status && $this->default_charset){
				$this->Charset($this->default_charset);
				$this->SetTimeZone($timezone);
			}
			return $status;
		}else{
			die("Connection not created");
		}
	}

	public function SetTimeZone($timezone="Europe/Rome"){
		$q = "SET time_zone = '{$timezone}'";
		$this->cn->Q( $q );
	}

	public function Charset($charset="utf8"){ //SETTA IL CHARSET
		//possibili valori: utf8, sjis(per giapponese)
		$this->Q("SET NAMES {$charset}");
	}

	public function TablePrefix($query){
		if( $this->db_table_prefix ){
			$query = str_replace("`^", "`" . $this->db_table_prefix, $query);
		}
		return $query;
	}

	private function toObj($arr){
		return json_decode( json_encode($arr) , false);
	}

	public function Q($query,$return='object'){ //esegue una query ($return => 'object' / 'array' / false ritorna il $ris)
		$query = $this->TablePrefix($query);
		if($this->db && $this->cn){
			$ris = $this->cn->query($query);
			if( $this->query_error && !$ris ){
				die( $this->cn->error . " :: QUERY => {$query}" );
			}
			$this->last_query_exec = $query;

			//****
			//CERCA IL TIPO DI QUERY
			//****
			if(strpos( strtolower($query) ,"select")<=5 && strpos( strtolower($query) ,"select")!==false){
				$this->n = $ris ? $ris->num_rows : 0;
				$this->ris = $ris;

				if( $return ){
					$arr = array();
					if( $this->n>0 ){
						while( $r=$this->R() ){
							$arr[] = $r;
						}
						@$this->F($ris);
					}
					if($return=="object"){
						$arr = $this->toObj($arr);
					}
					return $arr;
				}

			}else if(strpos( strtolower($query) ,"insert")<=5 && strpos( strtolower($query) ,"insert")!==false){
				$this->last_id = $this->cn->insert_id;
			}
			return $ris;
		}else{
			return false;
		}
	}

	public function OQ($query,$return='object'){ //seleziona una singola scheda, funziona solo con query SELECT di tipo WHERE id = x
		$query = $this->TablePrefix($query);
		$ris = $this->cn->query($query);
		if( $this->query_error && !$ris){
			die( $this->cn->error . " :: QUERY => {$query}" );
		}
		$this->last_query_exec = $query;
		if($ris && $ris->num_rows==1){
			$r = $ris->fetch_assoc();
			if( $return=='object' ){
				$r = $this->toObj($r);
			}
			$ris->free();
			return $r;
		}else return -1;
	}

	public function OF($query,$field=NULL){ //restituisce il campo della query specificato in "field", se vuoto restituisce il primo campo della query [ OF = One Field ]
		$row = $this->OQ($query,'array');
		if($row==-1)
			return false;
		if(empty($field)){
			foreach($row as $e){
				return $e;
			}
		}else{
			return $row[$field];
		}
	}

	public function R($ris=NULL){
		if(!$ris){
			$ris = &$this->ris;
		}
		return $ris->fetch_assoc();
	}

	public function F($ris){
		if(empty($ris))
			$ris = &$this->ris;
		if( $ris->num_rows>0 ){
			$ris->free();
		}else{
			return false;
		}
	}

	public function enumValues($table,$field){ //ritorna un array con i valori preimpostati di un campo set/enum
		$row = $this->OQ("SHOW COLUMNS FROM `{$table}` LIKE '{$field}'");
		$type = $row->Type;
		$type = substr($type,strpos($type,"(")+1);
		$type = substr($type,0,strlen($type)-1);

		$arr = explode(",",$type);
		foreach($arr as $k=>$v){
			$v = trim($v);
			$v = substr($v,1);
			$v = substr($v,0,strlen($v)-1);
			$arr[$k] = $v;
		}
		return $arr;
	}

	private function Error($error){
		die($error);
	}

	public function Rewind($ris){
		$this->Seek($ris,0);
	}

	public function Seek($ris,$pos){
		if(empty($ris))
			$ris = &$this->ris;
		$ris->data_seek($pos);
	}
}
?>
