<?php
class System{
	public $cn;
	
	public $Version = '0.1.0';
	
	public $id_lang = NULL;
	public $_lang = '';
	public $id_country = NULL;
	public $Page = NULL;
	public $pagesTree = NULL;
	public $lang_in_url = NULL;
	public $_path = NULL;
	public $id_customer = 0;
	public $force_https = false;
	public $reg_code_invalid = false;
	public $sendermail = 'phpmailer';
	protected $EOL = "\r\n";
	protected $csv_comma = ";";
	
	public $is_msie = false;
	public $msie_version = NULL;
	
	public function __construct( $cn ){
		@session_start();
		
		//DB
		$this->cn = &$cn;
		
		//Set charset
		$this->cn->Charset();
		
		/* Set paths */
        if( defined('path_docroot') ) {
            $this->_path = new stdClass();
            $this->_path->docroot = path_docroot;
            $this->_path->site = path_site;
            $this->_path->log = path_site . '_public/log/';
            $this->_path->root = path_webroot;
            $this->_path->css = path_webroot . '_ext/css/';
            $this->_path->js = path_webroot . '_ext/js/';
            $this->_path->img = path_webroot . '_ext/img/';
            $this->_path->scripts = path_webroot . '_ext/scripts/';
            $this->_path->mail = path_site . '_ext/mail/';
            $this->_path->mail_lang = '';//viene configurata dopo il riconoscimento lingua
        }

		$lang_in_url = $this->P('lang_in_url');
		$this->lang_in_url = $lang_in_url==1 ? true : false;

        if( defined('path_docroot') ) {
            $this->_path->absolute = $this->AbsoluteUrl();
        }
	}
	
	public function __destruct(){}
	
	public function O($arr=array()){//Convert array to object
		if( is_array($arr) ){
			return json_decode( json_encode($arr), false);
		}else{
			return false;
		}		
	}
	
	protected function Log($txt,$status='error'){
		$filename = "{$status}.log";
		$fp = fopen( "{$this->_path->site}_public/logs/{$filename}", "a" );
		$str = "---------------- " . date('d/m/Y H:i:s') . "\n";
		$str .= $txt;
		$str .= "\n\n\n";
		fwrite($fp,$txt); 
		fclose($fp);
	}
	
	protected function writeCSV($path_file,$data){
		$fp = fopen($path_file,"w");
		foreach($data as $row){
			$row .= $this->EOL;
			fwrite($fp,$row, strlen($row) );
		}
		fclose($fp);
	}
	
	public function setLang($id_lang=1,$lang='it'){//se $lang è "auto" lo prende da db
		if($lang=='auto'){
			$q = "SELECT url FROM `^langs` WHERE id = {$id_lang}";
			$lang = $this->cn->OF($q);
		}
		$_SESSION["_lang"] = $this->_lang = $lang;
		$this->id_lang = $id_lang;

        if( isset($this->_path) ) {
            $this->_path->mail_lang = $this->_path->mail . $lang . "/";
        }
	}
	
	public function Start(){
		if( isset($_SESSION['id_customer']) && $_SESSION['id_customer']>0){
			$this->id_customer =  $_SESSION['id_customer'];
		}
		
		$this->force_https = $this->P('force_https')=="1" ? true : false;
		if(!isset($_SERVER['HTTPS']) && $this->force_https && server=='live'){
			$url = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
			header("Location: {$url}");exit;
		}

		$_P = $_GET['_P'];
		if( $_P=='robots.txt' && !file_exists($this->_path->docroot . '/robots.txt')  ){
			header('Content-Type: text/plain; charset=utf-8');
			echo "User-agent: *\r\n";
			echo "Disallow: /hello\r\n";
			exit;
		}

		if( strpos($_P,'google')===0 && substr($_P,-5)==".html" ){
			header('Content-Type: text/html; charset=utf-8');
			$gsv = $this->P('gsv');
			echo "google-site-verification: {$gsv}";
			exit;
		}

		//Indirizzo attuale
		$this->request_url = isset($_SERVER["HTTPS"]) ? "https://" : "http://";
		$this->request_url .= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
		
		$ua = $_SERVER["HTTP_USER_AGENT"];
		//IE 8 example => Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022)
		$str = 'MSIE ';
		
		if( ($pos=strpos($ua,$str))>0 ){
			$this->is_msie = true;

			$pos_end = strpos($ua,".",$pos);
			$pos += strlen($str);
			$v = substr($ua,$pos, $pos_end-$pos);
			$this->msie_version = intval($v);
		}

		//Recognize Page
		$url_elements = $this->getUrlElements();

		/* Set language */
		$lang = false;
        if( $this->lang_in_url && count($url_elements)>0 ){
			$q = "SELECT * FROM `^langs` WHERE `url` = '{$url_elements[0]}' LIMIT 1";
			$lang = $this->cn->OQ($q);
			if( $lang!=-1 ){
				$_SESSION["_lang"] = $this->_lang = $lang->url;
				$this->id_lang = $lang->id;
			}
		}
		
		if( !isset($_SESSION["_lang"]) || empty($_SESSION["_lang"]) || !($this->id_lang)){
			$q = "SELECT * FROM `^langs` WHERE is_default=1 LIMIT 1";
			$lang = $this->cn->OQ($q);
			if( $lang != "-1" ){
				$this->setLang($lang->id,$lang->url);
			}else{
				$this->setLang();
			}
		}else{
			$this->_path->mail_lang = $this->_path->mail . $this->_lang . "/";
		}
		/* END Set language */
		
		/* Set Country */
		$this->setCountry();
		/* End Set Country */
		
		if( isset($lang->iso) )
			setlocale(LC_TIME, $lang->iso);
		$this->setPages();
		
		if( isset($_GET['reg_code']) ){
			//Verifica attivazione utente
			$id_customer = $this->activeCustomer($_GET['reg_code']);
			if( $id_customer ){
				header("Location: " . $this->Url('products') );
			}else{
				$this->reg_code_invalid = true;
			}
		}

		//LOGIN
		if(
			(isset($_POST['act']) && $_POST['act']=='login')
			||
			( !isset($_SESSION['id_customer']) && isset($_COOKIE['cst']) )
		){
			$this->Login();
		}

		//LOGOUT
		if( isset($_GET['logout']) ){
			unset($_SESSION['id_customer'] , $this->id_customer);
			session_regenerate_id(true);
		}

		//SET CUSTOMER
		if( isset($_SESSION['id_customer']) && $_SESSION['id_customer']>0 ){
			$this->Customer = $this->getCustomer();
		}

		//FOR DEBUG
		if( _DEBUGMODE && isset($_GET['Page']) ){
			$this->pr($this->Page);
		}

        //Cache system
        $cachefile = $this->cachefileName();
        if( false && $this->Page->cache_on && file_exists( $cachefile ) && filemtime( $cachefile )+(3600*1) > time() && count($_POST)==0 ){
            echo file_get_contents( $cachefile );exit;
        }else if(substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')){
            ob_start('ob_gzhandler');
        }else{
            ob_start();
        }
	}
	
	public function End(){
		//disable responsive
		$html = ob_get_contents();
		ob_end_clean();
		
		$html = $this->toHtml($html);
        if( $this->Page->cache_on && false) {
            file_put_contents($this->cachefileName(true), $html);//scrive la cache
        }
        echo $html;
	}

    protected function cachefileName($make_dir=false){
        $uri_md5 = md5( $_SERVER['REQUEST_URI'] . $_SESSION['id_customer'] );
        $dir = __DIR__ . '/../../../_public/cache/pages/' . substr($uri_md5,0,2);
        if( !file_exists($dir) && $make_dir ){
            @mkdir( $dir );
            @chmod($dir, 0777);
        }
        return  "{$dir}/{$uri_md5}.html";
    }
	
	public function toHtml($html){
		$html = $this->doHtml( $html );
		
		//SET BODY
		$class = array();
		$style = array();
		if( $this->Page->css_class ){
			$class[] = $this->Page->css_class;
		}
		
		if( isset($_GET['modal']) ){
			$class[] = 'modalWindow';
		}
		
		if( count($class)>0 ){
			$class = ' class="' . implode(" ",$class) . '"';
		}else{
			$class = '';
		}
		if( count($style)>0 ){
			$style = ' style="' . implode(" ",$style) . '"';
		}else{
			$style = '';
		}
		$body_tag = "<body{$class}{$style}>";

		//Google Analytics
		$ga = $this->P('ga');
		if( $ga && server=='live' ){
			$domain = $_SERVER['HTTP_HOST'];
			if( strpos($domain,"www.")===0 ){
				$domain = str_replace("www.","",$domain);
			}
			$body_tag .= "
				<script type=\"text/javascript\">
				  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				  ga('create', '{$ga}', 'auto');
				  ga('send', 'pageview');

				</script>
			";
		}
		//End Google Analytics

		$html = str_replace('<body>',$body_tag,$html);
		unset($body_tag,$class,$style);
		//END SET BODY
		
		//Base path
		$html = str_replace("{{root}}", $this->_path->root , $html);
		$html = str_replace("{{css}}", $this->_path->css , $html);
		$html = str_replace("{{js}}", $this->_path->js , $html);
		$html = str_replace("{{img}}", $this->_path->img , $html);
		
		//This url
		$urlthis = $_SERVER['REQUEST_URI'];
		$html = str_replace('{{urlthis_get}}', $urlthis, $html);
		list($urlthis) = explode("?",$urlthis);
		$html = str_replace('{{urlthis}}', $urlthis, $html);
		
		$html = str_replace('{{absolute_urlthis}}', $this->DomainUrl() . $urlthis, $html );
		
		//Url back
		//$urlback = $_SERVER['HTTP_REFERER'];
		//$html = str_replace('{{urlback}}',$urlback,$html);
		$html = str_replace('{{back}}', '<a href="#" class="Back"><i></i>' . $this->W('Indietro') . '</a>', $html);
		
		$q = "SELECT id,url FROM `^langs` WHERE status='enable'";
		$list = $this->cn->Q($q);
		foreach($list as $l){
			$opts = new stdClass();
			$opts->id_lang = $l->id;
			$html = str_replace('{{url lang ' . $l->url . '}}', $this->Url('this',$opts), $html );
		}
		
		//Url
		$q = "SELECT
		p.`k`, p.id, pl.url, pl.name, pl.absolute_url, pl.target_blank
		FROM
		`^pages` p INNER JOIN `^pages_ln` pl ON p.id = pl.id_page AND pl.id_lang = {$this->id_lang}
		WHERE p.enable=1";
		$list = $this->cn->Q($q);
		foreach($list as $item){
			if( $item->absolute_url ){
				$html = str_replace('{{url ' . $item->id . '}}', $item->url, $html);
				$html = str_replace('{{url ' . $item->k . '}}', $item->url, $html);
			}else{
				//URL WITH ID
				$found = preg_match_all("/({{url {$item->k}) (\d+)/", $html, $matches);
				if( $found && count($matches)==3){
					for($i=0;$i<$found;$i++){
						$opts = new stdClass();
						$opts->id = $matches[2][$i];
						$opts->type = $item->k;
						$html = str_replace('{{url ' . $item->k . ' ' . $opts->id . '}}', $this->Url($item->id,$opts), $html);
					}
				}else{
					//URL
					$html = str_replace('{{url ' . $item->id . '}}', $this->Url($item->id), $html);
					$html = str_replace('{{url ' . $item->k . '}}', $this->Url($item->id), $html);
				}
			}
			
			//TARGET BLANK
			$target = $item->target_blank ? ' target="_blank"' : '';
			$html = str_replace('{{urltarget ' .$item->id . '}}', $target, $html );
			$html = str_replace('{{urltarget ' .$item->k . '}}', $target, $html );
			
			//NOME PAGINE
			$html = str_replace('{{urlname ' . $item->id . '}}', $item->name, $html);
			$html = str_replace('{{urlname ' . $item->k . '}}', $item->name, $html);
			
			//AGGIUNGE CLASSE CSS "_fired" A URL CORRISPODNENTE ALLA PAGINA IN CORSO
			if( $item->id==$this->Page->id ){
				$str = " _fired";
				$str_class = ' class="_fired"';
			}else{
				$str = $str_class = "";
			}
			$html = str_replace('{{urlfired ' . $item->id . '}}', $str, $html);
			$html = str_replace('{{urlfired ' . $item->k . '}}', $str, $html);
			$html = str_replace('{{urlfired_class ' . $item->id . '}}', $str_class, $html);
			$html = str_replace('{{urlfired_class ' . $item->k . '}}', $str_class, $html);
		}
		
		return $html;
	}

	protected function pageNotExists($from=NULL){
		$page = $this->Page('home','k');
		header("Location: " . $this->Url($page->id) );exit;
	}

	protected function getLang($what='current',$par1=null){//$what -> current / default / id / url
		$q = "SELECT * FROM `^langs` WHERE";
		switch($what){
			case 'current':
				$q .= " id={$this->id_lang}";
				break;
			case 'default':
				$q .= " is_default=1 LIMIT 1";
				break;
			case 'id':
				$q .= " id={$par1}";
				break;
			case 'url':
				$q .= " url='{$par1}'";
				break;
		}
		$l = $this->cn->OQ($q);
		return $l=='-1' ? false : $l;
	}
	
	public function setCountry(){ //Inserire qui riconoscimento IP
		$Customer = $this->getCustomer();
		if( $Customer->id_country>0 ){
			$this->id_country = $Customer->BillingInfo->id_country;//Italia
			$q = "SELECT `code` FROM `^countries` WHERE id = {$this->id_country}";
			$this->iso_country = $this->cn->OF($q);
		}else{
			$this->id_country = 110;//Italia
			$this->iso_country = 'IT';
		}
	}

	protected function getUrlElements(){
		$_P = isset($_GET["_P"]) ? $_GET["_P"] : NULL;
		$url_elements = explode("/",$_P);
		for($new=array(),$i=0;$i<count($url_elements);$i++){
			if(strlen($url_elements[$i])>0){
				$new[] = $url_elements[$i];
			}
		}
		$url_elements = $new;
		unset($new,$i);
		return $url_elements;
	}

	protected function setPages(){
		$pages = array();
		$url_elements = $url_elements_page = $this->getUrlElements();
		
		 /* Recognize and set Page*/
		if( $this->lang_in_url ){
			for($new=array(),$i=1;$i<count($url_elements_page);$i++){
				$new[] = $url_elements_page[$i];
			}
			$url_elements_page = $new;
		}
		
		if( count($url_elements_page)>0 ){
			for($record_id=false,$id_parent=false, $level=0;$level<count($url_elements_page);$level++){
				$item_url = $url_elements_page[ $level ];
				
				if( preg_match("/^(.*)\.(\d+)/", $item_url, $matches) ){
					$record_id = array_pop($matches);
				}else{				
					$opts = new stdClass();
					if($id_parent){
						$opts->id_parent = $id_parent;
					}
					$p = $this->Page($item_url,'url',$opts);
					
					$id_parent = $p->id;
					$pages[] = $p;
				}
			}		
		}
		//echo "<pre>";print_r($pages);
		
		if( count($pages)==0 ){
			$p = $this->Page(0,'home');
			$pages[] = $p;
		}
		
		$this->Page = array_pop($pages);
		if( isset($record_id) && $record_id ){
			$this->Page->record_id = $record_id;
			switch($this->Page->k){
				case 'product-item':
					$x = $this->Product( $record_id );
					//$this->pr($x);
					if( $x!=-1 ){
						//#*brands*# #*type*# | #*pagemenu*# #*sex*# collezione #*season*# #*year*#
						
						$pagemenu = '';
						
						$sex = '';
						if( $x->sex=='U' ):
							$sex = $this->W('Man');
						elseif( $x->sex=='D' ):
							$sex = $this->W('Woman');
						elseif( $x->sex='B' ):
							$sex = $this->W('Children');
						endif;
						
						$season = '';
						if( $x->season==1 ){
							$season = 'Winter';	
						}elseif( $x->season==2 ){
							$season = 'Spring/Summer';
						}
						
						$replace = array(
							"brands" => $x->brand,
							"pagemenu" => $pagemenu,
							"type" => $x->type,
							"sex" => $sex,
							"season" => $season,
							"year" => '20' . $x->year,
							"barcode" => $x->barcode
						);
						$this->Page->title = $this->SeoParse($this->Page->title, $replace);
						$this->Page->keywords = $this->SeoParse($this->Page->keywords, $replace);
						$this->Page->description = $this->SeoParse($this->Page->description, $replace);
						//$this->pr($this->Page);
					}
					break;
			}
		}else if( $this->Page->k=='profilo-ordini-scheda' ){
			$id = $_GET['id'];					
			$this->Page->Record = $this->Order_getBasket($id);
			
			if( $this->id_customer != $this->Page->Record->customer_id){
				header("Location: " . $this->Url('profilo-ordini') );exit;
			}
		}
		
		$this->Page->auth_req = false;
		
		$this->pagesTree = $pages;
		
		unset($pages,$q,$q_base,$where,$new,$url_elements,$url_elements_page);
	}

	public function Page($id=NULL,$by='id',$opts=NULL){
		$id_lang = $opts->id_lang ? $opts->id_lang : $this->id_lang;
		
		$q_base = "SELECT
		p.*, pl.url, pl.title, pl.keywords, pl.description, pl.meta, pl.html
		FROM
		`^pages` p INNER JOIN `^pages_ln` pl ON p.id = pl.id_page AND pl.id_lang = {$id_lang}
		";
		
		if( $id=='home' || $by=='home'):
			$where = " WHERE p.is_home=1";
		elseif( $by=='id' ):
			$where = " WHERE p.id={$id}";
		elseif( $by=='k' ):
			$where = " WHERE p.`k` = '{$id}'";
		elseif( $by=='url' ):
			$where = " WHERE pl.`url` = '{$id}'";
		endif;
		
		$where .= " AND p.enable=1";
		
		if( isset($opts->id_parent) && $opts->id_parent){
			$where .= " AND p.id_parent = {$opts->id_parent}";
		}else{
			$where .= " AND p.id_parent IS NULL";
		}
		
		$q = $q_base . $where . " LIMIT 1";
		$o = $this->cn->OQ($q);
		if( $o=="-1" ){
			$o = false;
		}else{
			if( !empty($o->options) ){
				$opts = explode(",",$o->options);
				$opts_arr = array();
				foreach($opts as $opt){
					list($k,$v) = explode(":",$opt);
					$opts_arr[$k] = $v;
				}
				
				$o->options = $this->O($opts_arr);
			}
		}
		
		return $o;
	}
	
	protected function SeoParse($str,$replace){
		//#*pagemenu*#, #*brands*#, #*type*#
		foreach($replace as $search=>$rep){
			$str = str_replace("#*{$search}*#", $rep, $str );
		}
		return $str;
	}
	
	public function pathFile($id_file,$full=true){
		$q = "SELECT * FROM `{$this->_db_prefix}file` WHERE id = {$id_file}";
		$f = $this->cn->OQ($q);
		if( $f==-1 ){
			return false;
		}else{
			$pathfile = $full ? path_webroot : "";
			$subdir = substr($id_file,0,1);
			$pathfile .= "_public/file/{$subdir}/{$id_file}";
			$pathfile .= empty($f["ext"]) ? "" : ".{$f['ext']}";
			return $pathfile;
		}
	}
	
	protected function AbsoluteUrl(){
		$url = $this->DomainUrl() . $this->_path->root;
		return $url;
	}
	
	protected function DomainUrl(){
		if( _DEBUGMODE ){
			$url = 'http://';
		}else if( server=='live' && $this->force_https ){
			$url = 'https://';
		}else{
			$url = 'http://';
		}
		
		$url = $url . $_SERVER['HTTP_HOST'];
		
		return $url;
	}

    public function AltParse($txt){
        $txt = str_replace("&amp;","&",$txt);

        $txt = str_replace("&","&amp;",
                    str_replace('"','',
                        str_replace("'",'',$txt)
                    )
                );
        return $txt;
    }
	
	public function Url($id_page,$opts=NULL){
        $get_elements = array();
		if( $id_page=="this"){
			$id_page = $this->Page->id;
			if( $this->Page->k=='product-item' ){
				if( !$opts ){
					$opts = new stdClass();
				}
				$opts->type = 'product-item';
				$opts->id = $this->Page->record_id;
			}
		}
		
		$id_lang = $opts->id_lang>0 ? $opts->id_lang : $this->id_lang;
		$Lang = $this->Lang($id_lang);
				
		$url = array();
		if( is_string($id_page) && !(intval($id_page)>0) ){
			$q = "SELECT id FROM `^pages` WHERE `k`='{$id_page}'";
			$id_page = $this->cn->OF($q);	
		}

		$tmp = $id_page;
		$opts_page = new stdClass();
		$opts_page->id_lang = $id_lang;
		do{
			$p = $this->Page( $tmp, 'id', $opts_page);
			
			$url[] = $p->url;
			if( $p->id_parent>0 ){
				$flag = false;
				$tmp = $p->id_parent;
			}else{
				$flag = true;
			}
		}while(!$flag);
		
		if( $p->is_home && $Lang->is_default ){
			return $this->_path->root;
		}else if( $this->lang_in_url && $id_lang>0){
			$x = $this->Lang($id_lang);
			if( $x ){
				$url[] = $x->url;
			}
		}
		$url = array_reverse($url);
		
		if( $opts ){
			switch( $opts->type ){
				case "product-item":
					$x = $this->Product( $opts->id );
					if( $x!=-1 ){
						$tmpUrl = $this->strToUrl( "{$x->descrbase}-{$x->griffe}" );
						$tmpUrl .= ".{$opts->id}";
					}else{
						$tmpUrl = '#';
					}
					
					break;
                case "products":
                    if( $opts->ca ){
                        $url[] = 'ca.ca.' . $opts->ca;
                    }
                    if( $opts->gr ){
                        $url[] = $this->strToUrl( 'griffe' ) . '.gr.' . $opts->gr;
                    }

                    if( $opts->page ){
                        $get_elements[] = "page={$opts->page}";
                    }
                    if( $opts->sort ){
                        $get_elements[] = "sort={$opts->sort}";
                    }
                    if( $opts->range_price ){
                        $get_elements[] = "range_price={$opts->range_price}";
                    }

                    break;
			}
			
			$url[] = $tmpUrl;
		}
		
		
		$url = $this->_path->root . implode("/",$url);
		while(strpos($url,'//')){
			$url = str_replace('//','/',$url);
		}

		$url = $this->parseUrl($url);

        if( count($get_elements)>0 ){
            $url .= "?" . implode("&" , $get_elements);
        }

		return $url;
	}

	protected function parseUrl($url){
		$convert = array(
			"à" => "a",
			"è" => "e",
			"é" => "e",
			"ì" => "i",
			"ò" => "o",
			"ù" => "u",
			"€" => "e",
			"å" => "a",
			"ß" => "ss",
			"∂" => "d",
			"ƒ" => "f",
			"µ" => "m",
			"ç" => "c",
			"õ" => "o",
			"&" => "e"
		);
		$chars_allowed = "abcdefghijklmnopqrstuvwxyz";
		$chars_allowed .= strtoupper($chars_allowed);
		$chars_allowed .= "0123456789";
		$chars_allowed .= "-_.#=/";

		$chars_allowed = str_split($chars_allowed);

		foreach($convert as $from=>$to){
			$url = str_replace($from,$to,$url);
		}

		$url_arr = str_split($url);
		foreach($url_arr as $char){
			if( !in_array($char,$chars_allowed) ){
				$url = str_replace($char,"",$url);
			}
		}

		//Elimina spazi
		$url = str_replace(" ","-",$url);

		while( strpos($url,"--")!==false ){
			$url = str_replace("--","-",$url);
		}
		return $url;
	}
	
	public function Img($img,$type=false){
		if( !$img ){
			$img = 'img/foto_def.png';
		}else {
			if (strpos($img, '_public/') == 0) {
				$img = substr($img, strlen('_public/'));
			}
			if (!file_exists(__DIR__ . '/../../../_public/' . $img)) {
				$img = 'img/foto_def.png';
			}
		}

        $tmp = explode(".",$img);
        $ext = array_pop($tmp);
        $thumb = md5($img) . "-" . $ext;

        $path = $this->_path->root . 'img/' . $thumb;

        if($type){
            $opts = $this->images->$type;
            if($opts->w){
                $path .= "/w_{$opts->w}";
            }
            if($opts->h){
                $path .= "/h_{$opts->h}";
            }
            if($opts->m){
                $path .= "/m_{$opts->m}";
            }
            if($opts->wm){
                $path .= "/wm_{$opts->wm}";
            }
            if($opts->bg){
                $path .= "/bg_{$opts->bg}";
            }
        }
        $path .= "/f_" . $img;

        return $path;
	}
	
	protected function strToUrl($str){
		$str = strtolower($str);
		$allowed = str_split("abcdefghijklmnopqrstuvwxyz1234567890#?=&()$£-_");
		$chars = array(
			" " => "-",
			"_" => "-",
			"è" => "e",
			"é" => "e",
			"à" => "a",
			"ì" => "i",
			"ù" => "u",
			"ò" => "o"
		);
		foreach($chars as $from=>$to){
			$str = str_replace($from,$to,$str);
		}
		$str = str_split($str);
		$new_str = array();
		foreach($str as $c){
			if( in_array($c,$allowed) ){
				$new_str[] = $c;
			}
		}
		$str = implode("",$new_str);
		
		while( strpos($str,"--")>0 ){
			$str = str_replace("--","-",$str);
		}
		$str = trim($str);
		
		return $str;
	}
	
	public function Lang($id_lang=null){
		$id_lang = $id_lang>0 ? $id_lang : $this->id_lang;
		if( $id_lang>0 ){
			$q = "SELECT * FROM `^langs` WHERE id = {$id_lang} LIMIT 1";
			$url = $this->cn->OQ($q);
			return $url;
		}
		return false;
	}
	
	public function LoadTemplate(){
		$S = $this;
		$template = "{$this->_path->site}_ext/pages/{$this->Page->template}";
		if( is_file($template) && file_exists($template) && ( !isset($this->P) || $this->P!=-1) ){
			include($template);
		}else{
			header("Location: " . path_webroot);exit;
			//echo "Load template problem: <b>{$this->Page['template']}</b>";
		}	
	}
	
	public function LoadSchema(){
		$S = &$this;
		$UI = new UI($S);
		$schema = "{$this->_path->site}_ext/pages/{$this->Page->file_inc}";
		if( is_file($schema) && file_exists($schema) && $this->Page!="-1" ){
			include($schema);
		}else{
			echo "<!-- Schema {$this->Page->file_inc} not found -->";
		}
		unset( $UI );
	}
	
	public function Head(){
		$head = array();
		
		$home = $this->Page('home');
		
		$title = strlen($this->Page->title)>0 ? $this->Page->title : 'Accademia';
		$head[] = "<title>{$title}</title>";
		
		$tmp = strlen($this->Page->keywords)>0 ? $this->Page->keywords : $home->keywords;
		if( strlen($tmp)>0 ){
			$head[] = '<meta name="keywords" content="' . $tmp . '">';
		}
		
		$tmp = strlen($this->Page->description)>0 ? $this->Page->description : $home->description;
		if( strlen($tmp)>0 ){
			$head[] = '<meta name="description" content="' . $tmp . '">';
		}
		
		$responsive_on = $this->P("responsive_on");
		$responsive_on = $responsive_on=="1" ? true : false;
		if( $responsive_on ){
			$head[] = '<meta name="viewport" content="width=device-width, initial-scale=1.0">';	
		}
		
		//FAVICON
		if( file_exists( path_site . '_ext/img/favicons' )  ){
			$favicon_path = path_webroot . '_ext/img/favicons/';
			$head[] = '<link rel="shortcut icon" href="' . $favicon_path . 'favicon.ico">
			<link rel="apple-touch-icon" sizes="57x57" href="' . $favicon_path . 'apple-touch-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="114x114" href="' . $favicon_path . 'apple-touch-icon-114x114.png">
			<link rel="apple-touch-icon" sizes="72x72" href="' . $favicon_path . 'apple-touch-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="144x144" href="' . $favicon_path . 'apple-touch-icon-144x144.png">
			<link rel="apple-touch-icon" sizes="60x60" href="' . $favicon_path . 'apple-touch-icon-60x60.png">
			<link rel="apple-touch-icon" sizes="120x120" href="' . $favicon_path . 'apple-touch-icon-120x120.png">
			<link rel="apple-touch-icon" sizes="76x76" href="' . $favicon_path . 'apple-touch-icon-76x76.png">
			<link rel="apple-touch-icon" sizes="152x152" href="' . $favicon_path . 'apple-touch-icon-152x152.png">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-196x196.png" sizes="196x196">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-160x160.png" sizes="160x160">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-96x96.png" sizes="96x96">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-16x16.png" sizes="16x16">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-32x32.png" sizes="32x32">
			<meta name="msapplication-TileColor" content="#6550a2">
			<meta name="msapplication-TileImage" content="' . $favicon_path . 'mstile-144x144.png">
			<meta name="msapplication-config" content="' . $favicon_path . 'browserconfig.xml">';
		}
		$head[] = '<link rel="shortcut icon" href="' . path_webroot . 'favicon.ico">';
		//END FAVICON
		
		//SPECIFIC META PAGE
		if( !empty($S->Page->meta) ){
			$head[] = $S->Page->meta;
		}
		//END SPECIFIC META PAGE
		
		//Fonts
		$gfonts = $this->P('googlefonts');
		if( $gfonts ){
			$gfonts = explode("|",$gfonts);
			foreach($gfonts as $gfont){
				$head[] = '<link href="http://fonts.googleapis.com/css?family=' . $gfont . '" rel="stylesheet" type="text/css">';
			}
		}

		$arr = array('fontello/animation.css','fontello/fontello-codes.css','fontello/fontello.css','categorie/style.css');
		foreach($arr as $v){
			$head[] = '<link href="' . $this->_path->css . 'fonts/' . $v . '" rel="stylesheet" type="text/css">';
		}

		$external_css = array();
		if( server=='local' ){
			$external_css['bootstrap'] = $this->_path->css . 'bootstrap/css/bootstrap.min.css';
			$external_css['bootstrap_theme'] = $this->_path->css . 'bootstrap/css/bootstrap-theme.min.css';
			$external_css['font_awesome'] = $this->_path->css . 'font-awesome/css/font-awesome.min.css';
		}else{
			$external_css['bootstrap'] = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css';
			$external_css['bootstrap_theme'] = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css';
			$external_css['font_awesome'] = 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css';
		}

		$head[] = '<link href="' . $external_css['bootstrap'] . '" rel="stylesheet" media="screen">';
		//$head[] = '<link href="' . $external_css['bootstrap_theme'] . '" rel="stylesheet" media="screen">';
		$head[] = '<link href="' . $external_css['font_awesome'] . '" rel="stylesheet" media="screen">';

		//Plugin jQuery
		$head[] = '<link href="' . $this->_path->js . 'plugins/bxslider/jquery.bxslider.css" rel="stylesheet" media="screen">';
		$head[] = '<link href="' . $this->_path->js . 'plugins/fancybox/jquery.fancybox.css" rel="stylesheet" media="screen">';
		$head[] = '<link href="' . $this->_path->js . 'plugins/fancybox/helpers/jquery.fancybox-buttons.css" rel="stylesheet" media="screen">';
		$head[] = '<link href="' . $this->_path->js . 'plugins/fancybox/helpers/jquery.fancybox-thumbs.css" rel="stylesheet" media="screen">';
		$head[] = '<link href="' . $this->_path->js . 'plugins/venobox/venobox.css" rel="stylesheet" media="screen">';
		$head[] = '<link href="' . $this->_path->js . 'plugins/ion-rangeSlider/css/ion.rangeSlider.css" rel="stylesheet" media="screen">';
		$head[] = '<link href="' . $this->_path->js . 'plugins/ion-rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" media="screen">';
        $head[] = '<link href="' . $this->_path->js . 'plugins/custom-scrollbar/jquery.mCustomScrollbar.min.css" rel="stylesheet" media="screen">';
				//$head[] = '<link href="' . $this->_path->js . 'plugins/vex/css/vex.css" rel="stylesheet" media="screen">';
		//$head[] = '<link href="' . $this->_path->js . 'plugins/vex/css/vex-theme-default.css" rel="stylesheet" media="screen">';
		
		$head[] = '<link href="' . $this->_path->css . 'layout.css" rel="stylesheet" media="all">';
		if( $this->Page->css ){
			$css = explode(",",$this->Page->css);
			foreach($css as $item){
				if( file_exists('_ext/css/' . $item . '.css') ){
					$head[] = '<link href="' . $this->_path->css . $item . '.css" rel="stylesheet" media="all">';
				}
			}
		}

		//old IE
		if( $this->is_msie && $this->msie->version<=9 ){
			$head[] = '<link href="' . $this->_path->css . 'ie/lte9.css" rel="stylesheet" media="screen">';
			$head[] = '<link href="' . $this->_path->scripts . 'PIE/PIE.php" rel="stylesheet" media="screen">';
		}
		//END old IE
		
		//Versione di jQuery
		if( $this->is_msie && $this->msie->version<9 ){
			$head[] = '<!--[if lt IE 9]>
			<script src="' . $this->_path->js . 'plugins/html5shiv/html5shiv.min.js" type="text/javascript"></script>
			<script src="' . $this->_path->js . 'plugins/respond.min.js" type="text/javascript"></script>
			<![endif]-->';
			$jquery_ver = $this->P('jquery_ver1');
		}else{
			$jquery_ver = $this->P('jquery_ver2');
		}

		if( server=='local' ){
			$jquery_path = $this->_path->js . 'jquery-' . $jquery_ver . '.min.js';
		}else{
			$jquery_path = 'https://code.jquery.com/jquery-' . $jquery_ver . '.min.js';
		}
		$head[] = '<script type="text/javascript" src="' . $jquery_path . '"></script>';
		//End Versione di jQuery
				
		if( isset($this->Page->options->gmap) && $this->Page->options->gmap ){	
			$head[] = '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>';
			$head[] = '<script type="text/javascript" src="' . $this->_path->js . 'gmap.js"></script>';
		}
	
	
		$head[] = '';
		$head = implode("\n",$head);
		$head = str_replace("\t","",$head);
		return $head;
	}
	
	public function EndScript(){
		$end = array();
		$responsive_on = $this->P("responsive_on");
		$responsive_on = $responsive_on=="1" ? true : false;

		if( server=='local' ){
			$bootstrap = $this->_path->css . 'bootstrap/js/bootstrap.min.js';
		}else{
			$bootstrap = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js';
		}
		$end[] = '<script src="' . $bootstrap . '"></script>';
		
		$arr = array("jquery-easing","jquery-migrate","jquery-scrollto");
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/jquery-migrate-1.2.1.min.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/jquery.easing.1.3.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/jquery.mousewheel-3.0.6.pack.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/jquery.scrollTo.min.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/bxslider/jquery.bxslider.min.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/bxslider/plugins/jquery.fitvids.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/fancybox/jquery.fancybox.pack.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/fancybox/helpers/jquery.fancybox-buttons.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/fancybox/helpers/jquery.fancybox-media.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/fancybox/helpers/jquery.fancybox-thumbs.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/venobox/venobox.min.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/jquery.popupWindow.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/jquery.lazyload.min.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/ion-rangeSlider/ion.rangeSlider.min.js"></script>';
        $end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>';
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/jquery.zoom.min.js"></script>';

		//$end[] = '<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>';
		//old IE
		if( $this->is_msie && $this->msie->version<=9 ){
			$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'plugins/placeholders.min.js"></script>';
		}
		//END old IE
		
		$end[] = '<script type="text/javascript" src="' . $this->_path->js . 'main.js"></script>';

		$end[] = '<script type="text/javascript"><!--';
		$x = _DEBUGMODE ? 'true' : 'false';
		$end[] = 'var _DEBUGMODE = ' . $x . ';';
		$end[] = 'var path_webroot = "' . $this->_path->root .'";';
		$end[] = 'var absolute_url = "http://' . $_SERVER['HTTP_HOST'] . $this->_path->root . '";';
		$end[] = 'System.domain = \'' . $_SERVER['HTTP_HOST'] . '\';';
		$end[] = 'System.Page = {
			k: \'' . $this->Page->k . '\'
		};'; 
		/*foreach($this->_path as $k=>$v){
			$end[] = "var {$k} = '{$v}';";
		}*/
		if( isset($_GET['st']) && !empty($_GET['st']) ){
			$end[] = "System.st = '{$_GET['st']}';";
		}
		$end[] = "System.is_mobile = " . ($this->isMobile() ? 'true' : 'false') . ";";
		$end[] = '--></script>';

		$end[] = '';
		return implode("\n",$end);
	}

	public function HF($act_field=null){
		$hidden = 'hidden';
		//$hidden = 'text';
		
		$fields = array();
		if( $act_field ){
			$fields[] = '<input type="' . $hidden .'" name="act" value="' . $act_field . '">';	
		}
		$id_customer = $this->Customer->id>0 ? $this->Customer->id : 0;
		$fields[] = '<input type="' . $hidden .'" name="id_customer" value="' . $id_customer . '">';
		$fields[] = '<input type="' . $hidden .'" name="id_lang" value="' . $this->id_lang . '">';
		$fields[] = '<input type="' . $hidden .'" name="ip" value="' . $_SERVER["REMOTE_ADDR"] . '">';
		$fields[] = '<input type="' . $hidden .'" name="ua" value="' . $_SERVER["HTTP_USER_AGENT"] . '">';
		
		return implode("\r\n",$fields);
	}
	
	public $parameters = array();
	public function P($parameter,$id_country=false,$id_lang=false){ //parametri
		if( !isset($this->parameters[ md5($parameter) ]) ){
			$q = "SELECT `v`, `type` FROM `^parameters` WHERE `k`='{$parameter}'";
			if( $id_country ){
				$q .= " AND id_country={$id_country}";
			}
			if( $id_lang ){
				$q .= " AND id_lang={$id_lang}";
			}
			$ris = $this->cn->Q( $q );

			if( $this->cn->n==0 ){
				return false;
			}else if($this->cn->n>1){
				$ris = $this->P($parameter,$this->id_country);
				if( count($ris)>1 ){
					$ris = $this->P($parameter,false,$this->id_lang);
				}
			}else{
				$o = $ris[0];
				unset($ris);
			}

			switch($o->type){
				case "int":
					$this->parameters[ md5($parameter) ] = intval($o->v);
					break;
				case "float":
					$this->parameters[ md5($parameter) ] = floatval($o->v);
					break;
				case "boolean":
					$this->parameters[ md5($parameter) ] = $o->v=="1";
					break;
				default:
				case "text":
					$this->parameters[ md5($parameter) ] = $o->v;
					break;
			}
		}
		return $this->parameters[ md5($parameter) ];
	}
	
	public function Money($number,$decimal=2){//Implementare per futuro formattazione valuta
		return number_format($number,$decimal,",",".");
	}
	
	public function Date($data){//Implementare in futuro formattazione data
		return strftime("%d/%m/%Y", strtotime($data) );
	}
	
	public function Tax($k,$id_country=false,$is_vat=0){
		$q = "SELECT `tax` FROM `^tax` WHERE `k`='{$k}' AND enable_from <= NOW() AND is_vat = {$is_vat}";
		$q .= $id_country>0 ? " AND id_country = {$id_country}" : "";
		$q .= " ORDER BY enable_from DESC LIMIT 1";
		return $this->cn->OF($q);
	}
	public function Vat($k='vat',$id_country=false){
		return $this->Tax($k,$id_country,1);
	}
	
	/*public function MenuList($menu_k){
		$q = "SELECT p.k, pl.* FROM `^pages` p INNER JOIN `^pages_ln` pl ON p.id = pl.id_page AND pl.id_lang = {$this->id_lang} WHERE '{$menu_k}' IN p.menu_k AND p.enable=1 AND pl.menu_order IS NOT NULL";
		$list = $this->cn->Q($q);
		return $list;
	}*/
	
	public function MenuMobile($menu_k){
		$list = $this->MenuList($menu_k);
		$code = array();
		foreach($list as $l){
			$sel = $this->Page->id==$l->id_page ? ' selected' : '';
			$code[] = '<option value="' . $this->Url($l->id_page) . '"' . $sel . '>' . $l->menu . '</option>';
		}
		return implode("\n",$code);
	}

	protected function doHtml($html){
		$nh = $html;

		$nh = str_replace("\n","#*|n|*#",$nh);
		$nh = str_replace("\r","#*|r|*#",$nh);
		$nh = str_replace('<#',"\n<#",$nh);
		$nh = str_replace('#>',"#>\n",$nh);
		preg_match_all("/<# (.*) #>/", $nh, $out, PREG_PATTERN_ORDER);

		//$this->pr($out);
		for($i=0;$i<count($out[0]);$i++){
			$str = trim($out[1][$i]);
			$txt_q = addslashes($str);

			$replace = $this->W($txt_q);
			$nh = str_replace($out[0][$i],$replace,$nh);
		}
		$nh = str_replace("#*|n|*#","\n",$nh);
		$nh = str_replace("#*|r|*#","\r",$nh);
		return $nh;
	}
	
	protected function md5id($str){
		$str = trim(
				stripslashes($str)
			); 
		return md5($str);
	}
	
	public function W($word,$x=false){
		if($x){
			echo $word;ob_flush();
		}
		
		$strings_var = false;
		if( strpos($word,'%txt=')>0 ){
			list($word,$strings_var) = explode('%txt=',$word);
			$word = trim($word);
		}
		
		$dir = $this->_path->site . "_ext/scripts/langs/";
		$q = "SELECT * FROM `^langs` WHERE id = {$this->id_lang}";
		$lang = $this->cn->OQ($q);
		$file = $dir . "labels-{$lang->url}.inc.php";
		
		$id_word = $this->md5id($word);
		if( !is_int($word) && file_exists($file) ){
			include( $file );
			if( isset($_labels[ $id_word ] ) ){
				$txt = $_labels[ $id_word ];
			}
		}

		if( !$txt ){
			//echo $word;exit;
			//Lingua di default
			$q = "SELECT id FROM `^langs` WHERE is_default=1 LIMIT 1";
			$id_lang_def = $this->cn->OF($q);

			if( is_int($word) ){
				$q = "SELECT * FROM `^labels` WHERE id = '{$word}' LIMIT 1";
			}else{
				$q = "SELECT * FROM `^labels` WHERE id_parent IS NULL AND label = '" . addslashes($word) . "' LIMIT 1";
			}
			$row = $this->cn->OQ($q);
			$id_parent = $row==-1 ? false : $row->id;

			$not_found = false;
			if( $id_parent>0 ){
				$q = "SELECT `label` FROM `^labels` WHERE id_parent = {$id_parent} AND id_lang = {$this->id_lang} LIMIT 1";
				$txt = $this->cn->OF($q);
				if(!$txt){
					$txt = $word;
					$not_found = true;
				}
			}

			if( !is_int($word) || $not_found){
				$q = "INSERT INTO `^labels` (id_lang,`label`) VALUES ({$id_lang_def},'". addslashes($word) . "')";
				$this->cn->Q($q);
				$txt = $word;
			}
			//echo $word;print_r($row);exit;

			//Reiscrive il file delle lingue
			$this->WriteLabels( $this->id_lang );
		}
		
		if( $strings_var ){
			$strings = explode("|",$strings_var);
			$i=0;
			
			while( strpos($txt,'%s')!==false ){
				$pos = strpos($txt,'%s');
				$txt = substr_replace($txt, $strings[$i], $pos, 2);
				$i++;
			}
		}
		
		return stripslashes($txt);
	}

	public function WriteLabels($id_lang){
		$dir = __DIR__ . "/../langs/";
		$q = "SELECT * FROM `^langs` WHERE id = {$id_lang}";
		$lang = $this->cn->OQ($q);
		$file = $dir . "labels-{$lang->url}.inc.php";

		if( file_exists( $dir . 'lang.lock' ) ){
			return false;
		}

		if(!file_exists($dir)){
			mkdir($dir);
		}

		if( is_writable($dir) ){
			touch( $dir . 'lang.lock');
			$fp = fopen($file,"w");
			$q = "SELECT * FROM `^labels` WHERE id_lang = {$id_lang} AND id_parent IS NULL";
			$list = $this->cn->Q($q,true);
			$rows = array();
			$rows[] = '<?php';
			$rows[] = '$_labels = array();';
			foreach ($list as $item) {
				$id_word = $this->md5id( $item->label );
				$q = "SELECT `label` FROM `^labels` WHERE id_parent = {$item->id} AND id_lang = {$id_lang}";
				$w = $this->cn->OF($q);
				$w = $w ? $w : $item->label;
				$row = '$_labels[\'' . $id_word . '\'] = \'' . addslashes( $w ) . '\';';
				$rows[] = $row;
			}
			$rows[] = '?>';
			foreach($rows as $row){
				$row .= "\r\n";
				fwrite($fp, $row, strlen($row));
			}
			fclose($fp);
			unlink( $dir . 'lang.lock');
		}
	}
	
	public function pr($data,$exit=true){
		echo "<pre>";print_r($data);echo "</pre>";
		ob_flush();
		if( $exit )
			exit;
	}
	
	public function EndPage(){
		return "";
		$q = "SELECT * FROM `^pages` WHERE 1";
		$x = $this->cn->Q($q,true);
		$q = "SELECT * FROM asda WHERE 1";
		print_r($x);print_r($x);print_r($x);print_r($x);exit;
		$this->cn->Q($q);
	}
	
	public function RandomCode($n_chars=8,$chars_type="aAnS"){
		$chars = array(); //a=>alfabetico minuscolo, A=>alfabeto maiuscolo, n=>numeri, S=>caratteri speciali
		$chars["a"] = "abcdefghijklmopqrstuvwxyz";
		$chars["A"] = strtoupper($chars["a"]);
		$chars["n"] = "0123456789";
		$chars["S"] = "$!£%&/()=?^,.;:#*+";
		
		$Chars = "";
		$chars_type_arr = str_split($chars_type);
		foreach($chars as $k=>$v){
			if(in_array($k,$chars_type_arr)){
				$Chars .= $v;
			}
		}
	
		$n_chars = $n_chars<0 ? 8 : $n_chars;
		for($i=0,$code="";$i<$n_chars;$i++){
			$code .= substr($Chars, rand(0,strlen($Chars)-1), 1);
		}
		$code = trim($code);
		return $code;
	}
	
	public function sendMailLog($msg){
		$to = "roberto.webdeveloper@gmail.com";
		$to = "parrella@inol3.com";
		$this->sendMail($to,null,'test@domain.it',null,'Accademia B2B Log',$msg);
	}
	
	protected function footerMail($mailbody){
		$str = '&copy; Accademia srl
				<br>
				Via del Ferro, 137 - 59100 Prato &middot; PI 00318210978
				<a href="mailto:customercare@accademiaweb.com" style="color: #FFF;">customercare@accademiaweb.com</a>';
		$ret = str_replace("#*footermail*#", $str, $mailbody);
		return $ret;
	}
	
	public function sendInvitation($to,$nome_mittente,$ref_code){
		$subject = $this->W('Hai ricevuto un invito ad iscriverti su Accademia');
		//INVIO E-MAIL
		$mail = $this->getMailTemplate('invita.html');
		$mail = str_replace("#*img*#" , $this->DomainUrl() . '/_ext/mail/img/' , $mail);
		$mail = str_replace("#*mittente*#" , $nome_mittente , $mail);
		$mail = str_replace("#*url_invito*#" , $this->DomainUrl() . '?ref_code=' . $ref_code , $mail);
		$importo = $this->P('invitation_bonus');
		$importo = '&euro; ' . $this->Money($importo);
		$mail = str_replace("#*importo*#" , $importo , $mail);
		
		$this->sendMail($to, null, 'no-reply@accademiaweb','Accademia',$subject,$mail);
	}

	public function test(){
		$mail = $this->getMailTemplate( 'registrazione.html' );
		$mail = str_replace("#*company*#" , "Ragione sociale" , $mail);				
		$mail = str_replace("#*url_attivazione*#" , $this->DomainUrl() . $this->Url('home') . "?reg_code=" . sha1(sha1($reg_code)), $mail);
		$mail = str_replace("#*email*#" , 'email@email.it' , $mail);

		$subject = $this->W('Conferma la tua E-Mail');
		$this->sendMail("parrella@inol3.com", "Ragione sociale", "no-reply@accademiaweb.com","Accademia", "Benvenuto in Accademia" , $mail);
	}

	public function getMailTemplate($template){
		$bodymail = file_get_contents( $this->_path->mail_lang . $template);
		/*
		 * PARSING CORPO E-MAIL
		 */

		//Include dei file
		preg_match_all("/#*include (.*)\*#/i", $bodymail , $includes , PREG_PATTERN_ORDER);
		if( count($includes[1])>0 ){
			//$this->pr($includes);
			foreach($includes[1] as $include){
				if( file_exists( $this->_path->mail_lang . $include ) ){
					$inc = file_get_contents( $this->_path->mail_lang . $include );

				}else{
					$inc = '';
				}
				$bodymail = str_replace("#*include {$include}*#",$inc,$bodymail);
			}
		}

		//Sostituisce il path delle immagini
		$bodymail = str_replace("#*img*#" , $this->DomainUrl() . path_webroot . '_ext/mail/img/', $bodymail);
		$bodymail = $this->footerMail($bodymail);
		/*
		 * FINE PARSING CORPO E-MAIL
		 */

		return $bodymail;
	}
	
	public $noBccMain = false;
	public function sendMail($to,$to_name=null,$from,$from_name=null,$subject,$bodymail,$textmail=null,$bcc=null,$attachments=null,$images=null){
		$to = explode(",",$to);
		$to_name = explode(",",$to_name);
		if($bcc){
			$bcc = explode(",",$bcc);
		}else{
			$bcc = array();
		}
		if( server=='live' && !$this->noBccMain ){
			//$bcc[] = "sottosotto@inol3.com";
			$this->noBccMain = false;
		}

		if( empty($bodymail) )
			return false;
		
		if( $this->mailsender=='mandrill'):
			$mandrill_apikey = $this->P('mandrill-apikey');
			$mandrill = new Mandrill( $mandrill_apikey );

			$to_arr = array();
			for($i=0;$i<count($to);$i++){
				$to_arr[] = array(
					'email' => $to[$i],
					'name' => $to_name[$i],
					'type' => 'to'
				);
			}
			for($i=0;$i<count($bcc);$i++){
				$to_arr[] = array(
					'email' => $bcc[$i],
					'name' => $bcc[$i],
					'type' => 'bcc'
				);
			}
	
			try {
				
			    $message = array(
			        'html' => $bodymail,
			        'text' => $textmail,
			        'subject' => $subject,
			        'from_email' => $from,
			        'from_name' => $from_name,
			        'to' => $to_arr,
			        'headers' => array('Reply-To' => $from ),
			        'important' => false,
			        'track_opens' => null,
			        'track_clicks' => null,
			        'auto_text' => null,
			        'auto_html' => null,
			        'inline_css' => null,
			        'url_strip_qs' => null,
			        'preserve_recipients' => true,
			        'view_content_link' => null,
			        'bcc_address' => null,//implode(',', $bcc),
			        'tracking_domain' => null,
			        'signing_domain' => null,
			        'return_path_domain' => null,
			        'merge' => false,
			        'merge_language' => 'mailchimp',
			        'global_merge_vars' => null,
			        'merge_vars' => null,
			        'tags' => null,
			        'subaccount' => null,
			        'google_analytics_domains' => array('accademiaweb.com'),
			        'google_analytics_campaign' => 'message.from_email@example.com',
			        'metadata' => array('website' => 'www.accademiaweb.com'),
			        'recipient_metadata' => null,
			        'attachments' => $attachments,
			        'images' => $images
			    );
				
			    $async = false;
			    $ip_pool = 'Main Pool';
			    $send_at =  date('d m Y' , time() );
			    $result = $mandrill->messages->send($message, $async, $ip_pool);
			} catch(Mandrill_Error $e) {
			    // Mandrill errors are thrown as exceptions
			    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				mail("parrella@inol3.com" , "A mandrill error",$e->getMessage() );
			    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			    throw $e;
			}	
		else:		
			$oMail = new PHPMailer();
			$oMail->isHTML(true);
			$oMail->Subject = $subject;
			$oMail->setFrom($from,$from_name);
			
			for($i=0;$i<count($to);$i++){
				$name_tmp = !empty($to_name[$i]) ? $to_name[$i] : null;
				$oMail->addAddress($to[$i],$name_tmp);	
			}
			if($bcc){
				foreach($bcc as $tmp){
					$oMail->addBCC($tmp);
				}
			}
			$oMail->Body = $bodymail;
			if($textmail){
				$oMail->AltBody = $textmail;
			}
			
			$res = $oMail->send();
			
			unset($oMail);
		endif;
		
		return $res;
	}
	
	public function sendMail_mandrill($to,$to_name=null,$from,$from_name=null,$subject,$bodymail,$textmail=null,$bcc=null,$attachments=null,$images=null){
		$to = explode(",",$to);
		$to_name = explode(",",$to_name);
		$bcc = $bcc ? explode(",",$bcc) : false;
		
		$mandrill = new Mandrill( mandrill_key );
		
		$to_arr = array();
		for($i=0;$i<count($to);$i++){
			$to_arr[] = array(
				'email' => $to[$i],
				'name' => $to_name[$i],
				'type' => 'to'
			);
		}

		try {
		    $message = array(
		        'html' => $bodymail,
		        'text' => $textmail,
		        'subject' => $subject,
		        'from_email' => $from,
		        'from_name' => $from_name,
		        'to' => $to_arr,
		        'headers' => array('Reply-To' => $from ),
		        'important' => false,
		        'track_opens' => null,
		        'track_clicks' => null,
		        'auto_text' => null,
		        'auto_html' => null,
		        'inline_css' => null,
		        'url_strip_qs' => null,
		        'preserve_recipients' => null,
		        'view_content_link' => null,
		        'bcc_address' => $bcc,
		        'tracking_domain' => null,
		        'signing_domain' => null,
		        'return_path_domain' => null,
		        'merge' => false,
		        'merge_language' => 'mailchimp',
		        'global_merge_vars' => null,
		        'merge_vars' => null,
		        'tags' => null,
		        'subaccount' => null,
		        'google_analytics_domains' => array('sottosotto.it'),
		        'google_analytics_campaign' => 'message.from_email@example.com',
		        'metadata' => array('website' => 'www.sottosotto.it'),
		        'recipient_metadata' => null,
		        'attachments' => $attachments,
		        'images' => $images
		    );
			
		    $async = false;
		    $ip_pool = 'Main Pool';
		    $send_at =  date('d m Y' , time() );
		    $result = $mandrill->messages->send($message, $async, $ip_pool);
		} catch(Mandrill_Error $e) {
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		    throw $e;
		}
		
		unset($mandrill);
		return $result;
	}

	public function getCountry($id=false,$code=false){
		$q = "SELECT * FROM `^countries` c WHERE ";
		$q .= $id>0 ? " id={$id}" : " `code`='{$code}'";
		$C = $this->cn->OQ($q);
		
		if( $C!="-1" ){
			$q = "SELECT cc.main, cur.* FROM `^countries_currencies_br` cc INNER JOIN `^currencies` cur ON cc.id_currency = cur.id AND cc.id_country={$C->id} WHERE 1";
			$C->currencies = $this->cn->Q($q);
			$q = "SELECT cl.main, l.* FROM `^countries_lang_br` cl INNER JOIN `^langs` l ON cl.id_lang = l.id AND cl.id_country={$C->id} WHERE 1";
			$C->langs = $this->cn->Q($q);
			
			return $C;
		}else{
			return false;
		}
	}

	public function CountryList($filters="all"){
		if( !isset($this->CountryList_tmp) ){
			$this->CountryList_tmp = array();
		}
		
		if( !isset($this->CountryList_tmp[ $filters ]) ){
			$q = "SELECT * FROM `^countries` WHERE enable=1";
			if( $filters!="all" ){
				$filters_arr = explode(",",$filters);
				if( in_array("sell_enable",$filters_arr) ){
					$q .= " AND sell_enable=1";
				}
			}
			$q .= " ORDER BY name ASC";
			$this->CountryList_tmp[$filters] = $this->cn->Q($q);
		}
		return $this->CountryList_tmp[ $filters ];
	}

	public function isMobile(){
		return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
	}
}
?>