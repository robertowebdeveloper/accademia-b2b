<?php
class Products extends Customers{
    public function CategorieList( $only_full=false ){//$only_full=true -> mostra solo le tipologie che hanno almeno un prodtto
        $q = "SELECT ca.* FROM
		`^ext_categorie_articoli` AS ca";

        if( $only_full ){
            $q .= "
            INNER JOIN `^ext_articoli` AS a ON a.art_cat_stat = ca.codice

			WHERE a.deleted IS NULL AND a.giacenza>0";
        }else{
            $q .= " WHERE 1";
        }

        $q .= " GROUP BY ca.codice ORDER BY `codice` ASC";

        $list = $this->cn->Q($q);
        return $list;
    }

    public function CampagneList( $only_full=false ){//$only_full=true -> mostra solo le tipologie che hanno almeno un prodtto
        $q = "SELECT cam.*, CONCAT( cam.codice_group, ' ', YEAR(cam.data_uscita) ) AS codgroupyear FROM
		`^ext_campagne` AS cam";

        if( $only_full ){
            $q .= "
            INNER JOIN `^ext_articoli` AS a ON a.art_campagna = cam.codice

			WHERE a.deleted IS NULL AND a.giacenza>0
			    AND
			    cam.data_uscita <= NOW()
			    AND
			    YEAR(cam.data_uscita) >= YEAR( DATE_ADD(NOW(), INTERVAL -1 YEAR) )";
        }else{
            $q .= " WHERE 1";
        }

        $q .= " GROUP BY `codgroupyear` ORDER BY codice_group ASC, data_uscita DESC";

        $list = $this->cn->Q($q);
        return $list;
    }

    public function MarchiList( $only_full=false ){
        $q = "SELECT ma.* FROM
		`^ext_marchi` AS ma";

        if( $only_full ){
            $q .= "
				INNER JOIN `^ext_articoli` AS a ON a.art_marchio = ma.codice
				WHERE a.deleted IS NULL AND a.giacenza>0
			";
        }else{
            $q .= " WHERE 1";
        }

        $q .= "
			GROUP BY ma.codice
			ORDER BY `descrizione` ASC";

        $list = $this->cn->Q($q);
        return $list;
    }

    public function PriceRange(){
        $q = "SELECT MIN(a.prezzo) AS min, MAX(a.prezzo) AS max
		FROM `^e_articoli` AS a
		WHERE a.status = 1 AND a.deleted IS NULL";
        $prices = $this->cn->OQ($q);
        $prices->min = $prices->min>0 ? $prices->min : 0;
        $prices->max = $prices->max>0 ? $prices->max : 0;

        return $prices;
    }

    public $ProductLists_4_page = 16;
    public function ProductsList($get_search){
        $q_init_tot = "SELECT COUNT(DISTINCT a.id_art) AS n";

        $if_price = "IF( a.pvp>0 AND a.sconto>0,

            a.prezzo - (a.prezzo * (( ( (100 - a.pvp)*a.sconto/100) + a.pvp)/100)),

            IF( a.pvp>0,
                a.prezzo - ( a.prezzo * a.pvp / 100 ),
                IF( a.sconto>0,
                    a.prezzo - (a.prezzo * a.sconto / 100),
                    a.prezzo
                )
            )
        )";

        $q_init_single = "SELECT a.*, a.id_art AS id,

        :price AS prezzo,
        IF( a.pvp>0 OR a.sconto>0, a.prezzo, 0 ) AS prezzo_full,


		ma.descrizione AS marchio,
		cam.descrizione AS campagna,
		ca.descrizione AS categoria,
		foto.id AS main_foto,
		foto_retro.id AS retro_foto

		";

        $q_init = "SELECT a.*, a.id_art AS id, :price AS prezzo";

        $q_common_single = " FROM
		`^ext_articoli` AS a
		INNER JOIN `^ext_marchi` AS ma ON a.art_marchio = ma.codice

		INNER JOIN `^ext_categorie_articoli` AS ca ON a.art_cat_stat = ca.codice

		INNER JOIN `^ext_linea` AS li ON a.art_linea = li.codice";

        $q_common = " FROM
		`^ext_articoli` AS a";

		//LEFT JOIN `^articoli_foto` AS foto ON a.codicebase = foto.barcode AND foto.cod_colore = a.colore AND foto.tipo=1 AND foto.deleted IS NULL
		//LEFT JOIN `^articoli_foto` AS foto_retro ON a.codicebase = foto_retro.barcode AND foto_retro.cod_colore = a.colore AND foto_retro.tipo=2 AND foto_retro.deleted IS NULL

		$q_common .= " WHERE a.deleted IS NULL";
        $q_common_single .= " WHERE a.deleted IS NULL";

        /*if( !isset($get_search['no-status']) ){ //Se "no-status" è configurato filtra anche i prodotti per status = 0 (necessario per storico ordine, creazione di XML per sam, ecc...).
            $q_common .= " AND a.status!=0";
        }*/

        if( $get_search['id'] ) {
            $id_search = true;
            $q_common_single .= " AND a.idart = {$get_search['id']} LIMIT 1";

            $q = $q_init_single . $q_common_single;
            //echo $this->cn->TablePrefix($q);exit;
            $ret = $this->cn->OQ($q);

            //Foto
            /*$q = "SELECT * FROM `^articoli_foto` AS foto WHERE foto.barcode = '{$ret->codicebase}' AND foto.deleted IS NULL ORDER BY foto.tipo ASC";
            $foto = $this->cn->Q($q);
            $ret->foto = array();
            foreach($foto as $f){
                //$md5 = md5( $f->id );
                $filename = $this->ProductPathFoto( $f->id );

                if( file_exists( __DIR__ . "/../../../{$filename}") ){
                    $f->path = $filename;
                    $f->thumb = $this->Img( $f->path , 'product-thumb');
                    $f->medium = $this->Img( $f->path , 'product-medium');
                    $f->big = $this->Img( $f->path , 'product-big');
                    $ret->foto[] = $f;
                }
            }*/

            //$this->pr($ret);
        }else{
            $id_search = false;
            $having = ' HAVING 1';

            $show_taglia = "all";
            if (!empty($get_search['ca'])) {
                $q_common .= " AND a.art_cat_stat = '{$get_search['ca']}'";
            }
            if (!empty($get_search['ma'])) {
                $q_common .= " AND a.art_marchio = '{$get_search['ma']}'";
            }
            if (!empty($get_search['cam'])) {
                $q_common .= " AND a.art_campagna = '{$get_search['cam']}'";
            }
            if (!empty($get_search['range_price'])) {
                list($min, $max) = explode(";", $get_search['range_price']);
                //$q_common .= " AND '{$min}' <= :price AND :price <= '{$max}'";
                //$having .= " AND '{$min}' <= prezzo AND prezzo <= '{$max}'";
            }
            if ($get_search["not_id"] > 0) {
                $q_common .= " AND a.id!={$get_search['not_id']}";
            }
            if (isset($get_search['so']) && $get_search['so'] == 1) {//Special offers
                $q_common .= " AND (a.sconto>0 OR a.pvp>0)";
            }

            if (isset($get_search['s']) && !empty($get_search['s'])) {
                $str = $get_search['s'];
                $q_common .= "
                    AND (
                        a.descrizione LIKE '%{$str}%'
                        OR
                        ma.descrizione LIKE '%{$str}%'
                    )
                ";

                $q = "INSERT INTO `^search_log` (`search`,id_customer) VALUES ('" . addslashes($str) . "', {$this->id_customer})";
                $this->cn->Q($q);
            }

            //$group = " GROUP BY a.codicebase";
            $group = '';

            $order = " ORDER BY";
            list($orderBy,$orderAsc) = explode(",",$get_search['sort']);
            $orderAsc = strtolower($orderAsc)=="asc" ? "asc" : "desc";
            if( $orderBy=='price' ):
                $order .= " a.prezzo {$orderAsc}";
            elseif( $orderBy=="descr" ):
                $order .= " descrizione {$orderAsc}";
            elseif( $orderBy=="rand" ):
                $order .= " RAND()";
            else:
                $order .= " a.id_art DESC";
            endif;

            $q = $q_init_tot . $q_common;
            $q = str_replace(":price",$if_price,$q);
            //echo $this->cn->TablePrefix($q);exit;
            $prod_tot = $this->cn->OF($q);
            $q = $q_init . $q_common . $group . $having;
            $q = str_replace(":price",$if_price,$q);
            //echo $this->cn->TablePrefix($q);exit;
            $ris = $this->cn->Q($q,false);
            $prod_tot =$this->cn->n;
            $page_max = ceil($prod_tot/$this->ProductLists_4_page);

            $page = $get_search["page"]>=0 ? $get_search['page'] : 0;
            $page = $page>$page_max ? $page_max : $page;

            //Oggetto di ritorno
            $ret = new stdClass();
            $ret->marchi = $ret->categorie = $ret->campagne = array();
            //Codici campagne
            $q = "SELECT a.art_campagna " . $q_common . " GROUP BY art_campagna" . $having;
            $tmp = $this->cn->Q($q);
            if( $this->cn->n>0 ){
                foreach($tmp as $t){
                    $ret->campagne[] = $t->art_campagna;
                }
            }
            //Codici marchi
            $q = "SELECT a.art_marchio " . $q_common . " GROUP BY art_marchio" . $having;
            $tmp = $this->cn->Q($q);
            if( $this->cn->n>0 ){
                foreach($tmp as $t) {
                    $ret->marchi[] = $t->art_marchio;
                }
            }
            //Codici categorie
            $q = "SELECT a.art_cat_stat " . $q_common . " GROUP BY art_cat_stat" . $having;
            $tmp = $this->cn->Q($q);
            if( $this->cn->n>0 ){
                foreach($tmp as $t) {
                    $ret->categorie[] = $t->art_cat_stat;
                }
            }

            if( !$id_search ) {
                if (isset($get_search['limit'])) {
                    $limit = " LIMIT {$get_search['limit']}";
                } else {
                    $lim1 = $page * $this->ProductLists_4_page;
                    $limit = " LIMIT {$lim1},{$this->ProductLists_4_page}";
                }
            }

            $q = $q_init . $q_common . $group . $having . $order . $limit;
            $q = str_replace(":price",$if_price,$q);
            //echo $this->cn->TablePrefix($q);exit;
            $list = $this->cn->Q($q);

            $ret->list = $list;
            $ret->n = count($list);
            $ret->tot = $prod_tot;
            $ret->page = $page;
            $ret->page_max = $page_max;
        }

        return $ret;
    }
    public function ProductsList_old($get_search){
        $q_init_tot = "SELECT COUNT(DISTINCT a.idart) AS n";
        $q_init_single = "SELECT a.*, a.idart AS id, al.descrbase AS descrbase,
		IF( a.sconto>0,
		    a.prezzo - (a.prezzo * a.sconto/100),
		    a.prezzo
		) AS prezzo,
		IF( a.sconto>0, a.prezzo, 0) AS prezzo_full,
		ca.show_taglia,

		gr.descr AS griffe,
		IF(cal.descr='',ca.descr,cal.descr) AS categoria,
		IF(mal.descr='',ma.descr,mal.descr) AS materiale,
		IF(stl.descr='',st.descr,stl.descr) AS stampa,
		IF(lipl.descr='',lip.descr,lipl.descr) AS lineap,
		IF(cll.descr='',cl.descr,cll.descr) AS stagione,
		foto.id AS main_foto,
		foto_retro.id AS retro_foto

		";
		
		$q_init = "SELECT a.idart AS id, a.codgriffe, a.codcatomo";

        $q_common = " FROM
		`^e_articoli` AS a LEFT JOIN `^e_articoli_ln` AS al ON a.idart = al.idart AND al.id_lang = {$this->id_lang}
		INNER JOIN `^e_griffe` AS gr ON a.codgriffe = gr.codice

		INNER JOIN `^e_catomo` AS ca ON a.codcatomo = ca.codice
		LEFT JOIN `^e_catomo_ln` AS cal ON ca.codice = cal.codice AND cal.id_lang = {$this->id_lang}

		INNER JOIN `^e_materiale` AS ma ON a.codmateriale = ma.codice
		LEFT JOIN `^e_materiale_ln` AS mal ON ma.codice = mal.codice AND mal.id_lang = {$this->id_lang}

		INNER JOIN `^e_stampa` AS st ON a.codstampa = st.codice
		LEFT JOIN `^e_stampa_ln` AS stl ON st.codice = stl.codice AND stl.id_lang = {$this->id_lang}

		INNER JOIN `^e_lineap` AS lip ON a.codlineap = lip.codice
		LEFT JOIN `^e_lineap_ln` AS lipl ON a.codlineap = lipl.codice AND lipl.id_lang = {$this->id_lang}

		INNER JOIN `^e_classe` AS cl ON a.codstagione = cl.codice
		LEFT JOIN `^e_classe_ln` AS cll ON cl.codice = cll.codice AND cll.id_lang = {$this->id_lang}

		LEFT JOIN `^articoli_foto` AS foto ON a.codicebase = foto.barcode AND foto.cod_colore = a.colore AND foto.tipo=1 AND foto.deleted IS NULL
		LEFT JOIN `^articoli_foto` AS foto_retro ON a.codicebase = foto_retro.barcode AND foto_retro.cod_colore = a.colore AND foto_retro.tipo=2 AND foto_retro.deleted IS NULL

		WHERE a.id_parent IS NULL AND a.deleted IS NULL AND gr.deleted IS NULL AND gr.show=1";

        if( !isset($get_search['no-status']) ){ //Se "no-status" è configurato filtra anche i prodotti per status = 0 (necessario per storico ordine, creazione di XML per sam, ecc...).
            $q_common .= " AND a.status!=0";
        }

        $q_common .= $this->Customer && $this->Customer->tester ? '' : ' AND gr.test=0';

        if( $this->Customer && $this->Customer->griffe_disallowed_str ){
            $q_common .= " AND gr.id NOT IN ({$this->Customer->griffe_disallowed_str})";
        }
        //$get_search['id'] = 1259430;
        if( $get_search['id'] ) {
            $id_search = true;
            $q_common .= " AND a.idart = {$get_search['id']} LIMIT 1";

            $q = $q_init_single . $q_common;
            //echo $this->cn->TablePrefix($q);exit;
            $ret = $this->cn->OQ($q);

            //Altri prodotti collegati con stesso CODICEBASE / TAGLIA /  COLORE (Servono solo per la quantità totale)
            $q = "SELECT idart,giacenza FROM `^e_articoli` WHERE id_parent = {$get_search['id']}";
            $ret->children = $this->cn->Q($q);

            //Taglie
            if( isset($get_search['product_detail']) && $get_search['product_detail']==1 ) {
                $q = "SELECT ta.* FROM `^e_taglie` AS ta INNER JOIN `^e_articoli` AS a ON ta.codice = a.taglia WHERE a.codicebase = '{$ret->codicebase}' GROUP BY a.taglia ORDER BY ta.taord ASC";
                $ret->taglie = $this->cn->Q($q);
            }
            //Calcola i colori per le taglie
            if( isset($get_search['product_detail']) && $get_search['product_detail']==1 && count($ret->taglie)>0 ) {
                foreach ($ret->taglie AS $k => $ta) {
                    $q = "SELECT co.*, IF(col.descr='',co.descr,col.descr) AS descr
                      FROM
                      `^e_colori` AS co LEFT JOIN `^e_colori_ln` AS col ON co.codice = col.codice AND col.id_lang = {$this->id_lang}
                      INNER JOIN `^e_articoli` AS a ON a.colore = co.codice
                      WHERE a.codicebase = '{$ret->codicebase}' AND a.taglia = '{$ta->codice}'
                      GROUP BY a.colore
                       ORDER BY co.coord ASC
                      ";
                    $ret->taglie[$k]->colori = $this->cn->Q($q);
                }
            }

            //Colori
            $q = "SELECT co.*, IF(col.descr='',co.descr,col.descr) AS descr
                  FROM
                  `^e_colori` AS co LEFT JOIN `^e_colori_ln` AS col ON co.codice = col.codice AND col.id_lang = {$this->id_lang}
                  INNER JOIN `^e_articoli` AS a ON a.colore = co.codice
                  WHERE a.codicebase = '{$ret->codicebase}'
                  GROUP BY a.colore
                   ORDER BY co.coord ASC
                  ";
            $ret->colori = $this->cn->Q($q);
            //Taglie per i colori
            if( isset($get_search['product_detail']) && $get_search['product_detail']==1 && count($ret->colori)>0 ) {
                foreach ($ret->colori as $k => $co) {
                    $q = "SELECT ta.* FROM `^e_taglie` AS ta INNER JOIN `^e_articoli` AS a ON ta.codice = a.taglia WHERE a.codicebase = '{$ret->codicebase}' AND a.colore='{$co->codice}' GROUP BY a.taglia ORDER BY ta.taord ASC";
                    $ret->colori[$k]->taglie = $this->cn->Q($q);
                }
            }

            //Foto
            $q = "SELECT * FROM `^articoli_foto` AS foto WHERE foto.barcode = '{$ret->codicebase}' AND foto.deleted IS NULL ORDER BY foto.tipo ASC";
            $foto = $this->cn->Q($q);
            $ret->foto = array();
            foreach($foto as $f){
                //$md5 = md5( $f->id );
                $filename = $this->ProductPathFoto( $f->id );

                if( file_exists( __DIR__ . "/../../../{$filename}") ){
                    $f->path = $filename;
                    $f->thumb = $this->Img( $f->path , 'product-thumb');
                    $f->medium = $this->Img( $f->path , 'product-medium');
                    $f->big = $this->Img( $f->path , 'product-big');
                    $ret->foto[] = $f;
                }
            }

            //$this->pr($ret);
        }else{
            $id_search = false;

            $show_taglia = "all";
            if (!empty($get_search['ca'])) {
                $q_common .= " AND a.codcatomo = '{$get_search['ca']}'";

                //Calcola se la categoria ha la visualizzazione delle taglia
                $q = "SELECT show_taglia FROM `^e_catomo` WHERE codice = '{$get_search['ca']}'";
                $show_taglia = $this->cn->OF($q);
            }
            if (!empty($get_search['gr'])) {
                $q_common .= " AND a.codgriffe = '{$get_search['gr']}'";
            }
            if( !empty($get_search['lineap']) ){
                $q_common .= " AND a.codlineap = '{$get_search['lineap']}'";
            }
            if (!empty($get_search['range_price'])) {
                list($min, $max) = explode(";", $get_search['range_price']);
                $q_common .= "
                    AND IF(
                        a.sconto>0,
                        {$min} <= ( a.prezzo - (a.prezzo * a.sconto/100) ) AND ( a.prezzo - (a.prezzo * a.sconto/100) ) <= {$max},
                        {$min} <= a.prezzo AND a.prezzo <= {$max}
                    )";
            }
            if ($get_search["not_id"] > 0) {
                $q_common .= " AND a.id!={$get_search['not_id']}";
            }
            if (isset($get_search['so']) && $get_search['so'] == 1) {//Special offers
                $q_common .= " AND a.sconto>0";
            }
            if( is_array($get_search['taglie']) && count($get_search['taglie'])>0 ){
                $q_common .= " AND a.taglia IN (" . implode(",",$get_search['taglie']) . ")";
            }
            if( is_array($get_search['tagliegr']) && count($get_search['tagliegr'])>0 ){
                $q_common .= " AND a.taglia IN ( SELECT codice FROM `^e_taglie` WHERE id_group IN (" . implode("," , $get_search['tagliegr']) . ") )";
            }

            if (isset($get_search['s']) && !empty($get_search['s'])) {
                $str = $get_search['s'];
                $q_common .= "
                    AND (
                        a.descrbase LIKE '%{$str}%'
                        OR
                        al.descrbase LIKE '%{$str}%'
                        OR
                        gr.descr LIKE '%{$str}%'
                    )
                ";

                $q = "INSERT INTO `^search_log` (`search`,id_customer) VALUES ('" . addslashes($str) . "', {$this->id_customer})";
                $this->cn->Q($q);
            }

            $group = " GROUP BY a.codicebase";

            $order = " ORDER BY";
            list($orderBy,$orderAsc) = explode(",",$get_search['sort']);
            $orderAsc = strtolower($orderAsc)=="asc" ? "asc" : "desc";
            if( $orderBy=='price' ):
                $order .= " a.prezzo {$orderAsc}";
            elseif( $orderBy=="descr" ):
                $order .= " griffe {$orderAsc}";
            elseif( $orderBy=="rand" ):
                $order .= " RAND()";
            else:
                $order .= " a.idart DESC";
            endif;

            $q = $q_init_tot . $q_common;
            //echo $this->cn->TablePrefix($q);exit;
            $prod_tot = $this->cn->OF($q);
            $q = $q_init . $q_common . $group;
            $list_tot = $this->cn->Q($q);
            $prod_tot =$this->cn->n;
            $page_max = ceil($prod_tot/$this->ProductLists_4_page);

            $page = $get_search["page"]>=0 ? $get_search['page'] : 0;
            $page = $page>$page_max ? $page_max : $page;

            if( !$id_search ) {
                if (isset($get_search['limit'])) {
                    $limit = " LIMIT {$get_search['limit']}";
                } else {
                    $lim1 = $page * $this->ProductLists_4_page;
                    $limit = " LIMIT {$lim1},{$this->ProductLists_4_page}";
                }
            }

            $q = $q_init . $q_common . $group . $order . $limit;
            //echo $this->cn->TablePrefix($q);exit;
            $list = $this->cn->Q($q);

            $ret = new stdClass();

            //Elenca gli id griffe della ricerca:
            $ret->griffe = $ret->catomo = array();
            foreach($list_tot as $item){
                if( !in_array( $item->codgriffe,$ret->griffe ) ){
                    $ret->griffe[] = $item->codgriffe;
                }
                if( !in_array( $item->codcatomo , $ret->catomo ) ){
                    $ret->catomo[] = $item->codcatomo;
                }
            }

            $ret->list = $list;
            $ret->n = count($list);
            $ret->tot = $prod_tot;
            $ret->page = $page;
            $ret->page_max = $page_max;
            $ret->show_taglia = $show_taglia;
        }

        return $ret;
    }

    public function ProductPathFoto($id_foto , $check_file_exists = false){
        $md5 = $id_foto;
        $dir = substr($md5,0,2);
        $filename = "_public/articoli/" . $dir . '/' . $md5 . '.jpg';

        if( $check_file_exists ){
            $filename = file_exists( __DIR__ . "/../../../{$filename}") ? $filename : false;
        }

        return $filename;
    }

    public function Product($id , $others_search_opts = NULL){
        $opts = array("id" => $id );
        if( $others_search_opts ){
            foreach( $others_search_opts as $k=>$v){
                $opts[ $k ] = $v;
            }
        }
        $ret = $this->ProductsList( $opts );
        return $ret;
    }

    public function Product_IdFromTagliaColore($codicebase,$colore,$taglia){
        $q = "SELECT idart FROM `^e_articoli` WHERE codicebase = '{$codicebase}' AND colore = '{$colore}' AND taglia = '{$taglia}' AND status=1 AND deleted IS NULL LIMIT 1";
        return $this->cn->OF($q);
    }

    public function Qty($idart,$no_this_basket=false){
        $basket_lifetime = $this->P('basket_lifetime');

        $q = "SELECT * FROM `^e_articoli` WHERE idart = {$idart}";
        $art = $this->cn->OQ($q);

        $qty_init = 0;
        if( $art!='-1' ) {

            //Quantità iniziale
            $q = "SELECT giacenza_full AS giacenza FROM `^e_articoli` WHERE idart = {$idart}";
            $qty_init = $this->cn->OF($q);

            //Quantità ordini
            $q = "SELECT SUM(br.qty) AS qty_sum FROM `^basket` AS b INNER JOIN `^basket_row` AS br ON b.id = br.id_basket WHERE br.idart = {$idart} AND b.`session` IS NULL";
            $qty_ordered = $this->cn->OF($q);
            $qty_ordered = $qty_ordered>0 ? $qty_ordered : 0;

            //Quantità nel carrello
            $q = "SELECT SUM(br.qty) AS qty_sum FROM `^basket` AS b INNER JOIN `^basket_row` AS br ON b.id = br.id_basket WHERE br.idart = '{$idart}' AND b.`session` IS NOT NULL AND b.`updated` > DATE_ADD(NOW(), INTERVAL -{$basket_lifetime} HOUR )";
            if( $no_this_basket ){
                $ses = session_id();
                $q .= " AND b.`session` != '{$ses}'";
            }
            $qty_basket = $this->cn->OF($q);
            $qty_basket = $qty_basket>0 ? $qty_basket : 0;

            $qty = $qty_init - ($qty_ordered + $qty_basket);
            //echo $qty_init."<br>".$qty_recharged."<br>".$qty_ordered."<br>".$qty_basket;
            return $qty;
        }else{
            return 0;
        }
    }

    public function TagliaName($codice){
        $q = "SELECT descr FROM `^e_taglie` WHERE codice='{$codice}'";
        return $this->cn->OF($q);
    }
    public function ColoreName($codice){
        $q = "SELECT IF(col.descr='',co.descr,col.descr) AS descr FROM `^e_colori` AS co LEFT JOIN `^e_colori_ln` AS col ON co.codice = col.codice AND col.id_lang = {$this->id_lang} WHERE co.codice='{$codice}'";
        return $this->cn->OF($q);
    }

    public function TaglieList($only_active=true){
        if( $only_active ){
            /*$q = "SELECT t.* FROM
            `^e_taglie` AS t
            INNER JOIN `^e_articoli` AS art ON t.codice = art.taglia
            INNER JOIN `^e_griffe` AS gr ON art.codgriffe = gr.codice
            WHERE art.status=1 AND art.deleted IS NULL AND gr.deleted IS NULL AND gr.show=1 GROUP BY codice";*/
            $q = "SELECT t.* FROM
            `^e_articoli` AS art
            INNER JOIN `^e_griffe` AS gr ON art.codgriffe = gr.codice
            INNER JOIN `^e_taglie` AS t ON art.taglia = t.codice
            WHERE art.status=1 AND art.deleted IS NULL AND gr.deleted IS NULL AND gr.show=1 GROUP BY codice";
        }else{
            $q = "SELECT * FROM `^e_taglie` WHERE 1";
        }
        $order = " ORDER BY taord ASC";

        $q .= $order;

        $list = $this->cn->Q( $q );
        return $list;
    }


    public function TaglieGroupList(){
        $q = "SELECT * FROM `^taglie_group` ORDER BY `order` ASC";
        $list = $this->cn->Q($q);

        return $list;
    }

    public function CampiPersLang($k){
        $lang_it = $this->getLang('url','it');
        if( $this->id_lang==$lang_it->id ){//Se siamo in italiano torna il valore, non lo traduce
            return $k;
        }else{
            $k = trim($k);
            $q = "SELECT label FROM `^e_campipers_ln` WHERE `k` = '" . addslashes($k) . "' AND id_lang={$this->id_lang}";
            $label = $this->cn->OF($q);
            return $label ? $label : $k;
        }
    }











    public function Type($id){
        $q = "SELECT pt.*, ptl.name FROM `^products_types` pt LEFT JOIN `^products_types_ln` ptl ON pt.id = ptl.id_type AND ptl.id_lang = {$this->id_lang} WHERE pt.id={$id}";
        $item = $this->cn->OQ($q);
        return $item;
    }

    public function Discount($price_full,$price_discount,$decimal=0){
        $perc = 100*($price_full - $price_discount)/$price_full;
        $perc = number_format($perc,$decimal,"",",");
        return $perc;
    }

    public function Color($id){
        $q = "SELECT pc.*, IF(pcl.name!='', pcl.name, pc.name) AS name FROM `^products_colors` pc LEFT JOIN `^products_colors_ln` pcl ON pc.id = pcl.id_color AND pcl.id_lang = {$this->id_lang} WHERE pc.id = {$id}";
        $o = $this->cn->OQ($q);
        return $o;
    }
    public function Size($id){
        $q = "SELECT ps.*, IF(psl.name!='', psl.name, ps.name) AS name FROM `^products_sizes` ps LEFT JOIN `^products_sizes_ln` psl ON ps.id = psl.id_size AND psl.id_lang = {$this->id_lang} WHERE ps.id = {$id}";
        $o = $this->cn->OQ($q);
        return $o;
    }

    public function ColorsList( $only_full=false ){
        $q = "SELECT c.*, IF(cl.name!='',cl.name,c.name) AS name FROM
		`^products_colors` c
		INNER JOIN `^products_colors_ln` cl ON c.id = cl.id_color AND cl.id_lang = {$this->id_lang}";

        if( $only_full ){
            $q .= "
				INNER JOIN `^products_simple` ps ON ps.id_color = c.id
				INNER JOIN `^products` p ON p.id = ps.id_product
				WHERE p.deleted IS NULL AND p.status='published'
			";
        }else{
            $q .= " WHERE 1";
        }

        $q .= " GROUP BY c.id ORDER BY `name` ASC";

        $list = $this->cn->Q($q);
        return $list;
    }
}