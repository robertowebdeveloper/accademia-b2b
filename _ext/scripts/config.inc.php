<?php
define('path_docroot', __DIR__ . '/../../' );
define('ALERT_EMAIL','parrella@inol3.com,domini@inol3.com');

if( file_exists('/is_inol3') ){ //Locale
	$NE_db_data = array(
		"hostname"		=>	'localhost',
		"db"		=>	'accademia_b2b',
		"username" 	=>	'root',
		"password"		=>	'adrenaline',
		"db_table_prefix"	=>	'acc_'
	);

	$NE_db_data_frontiera = array(
		"hostname"		=>	'localhost',
		"db"		=>	'accademia_b2b_frontiera',
		"username" 	=>	'root',
		"password"		=>	'adrenaline',
		"db_table_prefix"	=>	''
	);
	
	define('_DEBUG',true);
	define('path_webroot', '/accademia-b2b/');
	define('server','local');
	define('paypal_business','roberto.webdeveloper@gmail.com'); //roberto.webdeveloper@gmail.com
	//Per i pagamenti si può usare questo utente: nadia.sarti-buyer@sottosotto.it : aaaaaaaa
}else if( file_exists('/is_rp') ){ //Locale2
	$NE_db_data = array(
		"hostname"		=>	'localhost',
		"db"		=>	'accademia_b2b',
		"username" 	=>	'root',
		"password"		=>	'jmcjmc',
		"db_table_prefix"	=>	'acc_'
	);

	$NE_db_data_frontiera = array(
		"hostname"		=>	'localhost',
		"db"		=>	'accademia_b2b',
		"username" 	=>	'root',
		"password"		=>	'adrenaline',
		"db_table_prefix"	=>	''
	);
	
	define('_DEBUG',true);
	define('path_webroot', '/accademia-b2b/');
	define('server','local');
	define('paypal_business','roberto.webdeveloper@gmail.com'); //roberto.webdeveloper@gmail.com
	//define('paypal_business','LNPGW7YCA5TMW');
}else{ //Produzione
	$NE_db_data = array(
		"hostname"		=>	'localhost',
		"db"		=>	'',
		"username" 	=>	'',
		"password"		=>	'',
		"db_table_prefix"	=>	'acc_'
	);

	$NE_db_data_frontiera = array(
		"hostname"		=>	'localhost',
		"db"		=>	'accademia_b2b',
		"username" 	=>	'root',
		"password"		=>	'adrenaline',
		"db_table_prefix"	=>	''
	);
	
	define('_DEBUG',false);
	define('path_webroot', '/');
	define('server','live');
	define('paypal_business','');
}

//define('path_site', path_docroot . path_webroot );
define('path_site', path_docroot );

//********* IMAGES
$_images = array(
    "prod_list" => array(
        "w" => 400,//189,
        "h" => 450,//286,
        "m" => "square",
		"out" => "jpg85",
        "bg" => "FFFFFF"/*,
		"wm" => 'mask02.png'*/
    ),
    "prod_item" => array(
        "w" => 600,//360,
        "h" => 908,//544,
        "m" => "prop",
		"out" => "jpg85",
        "bg" => "FFFFFF"
    ),
    "prod_thumb_colore" => array(
        "w" => 80,
        "h" => 121,
        "m" => "prop",
		"out" => "jpg85",
        "bg" => "FFFFFF"
    ),
    "prod_thumb_vista" => array(
        "w" => 68,
        "h" => 102,
        "m" => "prop",
		"out" => "jpg85",
        "bg" => "FFFFFF"
    ),
	"prod_thumb" => array(
		"w" => 90,
		"h" => 90,
		"m" => "prop",
		"out" => "jpg85",
		"bg" => "FFFFFF"
	),
	"big" => array(
		"w" => 1200,
		"h" => 1200,
		"m" => "prop4",
		"bg" => "FFFFFF"
	)
);
define('images' , json_encode($_images) );
unset($_images);
//********** END IMAGES
?>