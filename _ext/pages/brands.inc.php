<?php include("_ext/include/submenu.inc.php"); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<h3 class="uppercase"><# Brands #></h3>
			<hr>
		</div>		
	</div>

	<?php
	$brands = $S->GriffeList(true);
	foreach($brands as $k=>$brand){//Per correggere eventuali brand con lettere minuscole
		$brands[$k]->descr_upper = strtoupper($brand->descr);
	}
	$last_char = null;
	foreach($brands as $brand){
		$close_row = false;
		$char = substr($brand->descr_upper,0,1);
		if(!$last_char || $last_char!=$char){
			if( $last_char ){
				?></div><br><?php
			}
			$last_char = $char;
			?><div class="row">
				<div class="col-md-12">
					<div id="char-<?=$char; ?>" class="head"><span><?=$last_char; ?></span></div>
				</div>
			</div>
			<div class="row">
			<?php
		}
		?><div class="col-md-3 col-sm-4 col-xs-6"><a class="lnkBrand uppercase" href="{{url products}}?gr=<?=$brand->codice; ?>"><?=$brand->descr; ?></a></div><?php
	}
	?>
	</div><!-- chiude l'ultima row -->
	<br><br><br>
</div>
