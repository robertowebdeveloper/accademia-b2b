<?php
if( isset($S->Page->options->lineap) ):
    $_GET["lineap"] = $S->Page->options->lineap;
elseif( $S->Page->options->special_offers ):
    $_GET['so'] = 1;
endif;

if( !isset($_GET['sort']) ){
    $_GET['sort'] = 'price,asc';
}
//$ProductsList = $S->ProductsList($_GET);
//echo $ProductsList->q;
//echo "<pre>";print_r($ProductsList);echo "</pre>";
?>

<?php include("_ext/include/submenu.inc.php"); ?>

<div id="catalog_wrapper" class="container">
    <form id="catalog_search-form" enctype="application/x-www-form-urlencoded" method="get" action="{{url products}}" role="form">
        <div class="row">
            <input type="hidden" name="s" value="<?=$_GET['s']; ?>">
            <input type="hidden" name="ca" value="<?=isset($_GET['ca']) ? $_GET['ca'] : 0; ?>">
            <input type="hidden" name="gr" value="<?=isset($_GET['gr']) ? $_GET['gr'] : 0; ?>">
            <input type="hidden" name="so" value="<?=isset($_GET['so']) ? $_GET['so'] : 0; ?>">

            <div class="col-md-3 col-sm-3">
                <div id="Products_SearchCol">
                    <div class="text-right visible-xs-block">
                        <br>
                        <button type="button" class="close _closeMobileFilters">&times;&nbsp;&nbsp;</button>
                        <br>
                    </div>

                    <h6><# Cerca #></h6>
                    <input type="text" name="s" placeholder="<# Cerca un prodotto o marchio #>" class="form-control">
                    <div class="Margin"></div>

                    <!-- Campagna -->
                    <h6><# Campagne #></h6>
                    <div id="SearchCampagne" class="SearchList">
                        <ul>
                            <?php
                            $list = $S->CampagneList( true );
                            foreach($list as $li){
                                $fired = $li->codice_group==$_GET['cam'] ? ' class="fired"' : '';
                                ?><li>
                                <a href="#"<?=$fired; ?> data-cam="<?=$li->codgroupyear; ?>">
                                    <?=$li->codgroupyear; ?>
                                    <span class="deactive glyphicon glyphicon-remove-circle"></span>
                                </a>
                                </li><?php
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- End Category -->

                    <!-- Brands -->
                    <h6><# Marchi #></h6>
                    <div id="SearchMarchi" class="SearchList">
                        <ul>
                            <?php
                            $list = $S->MarchiList( true );
                            foreach($list as $li){
                                $fired = $li->codice==$_GET['ma'] ? ' class="fired"' : '';
                                ?><li>
                                <a href="#"<?=$fired; ?> data-ma="<?=$li->codice; ?>">
                                    <?=$li->descrizione; ?>
                                    <span class="deactive glyphicon glyphicon-remove-circle"></span>
                                </a>
                                </li><?php
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- End Brands -->

                    <!-- Category -->
                    <h6><# Categoria #></h6>
                    <div id="SearchCategorie" class="SearchList">
                        <ul>
                            <?php
                            $list = $S->CategorieList( true );
                            foreach($list as $li){
                                $fired = $li->codice==$_GET['ca'] ? ' class="fired"' : '';
                                ?><li>
                                <a href="#"<?=$fired; ?> data-ca="<?=$li->codice; ?>">
                                    <?=$li->descrizione; ?>
                                    <span class="deactive glyphicon glyphicon-remove-circle"></span>
                                </a>
                                </li><?php
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- End Category -->

                    <!-- Price -->
                    <h6><# Prezzo #></h6>
                    <input type="text" id="range_price" name="range_price">
                    <?php
                    $price_range = $S->PriceRange();
                    $price_range->min = 0;
                    list($min,$max) = explode(";",$_GET['range_price']);
                    $min = floatval($min);
                    $max = floatval($max);
                    $min = $min<$price_range->min ? $price_range->min : $min;
                    $max = $max>$price_range->max ? $price_range->max : $max;
                    ?>
                    <script type="text/javascript"><!--
                        $(document).ready(function(){
                            $("#range_price").ionRangeSlider({
                                min: <?=$price_range->min; ?>,
                                max: <?=$price_range->max; ?>,
                                from: <?=$min>0 ? $min : $price_range->min; ?>,
                                to: <?=$max>0 ? $max : $price_range->max; ?>,
                                type: 'double',
                                prefix: '&euro; '
                            });
                        });
                        --></script>
                    <!-- End Price -->
                    <div class="Margin"></div><div class="Margin"></div>
                    <button type="submit" class="Btn btn btn-block btn-lg"><# Cerca #></button>
                </div>
            </div>

            <div id="SearchResult" class="col-md-9 col-sm-9 col-xs-12">
                <!--a href="javascript:;" onclick="window.history.pushState('test', 'test', 'ciao.html');">qui</a-->
                <?php
                $class_yes_product = $ProductsList->n>0 ? '' : ' hide';
                $class_no_product = $ProductsList->n>0 ? ' hide' : '';
                ?>
                <div class="Loader"></div>
                <div class="result<?=$class_yes_product; ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default btn-sm pull-left visible-xs _openMobileFilters" style="margin: -10px 10px 0 0;"><i class="glyphicon glyphicon-menu-hamburger"></i></button>
                            <h6 class="founded" style="display: none;"><# Trovati <b>%s</b> prodotti %txt=<?=$ProductsList->tot; ?> #></h6>
                            <h6>&nbsp;</h6>
                            <?php include("_ext/include/catalog_pagination.inc.php"); ?>
                            <br><br>
                        </div>
                    </div>
                    <div class="cont row">
                        <?php
                        foreach($ProductsList->list as $item){
                            echo $UI->ProductItemView( $item );
                        }
                        ?>
                    </div>
                    <br>
                    <?php include("_ext/include/catalog_pagination.inc.php"); ?>
                </div>
                <div class="noresult<?=$class_no_product; ?>">
                    <div class="noFound text-center"><br><br><# La ricerca non ha prodotto nessun risultato. #></div>
                </div>
            </div>
        </div>
    </form>
</div>
<br><br>