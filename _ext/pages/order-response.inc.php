<?php
if( isset($_REQUEST['codTrans']) ){
	$codTrans = $_REQUEST['codTrans'];
	$id_order = str_replace("so_","",$codTrans);
}else if( isset($_REQUEST['io']) ){
	$id_order = $_REQUEST['io'];
}
$id_order = intval($id_order);

/*
* RISPOSTA CORRETTA DALLA BANCA:
* http://test.inol3.com/SottoSotto/it/ordine-concluso?regione=&session_id=a5204frbvos7ppthc75h6n4ar1&tipoTransazione=VBV_FULL&data=20150827&mac=5bfe4edcc5b07e328c53a593ea6188742e4eb2a2&tipoProdotto=VISA+BUSINESS+-+CREDIT+-+N&nazionalita=ITA&OPTION_CF=&esito=OK&scadenza_pan=201604&messaggio=Message+OK&mail=domini%40inol3.com&codAut=740906&alias=payment_756965&codiceEsito=0&orario=112146&importo=1&languageId=ITA&cognome=Palagano&pan=453220XXXXXX6276&divisa=EUR&codiceWallet=3091423&brand=VISA&nome=Vito&codTrans=so_0000037
*
* RISPOSTA ERRATA:
* http://test.inol3.com/SottoSotto/it/ordine-concluso?mail=domini%40inol3.com&session_id=a5204frbvos7ppthc75h6n4ar1&regione=&tipoTransazione=VBV_MERCHANT&codAut=&alias=payment_3444153&codiceEsito=400&orario=121005&data=20150827&mac=c5051a6400f53d4c96cfd61114fa1375c9d30fbc&importo=1&tipoProdotto=+-++-+&cognome=b&languageId=ITA&pan=455641XXXXXX4963&nazionalita=&divisa=EUR&OPTION_CF=&scadenza_pan=201703&esito=KO&brand=VISA&codTrans=so_0000037&nome=a&messaggio=Auth.+Denied
*/
?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
			<br>
			<?php if($_GET['esito']=='OK'){ ?>
				<div class="alert alert-success">
					<h3><# Congratulazioni. Il tuo ordine è concluso #></h3>
					<# Riceverai una e-mail di conferma. #>
					<br>
					<# Grazie per aver acquistato su <b>Accademia</b> #>
					<br>
					<# Il tuo numero di ordine è <b>%s</b> %txt=<?=$S->Order_codeFromId($id_order); ?> #>
					<br><br>
				</div>
				
				<hr>
				<ul class="confirm-registration-info">
					<li><# Se non ricevi le nostre e-mail verifica per favore la casella di Posta Indesiderata (spam) #></li>
					<?php if($id_customer>0){ ?>
						<li><# Non hai ricevuto la mail di conferma? #> <a href="{{urlthis}}?resent=<?=sha1($id_customer); ?>"><# Clicca qui #></a></li>
					<?php } ?>
					<li><# Puoi trovare una copia del tuo ordine nella tua area personale. #></li>
					<li><# Per assistenza puoi contattarci: #> <span data-e="customercare|sottosotto.it"></span></li>
				</ul>
			<?php }else{ ?>
				<div class="alert alert-danger">
					<h3><# Si è verificato un problema nel pagamento. #></h3>
					<# Il tuo ordine non è stato processato #>
					<br><br>
				</div>
				<hr>
				<ul class="confirm-registration-info">
					<li><# Per assistenza puoi contattarci: #> <span data-e="customercare|sottosotto.it"></span></li>
				</ul>
			<?php } ?>
			<hr>

			<div class="text-center"><img src="{{img}}top-quality.jpg" alt="" class="img-responsive img-rounded"></div>
			<br>
			
		</div>
	</div>
</div>