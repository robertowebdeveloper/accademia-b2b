<div class="container">
      <div class="row">
            <div class="col-sm-12">
                  <h3>
                        <b>Sotto Sotto</b> Fashion Outlet
                        <small><# CONDIZIONI GENERALI DI VENDITA #></small>
                  </h3>
                  <hr>
            </div>
            <div class="col-sm-9">
                  <?php if($S->id_lang==1){ ?>
                      <div class="text-justify">
                          <a id="art-termini-condizioni">&nbsp;</a>
                          <p>
                              <strong>TERMINI E CONDIZIONI D’USO:</strong>
                          </p>
                          <p>
                              <strong>Condizioni generali di vendita </strong>
                          </p>
                          <p>
                              Le presenti condizioni generali di vendita ("Condizioni generali") sono regolate dal Codice del
                              Consumo (D. Lgs. n. 206/2005), sezione II Contratti a distanza (artt. 50 - 67) e dalle norme in
                              materia di commercio elettronico (D. Lgs. n. 70/2003).
                          </p>
                          <p>
                              Il Cliente è invitato a stampare o a conservare secondo le modalità preferite le Condizioni generali.
                          </p>
                          <p>
                              In caso di variazioni delle Condizioni generali, all'ordine si applicheranno le Condizioni generali
                              pubblicate sul Sito al momento dell'invio dell'ordine stesso da parte del Cliente.
                          </p>
                          <p>
                              <strong>Offerta al pubblico </strong>
                          </p>
                          <p>
                              I prodotti ed i relativi prezzi presentati nel Sito costituiscono un'offerta al pubblico conformemente
                              alle modalità precisate nelle Condizioni generali e nel Sito stesso. Le condizioni di tale offerta si
                              applicano esclusivamente agli acquisti effettuati sul Sito.
                          </p>
                          <p>
                              I contratti d'acquisto stipulati sul Sito sono conclusi con Sotto Sotto srl.
                          </p>
                          <p>
                              <strong>Prezzi </strong>
                          </p>
                          <p>
                              Tutti i prezzi sono comprensivi di IVA e le spese di consegna sono evidenziate al momento
                              dell'ordine.
                          </p>
                          <p>
                              Ai prodotti si applica il prezzo evidenziato sul Sito al momento della conclusione dell'ordine, senza
                              alcuna considerazione di eventuali variazioni di prezzo intervenute successivamente o
                              precedentemente offerte.
                          </p>
                          <p>
                              <strong>Ordine </strong>
                          </p>
                          <p>
                              Il Cliente conclude correttamente la procedura d'ordine se il Sito non evidenzia alcun messaggio di
                              errore.
                          </p>
                          <p>
                              Il contratto si intende concluso al momento della ricezione dell'ordine da parte di Sotto Sotto srl. In tal caso Sotto Sotto srl darà riscontro della
                              ricezione dell'ordine con l'invio di una mail di conferma
                              d'ordine all'indirizzo di posta elettronica comunicato dal Cliente.
                          </p>
                          <p>
                              <strong>Modalità di pagamento </strong>
                          </p>
                          <p>
                              Il pagamento può essere effettuato on-line con le modalità precisate nel Sito.
                          </p>
                          <p>
                              Contestualmente alla comunicazione della conferma d'ordine, l'importo corrispondente ai prodotti
                              acquistati sarà addebitato al Cliente.
                          </p>

                          <a id="art-pagamento-sicuro">&nbsp;</a>
                          <p>
                              <strong>METODI DI PAGAMENTO</strong>
                          </p>

                          <p>
                              Metodi di pagamento accettati:
                          </p>
                            <ol>
                                <li>Carta di credito</li>
                                <li>Pay Pal</li>
                            </ol>
                          <p>
                              Pagare con carta di credito e PayPal è semplice e sicuro.
                              <br><br>
                              Sotto Sotto srl utilizza il sistema di pagamento sicuro PayPal che offre la protezione dei dati e garantisce una sicurezza efficace nelle transazioni on-line. Ma anche il POS virtuale per pagamenti X-Pay, sistema dotato di tutti i protocolli di sicurezza esistenti (3D Secure - Verified by Visa, MasterCard SecureCode e CV2).
                          </p>

                          <a id="art-consegna">&nbsp;</a>
                          <p>
                              <strong>CONSEGNA:</strong>
                          </p>

                          <a id="art-spedizioni-resi">&nbsp;</a>
                          <p>
                              <strong>SPEDIZIONI E RESI</strong>
                          </p>
                          <p>
                              <strong>Modalità e spese di consegna </strong>
                          </p>
                          <p>
                              Sotto Sotto srl emette fattura dei prodotti acquistati, inviandola tramite e-mail all'intestatario
                              dell'ordine. Per l'emissione della fattura, fanno fede le informazioni fornite dal Cliente. Nessuna
                              variazione dei dati sarà possibile dopo l'emissione della fattura stessa.
                          </p>
                          <p>
                              Le spese di consegna sono gratuite.
                          </p>
                          <p>
                              La consegna, a cura di vettori specializzati, avverrà entro 3-5 gg per spedizioni standard e in 2 gg. per spedizioni express (lun-ven entro le 15) nei giorni
                              lavorativi successivi dalla data di invio dell'ordini.
                          </p>
                          <p>
                              Nessuna responsabilità può essere imputata a Sotto Sotto srl in caso di ritardo nella consegna.
                          </p>
                          <p>
                              Nel caso di mancato ritiro dei prodotti da parte del Cliente entro il termine sopra precisato, l'ordine
                              verrà automaticamente annullato ed i prodotti restituiti a Sotto Sotto srl.
                          </p>
                          <p>
                              Al momento della consegna dei prodotti, il Cliente è tenuto a controllare che l'imballo risulti
                              integro, non danneggiato, né bagnato o comunque alterato, anche nei materiali di chiusura (nastro
                              adesivo o reggette metalliche).
                          </p>
                          <p>
                              Nel caso in cui risultino danni evidenti all'imballo e/o al prodotto, il Cliente può rifiutare la
                              consegna dei prodotti stessi, che saranno restituiti a Sotto Sotto srl senza alcuna spesa a carico del
                              Cliente.
                          </p>
                          <p>
                              Una volta firmato il documento di consegna, il Cliente non potrà opporre alcuna contestazione circa
                              le caratteristiche esteriori dei prodotti consegnati.
                          </p>
                          <p>
                              Eventuali problemi inerenti l'integrità fisica, la corrispondenza o la completezza dei prodotti
                              ricevuti devono essere segnalati tempestivamente a Sotto Sotto srl per l'attivazione delle condizioni
                              di garanzia.
                          </p>
                          <p>
                              <strong>Diritto di recesso </strong>
                          </p>
                          <p>
                              Non si effettuano cambi merce.
                              Il cliente può inviare a Sotto Sotto una mail nella quale indica il/i codice/i che vuole rendere con riferimento al relativo NRO ordine e indicando
                              possibilmente sia i "motivi" del reso sia il proprio codice IBAN; il reso deve avvenire entro 14 gg. solari a partire dal ricevimento della merce;
                              una volta che Sotto Sotto avrà ricevuto la merce “resa” verificherà l’integrità del capo e del sigillo di garanzia e, se tutto ok,
                              provvederà al rimborso dell’ importo dovuto – se il reso è totale il rimborso verrà fatto tramite carta di credito – se il reso è parziale Sotto Sotto effettuerà un bonifico al cliente.
                              La spedizione del reso sarà gratuita e DEVE essere effettuata tramite UPS.
                          </p>

                          <a id="art-nota-legale">&nbsp;</a>
                          <p>
                              <strong>NOTA LEGALE:</strong>
                          </p>
                          <p>
                              <strong>Garanzie </strong>
                          </p>
                          <p>
                              Tutti i prodotti venduti da Sotto Sotto srl godono delle garanzie di legge e delle garanzie
                              commerciali di volta in volta precisate nel Sito.
                          </p>
                          <p>
                              <strong>Trattamento dei dati personali </strong>
                          </p>
                          <p>
                              I dati del Cliente sono trattati da Sotto Sotto srl conformemente a quanto previsto dalla normativa
                              in materia di protezione dei dati personali, come specificato nell'informativa fornita nella sezione
                              "Privacy".
                          </p>
                          <p>
                              <strong>Comunicazioni </strong>
                          </p>
                          <p>
                              Ogni comunicazione potrà essere indirizzata a Sotto Sotto srl, ai recapiti indicati nel sito.
                          </p>
                          <p>
                              <strong>Foro competente </strong>
                          </p>
                          <p>
                              Eventuali controversie saranno risolte davanti all'Autorità giudiziaria competente in base alla
                              normativa applicabile.
                          </p>

                      </div>
			      <?php }else if($S->id_lang==2 && 0){ ?>
                      <div class="text-justify">
                          <a id="art-termini-condizioni">&nbsp;</a>
                          <p>
                              <strong>TERMS AND CONDITIONS OF USE:</strong>
                          </p>
                          <p>
                              <strong>General conditions of sale</strong>
                          </p>
                          <p>
                              The general conditions of sale ("General Conditions") are governed by the Consumer Code (Italian Legislative Decree no. 206/2005), section II Distance Contracts (articles 50 - 67) and regulations relating to E-Commerce (Italian Legislative Decree no 70/2003).
                          </p>
                          <p>
                              The Customer is invited to print or save these General Conditions as they see fit.<br>
                              In the event of any variations to the General Conditions those published on the Web Site when the order is placed by the Customer will apply.
                          </p>
                          <p>
                              In caso di variazioni delle Condizioni generali, all'ordine si applicheranno le Condizioni generali
                              pubblicate sul Sito al momento dell'invio dell'ordine stesso da parte del Cliente.
                          </p>
                          <p>
                              <strong>Public Offering</strong>
                          </p>
                          <p>
                              The products and prices published on the Web Site constitute an offer to the public in accordance with the procedures set out in the General Conditions and on the Site itself.<br>
                              The conditions therein apply exclusively to purchases made on the Web Site.
                          </p>
                          <p>
                              Purchase contracts concluded on the Web Site are with Sotto Sotto srl.
                          </p>
                          <p>
                              <strong>Prices</strong>
                          </p>
                          <p>
                              All prices include VAT and the delivery costs are indicated when the order is placed.<br>
                          </p>
                          <p>
                              The prices published on the Web Site at the time of placing the order will apply without any consideration of price variations that occur after or prior to the offer.
                          </p>
                          <p>
                              <strong>Orders</strong>
                          </p>
                          <p>
                              The Customer concludes the order correctly if no error message appears on the Web Site.
                          </p>
                          <p>
                              The contract will be concluded upon receipt of the order by Sotto Sotto srl. In this case Sotto Sotto srl will acknowledge receipt of the order by sending an e-mail confirming the order to the address supplied by the Customer.
                          </p>
                          <p>
                              <strong>Method of payment</strong>
                          </p>
                          <p>
                              Payment can be made on-line using the methods specified on the Web Site.
                          </p>
                          <p>
                              At the same time the order confirmation is sent the amount corresponding to the products purchased will be charged to the Customer.
                          </p>

                          <a id="art-pagamento-sicuro">&nbsp;</a>
                          <p>
                              <strong>METHOD OF PAYMENT</strong>
                          </p>

                          <p>
                              Payment can be made on-line using the methods specified on the Web Site.
                          </p>
                          <p>
                              At the same time the order confirmation is sent the amount corresponding to the products purchased will be charged to the Customer.
                          </p>

                          <p>
                              <strong>Secure payment</strong>
                          </p>
                          <p>
                              Paying by credit card and PayPal is simple and safe.
                              Sotto Sotto srl uses the secure PayPal system that provides data protection and ensures effective security for on-line transactions.
                          </p>

                          <a id="art-consegna">&nbsp;</a>
                          <p>
                              <strong>DELIVERY:</strong>
                          </p>

                          <a id="art-spedizioni-resi">&nbsp;</a>
                          <p>
                              <strong>SHIPPING AND RETURNS</strong>
                          </p>
                          <p>
                              <strong>Terms and cost of delivery8</strong>
                          </p>
                          <p>
                              Sotto Sotto srl issues invoices for products purchased and sends them to the recipient of the order via e-mail. Information supplied by the Customer will be used to issue the invoice. No variation to this data will be possible once the same invoice has been issued.
                          </p>
                          <p>
                              Delivery costs are free.
                          </p>
                          <p>
                              Delivery will be made using specialised couriers within 3-5 working days for standard shipping and 2 working days for express shipping (Mon-Fri by 15) after the order has been dispatched.
                          </p>
                          <p>
                              No responsibility can be attributed to Sotto Sotto srl in the event of late deliveries.
                          </p>
                          <p>
                              In the event the Customer does not collect the products within the above mentioned time the order will be cancelled automatically and the products will be returned to Sotto Sotto srl.
                          </p>
                          <p>
                              Upon delivery of products the Customer is bound to check the packaging is intact, not damaged, wet or altered in any way, including the sealing materials (adhesive tape or metal staples).
                          </p>
                          <p>
                              If there is obvious damage to the packaging and/or product the Customer can refuse to accept the same products that will be returned to Sotto Sotto srl without any charge to the Customer.
                          </p>
                          <p>
                              Once the delivery note has been signed the Customer will not be able to make any objection relating to the external characteristics of the products delivered.
                          </p>
                          <p>
                              Any problems relating to the physical integrity, correspondence or completeness of the products received must be reported to Sotto Sotto srl immediately to activate the warranty conditions.
                          </p>
                          <p>
                              <strong>Right of withdrawal</strong>
                          </p>
                          <p>
                              No goods will be exchanged. The Customer can send Sotto Sotto an e-mail indicating the code/s they wish to return with reference to the PO number, possible “reasons” for the return as well as their IBAN code; returns must be made within 14 calendar days from receipt of the goods; once Sotto Sotto has received the “returned” goods they will check the integrity of the item and the warranty seal and, if there are no problems, refund the amount owed – if it is a total return it will be made by credit card – if it is a partial return Sotto Sotto will send the Customer a bank transfer. Return shipping is free and MUST be carried out using UPS.
                          </p>

                          <a id="art-nota-legale">&nbsp;</a>
                          <p>
                              <strong>LEGAL NOTE:</strong>
                          </p>
                          <p>
                              <strong>Warranty</strong>
                          </p>
                          <p>
                              All products sold by Sotto Sotto srl are covered by legal warranties and commercial guarantees that are specified on the Web Site from time to time.
                          </p>
                          <p>
                              <strong>Personal data processing</strong>
                          </p>
                          <p>
                              Customer data is processed by Sotto Sotto srl in accordance with legislation on the protection of personal data as specified in the information contained in the "Privacy" section.
                          </p>
                          <p>
                              <strong>Communications</strong>
                          </p>
                          <p>
                              All communications should be sent to Sotto Sotto srl, using the addresses published on the Web Site.
                          </p>
                          <p>
                              <strong>Jurisdiction</strong>
                          </p>
                          <p>
                              All disputes will be referred to the competent Judicial Authorities in accordance with applicable legislation.
                          </p>

                      </div>
			      <?php } ?>
            </div>

            <div class="col-sm-3">
                  <div class="Box Box-gray">
                        <ul class="anchors-list">
                              <?php
                              $art = array(
                                  'termini-condizioni' => "Termini e condizioni d'uso",
                                  'pagamento-sicuro' => "Metodi di pagamento",
                                  'consegna' => "Consegna",
                                  'spedizioni-resi' => "Spedizioni e Resi",
                                  'nota-legale' => 'Nota Legale'
                              );
                              foreach($art as $k=>$v){
                                    ?>
                                    <li><a href="#art-<?=$k; ?>"><# <?=$v; ?> #></a></li>
                              <?php
                              }
                              ?>
                        </ul>
                  </div>
            </div>
      </div>
</div>
<br><br>