<?php
if( isset($_POST['id_customer']) ){
	$id_customer = $_POST['id_customer'];	
}else if( isset($_GET['resent']) ){
	if( $id_customer = $S->idCustomerFromSha1( $_GET['resent'] ) ){
		$S->sendMailRegisterCustomer($id_customer);
	}
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<br>
			<div class="alert alert-success">
				<img src="{{img}}icons/confirm-registration.png" alt="" width="80" class="pull-right">
				<br>
				<# <b>Benvenuto su Accademia!</b>
				<br>
				Abbiamo inviato una e-mail al tuo indirizzo, verifica la tua casella e clicca sul pulsante di attivazione del tuo account #>
				<div class="clearfix"></div>
			</div>
			
			<hr>
			<ul class="confirm-registration-info">
				<li><# Se non ricevi la mail controlla la Posta Indesiderata (spam) #></li>
				<?php if($id_customer>0){ ?>
					<li><# Non hai ricevuto la mail? #> <a href="{{urlthis}}?resent=<?=sha1($id_customer); ?>"><# Clicca qui #></a></li>
				<?php } ?>
				<li><# Per assistenza puoi contattarci: #> <span data-e="customercare|sottosotto.it"></span></li>
			</ul>
			<hr>

			<div class="text-center"><img src="{{img}}top-quality.jpg" alt="" class="img-responsive img-rounded"></div>
			<br>
			
		</div>
	</div>
</div>