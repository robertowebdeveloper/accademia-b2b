<div id="homeSlider" class="cleanBx">
    <div>
        <div class="homeSlide" style="background-image: url({{img}}temp/home1.jpg);"></div>
        <div class="homeSlide" style="background-image: url({{img}}temp/home2.jpg);"></div>
        <div class="homeSlide" style="background-image: url({{img}}temp/home3.jpg);"></div>
    </div>
</div>

<div class="container">
    <?php
    $list = $S->ProductsList(array(
        'new_products'=>true,
        'limit'=>3
    ));
    $opts = new stdClass();
    $opts->col = "col-md-4 col-xs-4";
    ?>
    <div class="row">
        <div class="col-sm-12">
            <h1 class="titleSection"><span><# Nuovi arrivi #></span></h1>
        </div>
    </div>
    <div class="row">
        <?php
        foreach($list->list as $p){
            echo $UI->ProductItem($p->id,$opts);
        }
        ?>
    </div>

    <div class="Margin"></div>
    <?php
    $list = $S->ProductsList(array(
        'new_products'=>true,
        'limit'=>4
    ));
    $opts = new stdClass();
    $opts->col = "col-md-3 col-xs-6";
    ?>
    <div class="row">
        <div class="col-sm-12">
            <h1 class="titleSection"><span><# Scuola #></span></h1>
        </div>
    </div>
    <div class="row">
        <?php
        foreach($list->list as $p){
            echo $UI->ProductItem($p->id,$opts);
        }
        ?>
    </div>


    <div class="Margin"></div>
    <?php
    $list = $S->ProductsList(array(
        'new_products'=>true,
        'limit'=>6
    ));
    $opts = new stdClass();
    $opts->col = "col-sm-2 col-xs-4";
    ?>
    <div class="row">
        <div class="col-sm-12">
            <h1 class="titleSection"><span><# Scelti per voi #></span></h1>
        </div>
    </div>
    <div class="row">
        <?php
        foreach($list->list as $p){
            echo $UI->ProductItem($p->id,$opts);
        }
        ?>
    </div>

    <div class="Margin"></div>
    <div class="row">
        <div class="col-sm-12">
            <h1 class="titleSection"><span><# Marchi #></span></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <img src="{{img}}temp/marchi-home.jpg" class="img-responsive">
        </div>
    </div>

    <div class="Margin"></div><div class="Margin"></div><div class="Margin"></div>
</div>

<?php if(0){ ?>
<div id="homeSlider" class="cleanBx">
    <div>

    </div>
</div>
<div class="container">
    <br>
    <?php
    $list = $S->ProductsList(array(
        'new_products'=>true,
        'limit'=>12
    ));
    $products = array_chunk($list->list,6);
    $opts = new stdClass();
    $opts->col = "col-md-2 col-xs-4";
    ?>
    <section class="stripeProducts">
        <header>
            <h3><# Nuovi arrivi #></h3>
        </header>
        <div class="row">
            <?php
            foreach($products[0] as $p){
                echo $UI->ProductItem($p->id,$opts);
            }
            ?>
        </div>
    </section>
    <section class="stripeProducts">
        <header>
            <h3><# I pi&ugrave; venduti #></h3>
        </header>
        <div class="row">
            <?php
            foreach($products[1] as $p){
                echo $UI->ProductItem($p->id,$opts);
            }
            ?>
        </div>
    </section>
    <section class="stripeProducts Griffe">
        <header>
            <h3><# Brands #></h3>
        </header>
        <?php
        $griffe_list = $S->GriffeList(true);
        $arr = array_chunk($griffe_list,30);
        $griffe_list = $arr[0];
        $arr = array();
        foreach( $griffe_list as $griffe ){
            $arr[] = '<a class="text-uppercase" href="{{url products}}?gr=' . $griffe->codice  . '">' . $griffe->descr . '</a>';
        }
        echo implode('<span class="separate">|</span>',$arr);
        ?>

    </section>

    <div id="homeBox" class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="Box Box-gray dt">
                <form id="Newsletter-home" class="dtc _form" role="form">
                    <?=$S->HF('signup-nl'); ?>
                    <input type="hidden" name="mc-group-name[0]" value="Negozio">
                    <input type="hidden" name="mc-group-groups[0]" value="BO - Adulto">
                    <b class="uppercase"><# Iscriviti alla newsletter #></b>
                    <br>
                    <# Registrati alla nostra newsletter #>
                    <div class="insert">
                        <input type="text" name="email" placeholder="<# E-mail #>">
                        <button type="submit" class="relative">
                            <span class="_text"><# Invia #></span>
                            <span class="loader loader16 loader-hide" style="top: 8px;"></span>
                        </button>
                    </div>
                    <br>
                    <div class="example-text">
                        <input type="checkbox" name="privacy" value="1" style="float:left;">
                        <div style="float:left; margin: 1px 5px 0px 5px"> <# Accetta condizioni di privacy #></div>
                        <?php if($S->_lang=="it"){ ?>
                            <a href="//www.iubenda.com/privacy-policy/123414" class="iubenda-black iubenda-embed" title="Privacy Policy">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
                        <?php }else{ ?>
                            <a href="//www.iubenda.com/privacy-policy/652364" class="iubenda-black iubenda-embed" title="Privacy Policy">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
                        <?php } ?>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="Box Box-green-light dt">
                <div class="dtc">
                    <img src="{{img}}/icons/delivery.png" alt="<# Spedizioni gratuite per ordini superiori a 150 euro #>" class="img-responsive pull-left" style="margin-right: 10px;">
                    <b class="title"><# Spedizioni gratuite #></b>
                    <!--<# Per ordini  superiori a 150 &euro; #>-->
                    <b class="text-uppercase"><# Fino al 31 ottobre #></b>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="Box Box-green dt">
                <div class="dtc" style="font-size: 18px; text-align: center;">
                    <b class="title"><# Reso gratuito #></b>
                    <# su tutti gli acquisti #>
                </div>
            </div>
        </div>
    </div>

    <div class="Margin"></div>
    <div class="Margin"></div>
    <div class="Margin"></div>
    <div class="Margin"></div>
</div>
<?php } ?>