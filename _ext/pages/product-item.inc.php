<?php
//print_r($S->Page);
$Item = $S->Product($S->Page->record_id , array('product_detail'=>1 ));

if( count($Item->foto)>0 ){
    $img = $Item->foto[0]->path;
}else{
    $img = '{{img}}foto_def.png';
}
//echo "<pre>";print_r($Item);echo "</pre>";
?>

<?php include('_ext/include/submenu.inc.php'); ?>

<div class="container">
    <div class="Margin"></div>
    <form id="ProductForm" enctype="application/x-www-form-urlencoded" method="post" data-action-this="{{urlthis}}" data-action="{{url basket}}" role="form">
        <div class="row">
            <input type="hidden" name="id_product" value="<?=$Item->id; ?>">
            <input type="hidden" name="id_customer" value="<?=$S->Customer->id; ?>">
            <input type="hidden" name="id_lang" value="<?=$S->id_lang; ?>">

            <div class="Margin"></div>

            <div class="col-md-1 col-sm-1 col-xs-3">
                <?php
                $iColori = 0;
                foreach($Item->colori as $colore){
                    $class = $iColori==0 ? '' : ' hide';
                    ?>
                    <div class="colore-thumbs<?=$class; ?>" data-codice="<?=$colore->codice; ?>">
                        <?php
                        $iFoto=0;
                        foreach($Item->foto as $foto){
                            if($foto->cod_colore == $colore->codice){
                                $class = $iFoto==0 ? ' fired' : '';
                                ?>
                                <a href="#" class="colore-thumb<?=$class; ?>" data-tipo="<?=$foto->tipo; ?>" data-colore="<?=$foto->cod_colore; ?>" data-src="<?=$S->Img($foto->path,'prod_item'); ?>" data-colore-zoom="<?=$S->Img($foto->path,'big'); ?>"><img src="<?=$S->Img( $foto->path , 'prod_thumb_vista'); ?>" alt="<?=$S->AltParse( $colore->descr ); ?>" class="img-responsive"></a>
                                <?php
                                $iFoto++;
                            }
                        }
                        ?>
                    </div>
                    <?php
                    $iColori++;
                } ?>
            </div>

            <div class="col-md-4 col-sm-5 col-xs-9">
                <span class="productZoom" data-zoom="<?=$S->Img($img,'big'); ?>"><img id="mainFoto" src="<?=$S->Img($img,'prod_item'); ?>" alt="<?=$S->AltParse( $Item->descrbase ); ?>" class="img-responsive"></span>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-1">

                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-6 BrandLabel">
                        <div>
                            <?=$Item->griffe; ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-6 PriceLabel">
                        <div>
                            &euro; <?=$S->Money($Item->prezzo,2,",","."); ?>
                            <?php
                            if($Item->prezzo_full>0){
                                ?>
                                <em><# di listino &euro; %s %txt=<?=$S->Money($Item->prezzo_full,0); ?> #></em>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="Margin"></div>

                <div class="seleziona-colore">
                    <h4><# Seleziona colore #></h4>
                    <ul>
                        <?php
                        $iColori=0;
                        foreach($Item->colori as $colore){
                            $class = $iColori==0 ? ' class="fired"' : '';
                            $img = '_ext/img/foto_def.png';
                            if( count($Item->foto)>0 ) {
                                $flag = false;
                                foreach ($Item->foto as $foto) {
                                    if( $foto->cod_colore==$colore->codice && !$flag){
                                        $img = $foto->path;
                                        $flag = true;
                                    }
                                }
                            }
                            ?>
                            <li class="text-center">
                                <a href="#"<?=$class; ?> data-colore="<?=$colore->codice; ?>"><img src="<?=$S->Img($img , 'prod_thumb_colore'); ?>" alt="<?=$colore->descr; ?>"></a>
                                <?=$colore->descr; ?>
                            </li>
                            <?php
                            $iColori++;
                        }
                        ?>
                    </ul>
                    <br>
                </div>

                <div class="seleziona-taglia hide">
                    <?php if($Item->show_taglia){ ?>
                        <button type="button" class="btn btn-warning btn-sm pull-right" style="margin-top: -5px;" data-toggle="modal" data-target="#guida-taglie-modal"><# Guida alle taglie #></button>
                    <?php } ?>
                    <h4><# Seleziona taglia #></h4>
                    <?php
                    $iColori=0;
                    //echo "<pre>";print_r($Item->colori);
                    foreach($Item->colori as $colore){
                        $class = $iColori==0 ? '' : ' class="hide"';
                        ?>
                        <ul data-codice="<?=$colore->codice; ?>"<?=$class; ?>>
                            <?php
                            $checked = $label_fired = '';
                            if( !$Item->show_taglia && count($colore->taglie)==1 ) {
                                $checked = ' checked';
                                $label_fired = ' class="fired"';
                            }
                            $i=0;
                            foreach($colore->taglie as $taglia){
                                $checked = $i==0 ? ' checked' : '';
                                //echo $Item->codicebase ." ". $colore->codice . " " . $taglia->codice;
                                $idart_tmp = $S->Product_IdFromTagliaColore($Item->codicebase,$colore->codice,$taglia->codice);
                                if($idart_tmp>0) {
                                    ?>
                                    <li>
                                        <label<?=$label_fired; ?>>
                                            <?=$taglia->descr; ?>
                                            <input type="radio" name="idart" value="<?=$idart_tmp; ?>"<?=$checked; ?>>
                                            <!--<?= $S->Product_IdFromTagliaColore($Item->codicebase, $colore->codice, $taglia->codice); ?>-->
                                        </label>
                                    </li>
                                    <?php
                                }
                                $i++;
                            }
                            ?>
                        </ul>
                        <?php
                        $iColori++;
                    }
                    ?>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#" class="Btn btn btn-lg xs-block _addBasket">
                            <span class="_text">
                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                &nbsp;&nbsp;&nbsp;
                                <# Aggiungi al carrello #>
                            </span>
                            <span class="loader loader16 hide"></span>
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="statusAddBasket">
                            <span class="text"></span>
                            <a href="{{url basket}}" class="added">
                                <span class="glyphicon glyphicon-ok-circle"></span>
                                <# Hai aggiunto il prodotto a carrello #>
                            </a>
                        </div>
                    </div>
                </div>

                <hr>

                <div>
                    <h4><?=$Item->descrbase; ?></h4>
                    <table class="table table-striped">
                        <!--tr>
                            <td><# Materiale #></td>
                            <td>
                                <?=$Item->materiale; ?>
                            </td>
                        </tr-->
                        <tr>
                            <td><# Stampa #></td>
                            <td><?=$Item->stampa; ?></td>
                        </tr>
                        <tr>
                            <td><# Codice #></td>
                            <td><?=$Item->codicebase; ?></td>
                        </tr>

                    </table>
                </div>

                <input type="hidden" name="qty" value="1">

            </div>
        </div>
    </form>

    <div class="row">
        <?php
        /*$data = array("sort"=>"rand","limit"=>8,"not_id"=>$Item->id);
        $list = $S->ProductsList($data);
        $opts = new stdClass();
        $opts->col = "col-md-3 col-sm-6 col-xs-6";
        foreach($list->list as $item){
            echo $UI->ProductItem( $item->id , $opts);
        }*/
        ?>
    </div>
</div>
<br><br>

<?php
include("_ext/include/guida_taglie.inc.php");
include("_ext/include/guida_materiali.inc.php");
?>