<?php include("_ext/include/submenu.inc.php"); ?>

<div class="container">
      <div class="row">
            <div class="col-sm-12">
                  <h3>
                        <b>Accademia</b>
                        <small><# CONDIZIONI GENERALI DI VENDITA #></small>
                  </h3>
                  <hr>
            </div>
            <div class="col-sm-12">
                  <?php if($S->id_lang==1 ){ ?>
                      <div class="text-justify">
                          <a id="art-termini-condizioni">&nbsp;</a>

                          <p>
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus ligula in ante egestas laoreet. Vestibulum pharetra ex nec cursus porta. Ut volutpat risus ante, non mollis sapien laoreet et. Vestibulum porttitor orci id venenatis cursus. Donec sed ex tellus. Donec elit lorem, euismod sit amet elit sed, tempus tempus mauris. Nullam mattis tellus risus, in dapibus risus faucibus in. Aliquam vel purus diam. Vivamus dictum dui ut massa mattis, sed viverra odio congue. Nam aliquam orci sed nisl blandit vehicula. Phasellus ac commodo libero. Cras nibh odio, ultricies quis ipsum finibus, sodales tincidunt eros. Maecenas faucibus tortor justo, id bibendum nibh pellentesque ultricies. Mauris massa dolor, aliquet in magna vitae, tincidunt viverra dolor.

                              Donec viverra sollicitudin iaculis. Maecenas gravida nulla vitae quam consequat, eget commodo neque ultricies. Mauris mollis ipsum at odio mollis elementum. Donec maximus, velit eu feugiat varius, sapien purus tristique felis, eu faucibus ante libero ut ligula. Praesent hendrerit nulla arcu, id tincidunt leo feugiat eu. Maecenas blandit erat tempor aliquam faucibus. Aenean iaculis ipsum pretium sapien egestas eleifend. Praesent interdum convallis diam, sed elementum quam maximus sit amet. Praesent vestibulum non nisi sed pellentesque.

                              Duis porta mattis nisl, scelerisque consectetur arcu finibus ac. Aliquam nec sem id nibh vehicula ullamcorper. Sed laoreet eros turpis, sed posuere quam sodales dapibus. Quisque a dictum orci. Aenean id ultrices risus. Donec hendrerit dolor eros, vel vestibulum dui semper in. Nunc scelerisque tincidunt ipsum, sit amet vehicula dui pretium non. Suspendisse bibendum sem eu leo varius, eget elementum orci pulvinar. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc et scelerisque nisl, a bibendum metus. Phasellus mollis elementum nibh, et eleifend libero finibus at. Cras interdum porta fringilla. Sed porttitor libero a augue accumsan, lacinia pellentesque eros tempus. Curabitur dolor erat, auctor nec blandit quis, consectetur nec orci.

                              Fusce sed eros in dolor blandit euismod eu sit amet tellus. Vestibulum non lorem sodales, ultricies lectus nec, tincidunt lectus. Proin vehicula sem a eleifend pharetra. Aenean id mi accumsan, tincidunt lacus eget, vestibulum ligula. Duis pretium, mauris quis tempus dignissim, nisl arcu cursus ipsum, quis posuere dolor enim quis felis. Fusce porttitor placerat finibus. Cras semper fermentum metus eget ornare. Praesent lacinia metus arcu, vel finibus turpis scelerisque quis. Ut sit amet tellus mauris. Cras mi justo, sagittis finibus mauris vitae, auctor ultrices ex. Donec interdum velit quis elit congue feugiat.
                          </p>

                      </div>
			      <?php }else if($S->id_lang==2){ ?>
                      <div class="text-justify">
                          <a id="art-termini-condizioni">&nbsp;</a>

                      </div>
			      <?php } ?>
            </div>
      </div>
</div>
<br><br>