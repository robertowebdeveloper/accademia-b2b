<?php include("_ext/include/submenu.inc.php"); ?>

<div class="Margin"></div><div class="Margin"></div><div class="Margin"></div>
<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<?php include("_ext/include/menu_account.inc.php"); ?>
		</div>		
	</div>
	<br><br>
	<form id="accountForm">
		<input type="hidden" name="act" value="saveAccount">
		<input type="hidden" name="id_customer" value="<?=$S->Customer->id; ?>">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h4 class="uppercase"><# I tuoi dati #></h4>
				<hr>
				<table class="table table-striped table-bordered">
					<tr>
						<td width="30%"><label><# Ragione sociale #></label></td>
						<td>
							<b><?=$S->Customer->company; ?></b>
							<input type="text" class="form-control" name="company" value="<?=$S->Customer->company; ?>">
						</td>
					</tr>
					<tr>
						<td><label><# Partita iva #></label></td>
						<td>
							<b><?=$S->Customer->vat; ?></b>
							<input type="text" class="form-control" name="vat" value="<?=$S->Customer->vat; ?>">
						</td>
					</tr>
					<tr>
						<td width="30%">
							<label>
								<# Nome #>
							</label>
						</td>
						<td>
							<input type="text" name="name-bill" class="form-control" value="<?=$S->Customer->name; ?>">
						</td>
					</tr>
					<tr>
						<td width="30%">
							<label>
								<# Cognome #>
							</label>
						</td>
						<td>
							<input type="text" name="surname-bill" class="form-control" value="<?=$S->Customer->surname; ?>">
						</td>
					</tr>
					<tr>
						<td><label><# Stato #></label></td>
						<td>
							<input type="hidden" name="id_country-bill" value="<?=$S->Customer->BillingInfo->id_country; ?>">
							<select class="form-control"><?php
								$list = $S->CountryList("sell_enable");
								foreach($list as $c){
									if( $c->id==$S->Customer->BillingInfo->id_country ){
										?><option value="<?=$c->id; ?>"<?=$sel; ?>><?=$c->name_inter; ?></option><?php
									}
								}
							?></select>
						</td>
					</tr>
					<tr>
						<td><label><# Città #><sup>*</sup></label></td>
						<td><input type="text" name="city-bill" class="form-control" value="<?=$S->Customer->BillingInfo->city; ?>"></td>
					</tr>
					<tr>
						<td><label><# Indirizzo #><sup>*</sup></label></td>
						<td><input type="text" name="address-bill" class="form-control" value="<?=$S->Customer->BillingInfo->address; ?>"></td>
					</tr>
					<tr>
						<td><label><# Numero civico #></label></td>
						<td><input type="text" name="address_number-bill" class="form-control form-auto" maxlength="20" size="15" value="<?=$S->Customer->BillingInfo->address_number; ?>"></td>
					</tr>
					<tr>
						<td><label><# CAP #><sup>*</sup></label></td>
						<td><input type="text" name="zip-bill" class="form-control form-auto" maxlength="20" size="15" value="<?=$S->Customer->BillingInfo->zip; ?>"></td>
					</tr>
					<tr>
						<td><label><# Telefono #><sup>*</sup></label></td>
						<td><input type="text" name="tel-bill" class="form-control" value="<?=$S->Customer->BillingInfo->tel; ?>"></td>
					</tr>
					<tr>
						<td><label><# Telefono (2) #></label></td>
						<td><input type="text" name="tel2-bill" class="form-control" value="<?=$S->Customer->BillingInfo->tel2; ?>"></td>
					</tr>
					<tr>
						<td><label><# E-mail aggiuntive #></label></td>
						<td>
							<input type="text" name="email_address-bill" class="form-control" value="<?=$S->Customer->BillingInfo->email_address; ?>">
							<span class="help-block size2"><# Puoi inserire indirizzi multipli separandoli da virgola (primary@domain.ext,secondary@domain.ext,...) #></span>
						</td>
					</tr>
				</table>			
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h4 class="uppercase"><# Dati di spedizione #></h4>
				
				<hr>
				
				<table class="table table-striped table-bordered">
					<tr>
						<td width="30%"><label><# Ragione sociale #></label></td>
						<td>
							<b><?=$S->Customer->company; ?></b>
							<input type="text" class="form-control" name="company_sped" value="<?=$S->Customer->company; ?>">
						</td>
					</tr>
					<tr>
						<td width="30%">
							<label>
								<# Nome #>
							</label>
						</td>
						<td>
							<input type="text" name="name-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->name; ?>">
						</td>
					</tr>
					<tr>
						<td width="30%">
							<label>
								<# Cognome #>
							</label>
						</td>
						<td>
							<input type="text" name="surname-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->surname; ?>">
						</td>
					</tr>
					<tr>
						<td><label><# Stato #></label></td>
						<td>
							<select name="id_country-shipping" class="form-control"><?php
								$list = $S->CountryList("sell_enable");
								foreach($list as $c){
									$sel = $c->id==$S->Customer->ShippingInfo[0]->id_country ? ' selected' : '';
									?><option value="<?=$c->id; ?>"<?=$sel; ?>><?=$c->name_inter; ?></option><?php
								}
							?></select>
						</td>
					</tr>
					<tr>
						<td><label><# Città #><sup>*</sup></label></td>
						<td><input type="text" name="city-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->city; ?>"></td>
					</tr>
					<tr>
						<td><label><# Indirizzo #><sup>*</sup></label></td>
						<td><input type="text" name="address-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->address; ?>"></td>
					</tr>
					<tr>
						<td><label><# Numero civico #></label></td>
						<td><input type="text" name="address_number-shipping" class="form-control form-auto" maxlength="20" size="15" value="<?=$S->Customer->ShippingInfo[0]->address_number; ?>"></td>
					</tr>
					<tr>
						<td><label><# CAP #><sup>*</sup></label></td>
						<td><input type="text" name="zip-shipping" class="form-control form-auto" maxlength="20" size="15" value="<?=$S->Customer->ShippingInfo[0]->zip; ?>"></td>
					</tr>
					<tr>
						<td><label><# Telefono #><sup>*</sup></label></td>
						<td><input type="text" name="tel-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->tel; ?>"></td>
					</tr>
					<tr>
						<td><label><# Telefono (2) #></label></td>
						<td><input type="text" name="tel2-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->tel2; ?>"></td>
					</tr>
					<tr>
						<td><label><# E-mail aggiuntive #></label></td>
						<td>
							<input type="text" name="email_address-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->email_address; ?>">
							<span class="help-block size2"><# Puoi inserire indirizzi multipli separandoli da virgola (primary@domain.ext,secondary@domain.ext,...) #></span>
				    		</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-5 col-sm-2 col-sm-offset-5 col-xs-12 text-center">
				<br>
				<a href="#" class="Btn btn btn-lg _btnSave">
					<span class="_text"><# Salva #></span>
					<span class="loader loader16 hide"></span>
				</a>
			</div>
		</div>
	</form>
	
	
	
	
	<br><br><br>
</div>
