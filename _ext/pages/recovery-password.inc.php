<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<br>
			
			<form id="recoveryPwForm" role="form">
				<input type="hidden" name="act" value="recovery-pw">
				<h3><# Password dimenticata? #></h3>
				
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group">			
						<label><# Inserisci il tuo indirizzo e-mail #></label>
						<input type="text" name="email" class="form-control" placeholder="<# E-mail #>">
					</div></div>
					<div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group">
						<label>&nbsp;</label><br>
						<button type="submit" class="Btn Btn-green">
							<span><# Recupera password #></span>
							<span class="loader loader16" style="top: 10px; margin-left: -8px;"></span>
						</button>
					</div></div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-success hide">
							<b><# &Egrave; stata inviata una nuova password al tuo indirizzo e-mail #></b>
						</div>
					</div>
				</div>
			</form>	
					
			<hr>
			<ul class="confirm-registration-info">
				<li><# Se non ricevi la mail ricordati di verificare la tua casella di posta indesiderata (spam) #></li-->
				<li><# Per assistenza puoi contattarci: #> <span data-e="servizioclienti|sottosotto.it"></span></li>
			</ul>
			<hr>

			<div class="text-center"><img src="{{img}}top-quality.jpg" alt="" class="img-responsive img-rounded"></div>
			<br>
			
		</div>
	</div>
</div>