<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<?php include("_ext/include/menu_account.inc.php"); ?>
		</div>		
	</div>
	<br><br>
	<form id="accountForm">
		<input type="hidden" name="act" value="changePw">
		<input type="hidden" name="id_customer" value="<?=$S->Customer->id; ?>">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h4 class="uppercase"><# Modifica Password #></h4>
				<hr>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="33%">
								<# Password corrente #>
							</th>
							<th width="34%">
								<# Nuova password #>
							</th>
							<th width="33%">
								<# Conferma password #>
							</th>
						</tr>
					</thead>
					<tr>
						<td><input type="password" name="pw" class="form-control" placeholder="<# Password corrente #>"></td>
						<td><input type="password" name="new_pw" class="form-control" placeholder="<# Nuova password #>"></td>
						<td><input type="password" name="new_pw2" class="form-control" placeholder="<# Conferma password #>"></td>
					</tr>
					<tr>
						<td colspan="3">
							<div id="return_msg" class="Red pull-left"></div>
							<div class="text-right">
								<a href="#" class="Btn Btn-green _btnSave">
									<span class="_text"><# Modifica password #></span>
									<span class="loader loader16 hide"></span>
								</a>
							</div>
						</td>
					</tr>
				</table>			
			</div>
		</div>
	</form>
	
	<br><br><br>
</div>
