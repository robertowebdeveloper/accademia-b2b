<?php include("_ext/include/submenu.inc.php"); ?>

<div class="Margin"></div><div class="Margin"></div><div class="Margin"></div>

<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<?php include("_ext/include/menu_account.inc.php"); ?>
		</div>		
	</div>
	<br>
	
	<?php
	$show_detail = $show_return = false;
	if( isset($_GET['d']) ){
		$Basket = $S->Order_getOrderByCustomer($S->Customer->id,$_GET['d']);
		if( $Basket ){
			//$S->pr( json_decode($Basket->shipping_data,false) );
			//$S->pr($Basket);
			$show_detail = true;
		}

		if( $Basket->return_available && isset($_GET['return']) && $_GET['return']==1 ){
			$show_return = true;
		}
	}
	
	if($show_detail){
		echo 'qi';exit;
	?>
		<div class="row">
			<div class="col-md-12">
				<a href="{{url ordini}}" class="btn">&laquo; <# Elenco Ordini #></a>
				<hr>

				<?php if($show_return){ ?>
					<div class="text-center">
						<h4 class="warning uppercase"><# Procedura di reso #></h4>
					</div>
					<br>
					<div class="alert alert-warning">
						<# Vuoi procedere al reso di questo ordine?<br>Ti invitiamo a compilare il modulo sottostante, selezionando il/i capo/i per il quale richiedi il reso. #>
					</div>
				<?php } ?>

				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<td width="50%"><# Ordine #></td>
							<td width="50%"><# Data #></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<b><?=$S->Order_codeFromId($Basket->id); ?></b>
							</td>
							<td>
								<b><?=$S->Date($Basket->date); ?></b>
							</td>
						</tr>
					</tbody>
				</table>

				<form id="OrderReturn" role="form">
					<input type="hidden" name="id" value="<?=$Basket->id; ?>">
					<input type="hidden" name="act" value="order-return">

					<table id="BasketTable" class="table table-striped">
						<thead>
							<tr>
								<th colspan="2"><# Prodotto #></th>
								<th width="100"><# Prezzo unitario  #></th>
								<th width="120" class="text-center"><# Q.tà #></th>
								<th width="100"><# Totale #></th>
								<?php if($show_return && 0){ ?>
									<th width="150" class="text-center">
										<# Richiedi reso? #>
									</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($Basket->list as $row){
								//$S->pr($row);
								if( count( $row->Product->foto)>0 ){
									$img = $S->Img( $row->Product->foto[0]->path ,'prod_item');
								}else{
									$img = '{{img}}foto_def.png';
								}
								$opts = new stdClass();
								$opts->type = 'product-item';
								$opts->id = $row->Product->id;

								$url = $S->Url('product-item', $opts);
							?>
							<tr>
								<td width="100"><a href="<?=$url; ?>"><img src="<?=$img; ?>" alt="" class="img-responsive"></a></td>
								<td>
									<a href="<?=$url; ?>"><?=$row->Product->title; ?><br>
									<# Taglia: #> <?=$row->Product->taglia; ?> - <# Colore: #> <?=$row->Product->colore; ?></a>
								</td>
								<td>
									<input type="hidden" name="price[<?=$row->id_product_simple; ?>]" value="<?=$row->price; ?>">
									<span class="price">&euro; <?=$S->Money($row->price); ?></span>
								</td>
								<td class="text-center">
									<span class="price"><?=$row->qty; ?></span>
								</td>
								<td class="text-center"><span class="price">&euro; <?=$S->Money($row->price*$row->qty); ?></span></td>

								<?php if($show_return){ ?>
									<td class="text-center">
										<select name="prod_return[<?=$row->idart; ?>]" class="form-control">
											<option value="0">No</option>
											<?php
											for($i=1;$i<=$row->qty;$i++){
												if( $row->qty==1 && $i==1 ){
													$txt = $this->W('Sì');
												}else if( $row->qty>1 && $i==$row->qty ){
													$txt = $this->W('Tutti');
												}else{
													$txt = $i;
												}
												?>
												<option value="<?=$i; ?>"><?=$txt; ?></option>
											<?php
											}
											?>
										</select>
									</td>
								<?php } ?>

							</tr>
							<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" class="text-right"><# Totale ordine #>:</td>
								<td class="text-center"><?=$Basket->totPrice_txt; ?></td>
								<?php if($show_return){ ?>
									<td></td>
								<?php } ?>
							</tr>
							<?php if($Basket->vat>0){ ?>
								<tr>
									<td colspan="4" class="text-right"><# IVA #>:</td>
									<td class="text-center"><?=$Basket->vat_cost_txt; ?></td>
									<?php if($show_return){ ?>
										<td></td>
									<?php } ?>
								</tr>
							<?php } ?>
							<?php if($Basket->buono_cost>0){ ?>
								<tr>
									<td colspan="4" class="text-right"><# Coupon #>:</td>
									<td class="text-center"><?=$Basket->buono_cost_txt; ?></td>
									<?php if($show_return){ ?>
										<td></td>
									<?php } ?>
								</tr>
							<?php } ?>
							<tr>
								<td colspan="4" class="text-right"><h4><# Totale #>:</h4></td>
								<td class="text-center"><h4><?=$Basket->totBasket_txt; ?></h4></td>

								<?php if($show_return){ ?>
									<td></td>
								<?php } ?>

							</tr>
						</tfoot>
					</table>

					<?php if($show_return){ ?>
						<hr>
						<h4><# Note #><sup>*</sup></h4>
						<span class="help-block">
							<# Inserisci una breve spiegazione dei problemi che hai trovato #>
						</span>

						<textarea name="note" class="form-control" rows="4"></textarea>

						<br>
						<sup>*</sup>campi necessari
						<hr>

						<div class="text-right">
							<button type="submit" class="Btn big Btn-green">
								<span class="_text"><# Invia richiesta reso #></span>
								<span class="loader loader16" style="top: 10px; margin-left: -8px;"></span>
							</button>
						</div>

					<?php } ?>

				</form>
			</div>
		</div>
	<?php }else{ ?>	
		<div class="row">
			<div class="col-md-12">
				<h4 class="uppercase"><# I tuoi ordini #></h4>
				<hr>
				<?php
				$list = $S->Order_orderList($S->Customer->id);
				$n = count($list);
				if($n==0){
				?>
					<div class="alert alert-warning">
						<b><# Nessun ordine presente #></b>
					</div>
				<?php }else{ ?>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th><# Ordine #></th>
								<th width="10%" class="text-center"><# Data #></th>
								<!--th width="20%"><# Status #></th-->
							</tr>
						</thead>
						<tbody>
							<?php foreach($list as $item){ ?>
								<tr>
									<td>
										<a href="{{url ordini}}?d=<?=md5( $item->id ); ?>" class="pull-right btn Btn btn-sm"><i class="glyphicon glyphicon-file"></i> <# Dettaglio #></a>
										<?php if($item->return_available && 0){ ?>
											<a href="{{url ordini}}?d=<?=md5($item->id); ?>&return=1" class="pull-right btn btn-sm btn-warning" style="margin-right: 1em;"><# Richiedi reso #></a>
										<?php } ?>

										<?=$S->Order_codeFromId($item->id); ?>
									</td>
									<td class="text-center"><?=$S->Date($item->date); ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
	<br><br><br>
</div>
