<?php
include("_ext/include/submenu.inc.php");
?>

<?php
$Basket = $S->Order_getBasket();
//$S->pr($Basket);
//$S->pr($S->Customer);

$need_help = '
    <hr>

    <div class="help">
        <div class="text-center uppercase"><b><# Ti serve supporto? #></b></div>
        <ul class="text-center">
            <li>
                <i class="glyphicon glyphicon-earphone"></i>
                <a href="tel:+390550000000"><# Chiamaci +39 055 0000000 #></a>
            </li>
        </ul>
    </div>';
$need_help = '<br>';

if( isset($_GET['dc']) && $_GET['dc']>0){//Rimuove coupon
    $S->Order_removeCoupon($Basket->id);
    $this->Order_getBasket_tmp = null;
    $Basket = $S->Order_getBasket();
}

if( !$S->Customer->sell_enable ){ //Se il paese dell'utente non è abilitato all'acquisto
    $k = 'basket';
}else{
    $k = $this->Page->k;
}


//Verifica del carrello
$msg_back_to_first = false;
if( $k=='basket-order-confirmation' ){
    $ck = $S->Order_IntegrityBasket($Basket->id);
    if( !$ck ){
        $k = 'basket';
        $msg_back_to_first = true;
    }
}
switch( $k ){
    case 'basket':
        $_Step = 1;
        $action_url = 'basket-checkout';
        $action_this = 'basket';
        $action_prev = 'basket';
        $action = false;
        break;
    case 'basket-checkout':
        $_Step = 2;
        $action_url = 'basket-order-confirmation';
        $action_this = 'basket-checkout';
        $action_prev = 'basket';
        $action = false;

        $S->Order_setBasketCustomer();//Fissa il customer del basket
        //Elimina dal carrello prodotti con quantità pari a 0 (in caso di carrello con recupero) e Prodotti Reali nel caso sia un carrello di test
        $S->Order_removeEmptyBasketRows();

        break;
    case 'basket-order-confirmation':
        $_Step = 3;
        $action_url = 'basket-order-confirmation';
        $action_this = 'basket-order-confirmation';
        $action_prev = 'basket-checkout';

        if( $Basket->payment=='cc' ) {
            $action = "https://ecommerce.keyclient.it/ecomm/ecomm/DispatcherServlet";

            $codtrans = 'so_' . str_pad($Basket->id, 7, "0", STR_PAD_LEFT);
            $codtrans .= "." . $S->Order_BankShift($Basket->id,$Basket->bank_shift);

            /*
             * RISPOSTA CORRETTA DALLA BANCA:
             * http://test.inol3.com/SottoSotto/it/ordine-concluso?regione=&session_id=a5204frbvos7ppthc75h6n4ar1&tipoTransazione=VBV_FULL&data=20150827&mac=5bfe4edcc5b07e328c53a593ea6188742e4eb2a2&tipoProdotto=VISA+BUSINESS+-+CREDIT+-+N&nazionalita=ITA&OPTION_CF=&esito=OK&scadenza_pan=201604&messaggio=Message+OK&mail=domini%40inol3.com&codAut=740906&alias=payment_756965&codiceEsito=0&orario=112146&importo=1&languageId=ITA&cognome=Palagano&pan=453220XXXXXX6276&divisa=EUR&codiceWallet=3091423&brand=VISA&nome=Vito&codTrans=so_0000037
             *
             * RISPOSTA ERRATA:
             * http://test.inol3.com/SottoSotto/it/ordine-concluso?mail=domini%40inol3.com&session_id=a5204frbvos7ppthc75h6n4ar1&regione=&tipoTransazione=VBV_MERCHANT&codAut=&alias=payment_3444153&codiceEsito=400&orario=121005&data=20150827&mac=c5051a6400f53d4c96cfd61114fa1375c9d30fbc&importo=1&tipoProdotto=+-++-+&cognome=b&languageId=ITA&pan=455641XXXXXX4963&nazionalita=&divisa=EUR&OPTION_CF=&scadenza_pan=201703&esito=KO&brand=VISA&codTrans=so_0000037&nome=a&messaggio=Auth.+Denied
             */


            if (server == 'live' && !$Basket->test && 0) {
                $importo = $Basket->totBasket;
                $input_type = 'hidden';

                if( false && $_SERVER['REMOTE_ADDR']=='188.9.55.27'){
                    $importo = .01;
                    $input_type = 'text';
                }

                $BankData = array(
                    //'input_type' => 'text',
                    'input_type' => $input_type,
                    'alias' => 'payment_756965',
                    'key' => '3LT2T7qt5M472K64Y0D3B21948S3Dt4MJMH0EHFJ',
                    'divisa' => 'EUR',
                    'codTrans' => $codtrans,
                    'importo' => round($importo * 100)
                );
            } else {
                $BankData = array(
                    'input_type' => 'text',
                    //'input_type' => 'hidden',
                    'alias' => 'payment_3444153',
                    'key' => 'TLGHTOWIZXQPTIZRALWKG',
                    /*'alias' => 'payment_977085',
                    'key' => 'P96O1rR18Fn34K9G8Nf851lN42A655HOr7ssZ7de',*/
                    'divisa' => 'EUR',
                    'codTrans' => $codtrans,
                    'importo' => 1
                );
            }

            $mac = sha1("codTrans={$codtrans}divisa={$BankData['divisa']}importo={$BankData['importo']}{$BankData['key']}");

            $BankData['mac'] = $mac;
            unset($BankData['key']);
            $BankData['url'] = $S->DomainUrl() . $S->Url('order-response');
            $BankData['url_back'] = $S->DomainUrl() . $S->Url('order-response');
            $BankData['urlpost'] = $S->DomainUrl() . path_webroot . 'bank_s2s.php';
            $BankData['mail'] = $S->Customer->email;
            include("_ext/scripts/bank_language_id.inc.php");
            $BankData['languageId'] = $Bank_language_id[$S->_lang];
            $BankData['session_id'] = $Basket->session;
        }else if( $Basket->payment=='paypal'){
            $action = _DEBUG || $Basket->test ? "https://www.sandbox.paypal.com/cgi-bin/webscr" : "https://www.paypal.com/cgi-bin/webscr";
        }
        break;
}
?><section id="Basket"><div class="container"><form id="BasketForm" role="form" enctype="application/x-www-form-urlencoded" method="post" data-action="{{url <?=$action_url; ?>}}" data-action-prev="{{url <?=$action_prev; ?>}}" data-action-this="{{url <?=$action_this; ?>}}"<?=$action ? ' action="' . $action . '"' : ' class="ajaxform"'; ?>>
            <input type="hidden" name="id_basket" value="<?=$Basket->id; ?>">
            <input type="hidden" name="id_lang" value="<?=$S->id_lang; ?>">
            <input type="hidden" name="id_country" value="<?=$S->id_country; ?>">
            <input type="hidden" name="id_customer" value="<?=$S->id_customer; ?>">
            <input type="hidden" id="step" name="step" value="<?=$_Step; ?>">

            <div class="Margin"></div>
            <div class="Margin"></div>

            <div class="row">
                <?php
                if( count($Basket->list)==0 ){
                    ?>
                    <div class="col-md-12">
                        <br><br><br><br><br>
                        <h2 class="text-center Red"><# Il tuo carrello è vuoto #></h2>
                    </div>
                <?php }else{ ?>

                <?php if($_Step==1 || $_Step==3 ): ?>
                    <div class="col-md-12">
                        <?php include("_ext/include/basket_navbar.inc.php"); ?>
                    </div>
                    <div class="col-sm-9">
                        <?php if($msg_back_to_first){ ?>
                            <div class="alert alert-danger"><# Si è verificato un errore: cortesemente verifica il tuo carrello e continua con l&apos;ordine da qui. <b>Grazie</b> #></div>
                        <?php } ?>
                        <table id="BasketTable" class="table table-striped">
                            <thead>
                            <tr>
                                <th width="100"> </th>
                                <th><# Articolo #></th>
                                <th width="100" class="hidden-xs"><# Prezzo #></th>
                                <th width="120" class="text-center"><# Q.tà #></th>
                                <th width="100" class="text-center"><# Totale #></th>
                                <?php if($_Step==1){ ?>
                                    <th width="16"> </th>
                                <?php } ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($Basket->list as $row){
                                $qty_available=$S->Qty($row->idart,true);
                                //$S->pr($row);
                                $img = false;
                                if( count($row->Product->foto)>0 ){
                                    foreach($row->Product->foto as $f){
                                        if( $f->cod_colore==$row->Product->colore ){
                                            $img = $f->path;
                                        }
                                    }
                                }
                                $opts = new stdClass();
                                $opts->type = 'product-item';
                                $opts->id = $row->Product->id;

                                $url = $S->Url('product-item', $opts);
                                ?>
                                <tr>
                                    <td><a href="<?=$url; ?>"><img src="<?=$S->Img($img,'prod_thumb_colore'); ?>" alt="" class="img-responsive"></a></td>
                                    <td class="va">
                                        <a href="<?=$url; ?>">
                                            <b><?=$row->Product->griffe; ?></b>
                                            <br>
                                            <?=$row->Product->categoria; ?><br>
                                        </a>
                                    </td>
                                    <td class="hidden-xs">
                                        <input type="hidden" name="price[<?=$row->idart; ?>]" value="<?=$row->price; ?>">
                                        <span class="price">&euro; <?=$S->Money($row->price); ?></span>
                                    </td>
                                    <td>
                                        <?php if($_Step==3): ?>
                                            <div class="text-center"><?=$row->qty; ?></div>
                                        <?php else: ?>
                                            <?php if( $qty_available>0 ){
                                                ?>
                                                <select name="qty[<?=$row->idart; ?>]" data-id_ps="<?=$row->idart; ?>" class="form-control">
                                                    <?php
                                                    for($i=1;$i<=$qty_available;$i++){
                                                        $sel = $row->qty==$i ? ' selected' : '';
                                                        ?><option value="<?="{$i}"; ?>"<?=$sel; ?>><?=$i; ?></option><?php
                                                    }
                                                    ?>
                                                </select>

                                                <?php if( $row->qty_changed ){ ?><div class="qty-changed-alert"><span><# La disponibilità del prodotto è cambiata #></span></div><?php } ?>
                                            <?php }else{ ?>
                                                <span class="Red"><# Il prodotto non è più disponibile #></span>
                                                <input type="hidden" name="qty[<?=$row->idart; ?>]" value="0">
                                            <?php } ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($_Step==1): ?>
                                            <?php if($qty_available>0){ ?><span class="price" data-id="price_<?=$row->idart; ?>">-</span>
                                            <?php }else{ ?><span class="price">-</span><?php } ?>
                                        <?php else: ?>
                                            <div class="text-center"><b>&euro; <?=$S->Money($row->totPriceRow,2); ?></b></div>
                                        <?php endif; ?>
                                    </td>
                                    <?php if($_Step==1){ ?>
                                        <td class="text-center">
                                            <a href="#" class="remove" data-id="<?=$row->idart; ?>" data-text-confirm="<# Rimuovere prodotto? #>">
                                                <i class="glyphicon glyphicon-remove hidden-xs"></i>
                                                <span class="visible-xs-block"><# Rimuovere #></span>
                                            </a>
                                        </td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <hr>

                        <?php if($_Step==1): ?>
                            <a href="{{url products}}" class="Btn btn hidden-xs"><# Continua con gli acquisti #></a>
                        <?php elseif($_Step==3): ?>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h4 class="uppercase"><# I tuoi dati #></h4>
                                    <hr>
                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <td>
                                                <div><# Nome #></div>
                                                <b><?=$S->Customer->name." ".$S->Customer->surname; ?></b>
                                            </td>
                                        </tr>
                                        <!--tr>
                                    <td>
                                        <div><# VAT Number #></div>
                                        <b><?=$S->Customer->vat; ?></b>
                                    </td>
                                </tr-->
                                        <tr>
                                            <td>
                                                <div><# Stato #></div>
                                                <?php
                                                $x = $S->getCountry($S->Customer->BillingInfo->id_country);
                                                ?>
                                                <b><?=$x->name_inter; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Città #></div>
                                                <b><?=$S->Customer->BillingInfo->city; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Indirizzo #></div>
                                                <b><?=$S->Customer->BillingInfo->address; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Numero civico #></div>
                                                <b><?=$S->Customer->BillingInfo->address_number; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# CAP #></div>
                                                <b><?=$S->Customer->BillingInfo->zip; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Telefono #></div>
                                                <b><?=$S->Customer->BillingInfo->tel; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Telefono (2) #></div>
                                                <b><?=empty($S->Customer->BillingInfo->tel2) ? '-' : $S->Customer->BillingInfo->tel2; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# E-mail aggiuntive #></div>
                                                <b><?=empty($S->Customer->BillingInfo->email_address) ? '-' : $S->Customer->BillingInfo->email_address; ?></b>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h4 class="uppercase"><# Dati di spedizione #></h4>
                                    <?php
                                    $shipping_data = json_decode( $Basket->shipping_data );
                                    //print_r($shipping_data);
                                    ?>

                                    <hr>

                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <td>
                                                <div><# Nome #></div>
                                                <b><?=$shipping_data->name ." ".$shipping_data->surname; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Stato #></div>
                                                <?php
                                                $x = $S->getCountry($shipping_data->id_country);
                                                ?>
                                                <b><?=$x->name_inter; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Città #></div>
                                                <b><?=$shipping_data->city; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Indirizzo #></div>
                                                <b><?=$shipping_data->address; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Numero civico #></div>
                                                <b><?=$shipping_data->address_number; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# CAP #></div>
                                                <b><?=$shipping_data->zip; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Telefono #></div>
                                                <b><?=$shipping_data>tel; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# Telefono (2) #></div>
                                                <b><?=empty($shipping_data->tel2) ? '-' : $shipping_data->tel2; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div><# E-mail aggiuntive #></div>
                                                <b><?=empty($shipping_data->email_address) ? '-' : $shipping_data->email_address; ?></b>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?php if($_Step==1): ?>
                            <section id="BasketTotalBox">
                                <div class="Title"><# Carrello #></div>
                                <ul>
                                    <li>
                                        <span class="info" data-id="total">-</span>
                                        <# Il tuo carrello #>
                                    </li>
                                    <?php if($Basket->buono_dati){ ?>
                                        <li>
                                            <span class="info" data-id="buono-cost">-</span>
                                            <# Coupon #>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <span class="info" data-id="vat">-</span>
                                        <# IVA #>
                                    </li>
                                    <li>
                                <span class="info">
                                    <b><# GRATIS #></b>
                                </span>
                                        <# Spedizione #>
                                    </li>
                                    <li class="tot-basket">
                                        <span class="info" data-id="total-basket">-</span>
                                        <# Totale ordine #>
                                    </li>
                                </ul>

                                <?php if( $_SESSION['id_customer']>0 && $S->Customer->sell_enable ){
                                    if( $S->BasketMin() || _DEBUG || $S->Customer->tester ){ ?>
                                        <br>
                                        <a href="{{url <?=$action_url; ?>}}" class="Btn btn block">
                                            <# Pagamento #>
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>

                                        <?php if($Basket->test){ ?><p style="font-size: .9em;"><br>
                                            <b class="Red">Il carrello presenta dei prodotti di test.</b>
                                            <br>
                                            Al passaggio successivo i prodotti REALI saranno eliminati, resteranno solo quelli di test
                                        </p><?php } ?>
                                    <?php }else{ ?>
                                        <a href="#" class="Btn btn disabled block">
                                            <# Pagamento #>
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                        <br>
                                        <b><# minimal order: %s %txt=&euro; <?=$S->Money($this->P('minimum_basket'),0); ?> #></b>
                                        <br>
                                    <?php }
                                }else{ ?>
                                    <!--a href="#" class="Btn Btn-white disabled block"><# Pagamento #></a-->
                                    <br>
                                    <div class="text-center">
                                        <a class="Btn btn block" href="#" data-login="1">
                                            <span class="glyphicon glyphicon-user"></span>
                                            &nbsp;&nbsp;
                                            <# Accedi per proseguire #>
                                        </a>
                                    </div>
                                    <?php
                                }
                                ?>

                                <?=$need_help; ?>
                            </section>
                        <?php elseif($_Step==3): ?>
                            <section id="BasketTotalBox">
                                <div class="Title"><# Carrello #></div>
                                <ul>
                                    <li>
                                        <span class="info" data-id="total"><?=$Basket->totPrice_txt; ?></span>
                                        <# Il tuo carrello #>
                                    </li>
                                    <?php if($Basket->buono_dati){ ?>
                                        <li>
                                            <span class="info" data-id="buono-cost"><?=$Basket->buono_cost_txt; ?></span>
                                            <# Coupon #>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <span class="info" data-id="vat"><?=$Basket->vat_cost_txt; ?></span></span>
                                        <# IVA #>
                                    </li>
                                    <li>
                                <span class="info">
                                    <b><# GRATIS #></b>
                                </span>
                                        <# Spedizione #>
                                    </li>
                                    <li class="tot-basket">
                                        <b class="info" data-id="total-basket"><?=$Basket->totBasket_txt; ?></span></b>
                                        <b><# Totale ordine #></b>
                                    </li>
                                </ul>

                                <br>

                                <div class="Title"><# Metodo di pagamento #></div>
                                <ul>
                                    <li class="text-center">
                                        <?php if( $Basket->payment=='paypal' ){ ?>
                                            <img src="{{img}}icons/checkout-paypal.png" width="32" alt="Paypal">
                                            PAYPAL
                                        <?php }else if($Basket->payment=='cc'){ ?>
                                            <img src="{{img}}icons/checkout-cc.png" width="32" alt="<# Carta di credito #>">
                                            <# Carta di Credito #></#>
                                        <?php }else if($Basket->payment=='bonifico'){ ?>
                                            <img src="{{img}}icons/checkout-bonifico.png" width="32" alt="<# Bonifico #>">
                                            <# Bonifico #></#>
                                        <?php }else if($Basket->payment=='contrassegno'){ ?>
                                            <img src="{{img}}icons/checkout-cc.png" width="32" alt="<# Contrassegno #>">
                                            <# Contrassegno di Credito #></#>
                                        <?php } ?>
                                        <input type="hidden" name="pay" value="<?=$Basket->totBasket; ?>">
                                    </li>
                                </ul>
                                <br>
                                <!--button type="submit" class="Btn btn block"><# Conferma  #></button-->

                                <?=$need_help; ?>
                            </section>

                            <?php if($Basket->payment=='cc'){
                                //BANK DATA
                                foreach($BankData as $k=>$v){
                                    if($k!='input_type'){
                                        ?>
                                        <input type="<?=$BankData['input_type']; ?>" name="<?=$k; ?>" value="<?=$v; ?>">
                                    <?php }
                                }
                                //END BANK DATA
                            }else if($Basket->payment=='paypal'){
                                $paypal_type_input = 'hidden';
                                //$paypal_type_input = 'text';
                                ?>
                                <input type="<?=$paypal_type_input; ?>" name="business" value="<?=paypal_business; ?>">
                                <!--input type="<?=$paypal_type_input; ?>" name="cmd" value="_xclick"-->
                                <input type="<?=$paypal_type_input; ?>" name="cmd" value="_cart">
                                <input type="<?=$paypal_type_input; ?>" name="upload" value="1">

                                <input type="<?=$paypal_type_input; ?>" name="custom" value="<?=$Basket->id; ?>">

                                <?php for($iItem=1,$i=0;$i<count($Basket->list);$iItem++,$i++){
                                    $item = $Basket->list[$i];
                                    $name = "{$item->Product->descrbase}";

                                    $price_row = $item->price;
                                    if( $Basket->buono_dati && $Basket->buono_dati->discount>0 ){
                                        $price_row = $price_row - ( $price_row * $Basket->buono_dati->discount / 100 );
                                    }
                                    ?>
                                    <input type="<?=$paypal_type_input; ?>" name="item_name_<?=$iItem; ?>" value="<?=$name; ?>">
                                    <input type="<?=$paypal_type_input; ?>" name="quantity_<?=$iItem; ?>" value="<?=$item->qty; ?>">
                                    <input type="<?=$paypal_type_input; ?>" name="amount_<?=$iItem; ?>" value="<?=number_format($price_row,2,".",""); ?>">
                                <?php } ?>

                                <?php if( $Basket->id_giftcard>0 || $Basket->id_buono>0 || $Basket->id_voucher>0 ){ ?>
                                    <input type="<?=$paypal_type_input; ?>" name="item_name_<?=$iItem; ?>" value="<# Buoni sconto #>">
                                    <input type="<?=$paypal_type_input; ?>" name="quantity_<?=$iItem; ?>" value="1">
                                    <input type="<?=$paypal_type_input; ?>" name="amount_<?=$iItem; ?>" value="<?=number_format($Basket->buono_cost,2,".",""); ?>">
                                    <?php
                                    $iItem++;
                                } ?>

                                <input type="<?=$paypal_type_input; ?>" name="shipping_1" value="<?=number_format($Basket->shipping_cost,2,".",""); ?>">
                                <input type="<?=$paypal_type_input; ?>" name="currency_code" value="EUR">

                                <input type="<?=$paypal_type_input; ?>" name="first_name" value="<?=$S->Customer->name; ?>">
                                <input type="<?=$paypal_type_input; ?>" name="last_name" value="<?=$S->Customer->surname ?>">
                                <input type="<?=$paypal_type_input; ?>" name="address1" value="<?=empty($S->Customer->ShippingInfo[0]->address) ? $S->Customer->BillingInfo->address : $S->Customer->ShippingInfo[0]->address; ?>">
                                <input type="<?=$paypal_type_input; ?>" name="zip" value="<?=empty($S->Customer->ShippingInfo[0]->zip) ? $S->Customer->BillingInfo->zip : $S->Customer->ShippingInfo[0]->zip; ?>">
                                <input type="<?=$paypal_type_input; ?>" name="country" value="IT">
                                <input type="<?=$paypal_type_input; ?>" name="city" value="<?=empty($S->Customer->ShippingInfo[0]->city) ? $S->Customer->BillingInfo[0]->city : $S->Customer->ShippingInfo[0]->city; ?>">
                                <!--input type="<?=$paypal_type_input; ?>" name="state" value="<?=empty($Customer->province_sped) ? $Customer->province : $Customer->province_sped; ?>"-->
                                <input type="<?=$paypal_type_input; ?>" name="H_PhoneNumber" value="<?=empty($S->Customer->ShippingInfo[0]->tel) ? $S->Customer->BillingInfo->tel : $S->Customer->ShippingInfo[0]->tel; ?>">
                                <input type="<?=$paypal_type_input; ?>" name="email" value="<?=$S->Customer->email; ?>">

                                <input type="<?=$paypal_type_input; ?>" name="notify_url" value="<?=$S->_path->absolute; ?>_ext/scripts/ipn_listener.php">
                                <input type="<?=$paypal_type_input; ?>" name="return" value="<?=$S->_path->absolute; ?>{{url order-response}}?esito=OK&io=<?=$Basket->id; ?>">
                                <input type="<?=$paypal_type_input; ?>" name="rm" value="2"> <?php // ritorno variabili: 1=>GET, 2=>POST ?>
                                <input type="<?=$paypal_type_input; ?>" name="cbt" value="Torna su">
                                <input type="<?=$paypal_type_input; ?>" name="cancel_return" value="<?=$S->_path->absolute; ?>{{url order-response}}?pp_cancel=1">
                                <?php
                            }
                            ?>

                        <?php endif; ?>
                    </div>

                    <?php if($_Step==1): ?>
                        <div class="col-sm-12"><hr></div>
                        <div class="col-sm-6 col-xs-12">
                            <h4 class="uppercase"><# Hai un coupon? #></h4>
                            <?php if( $Basket->id_coupon>0){ ?>
                    <# Hai usato il coupon: <b>%s</b> (-%s%) %txt=<?=$Basket->buono_dati->name; ?>|<?=$Basket->buono_dati->discount; ?> #>
                    <br><a href="{{url basket}}?dc=<?=$Basket->buono_dati->id; ?>" class="Red size2"><i><# rimuovi coupon #></i></a>
                <?php }else{ ?>
                    <div class="form-group">
                        <input type="text" id="couponCode" name="coupon" class="form-control input-inline" placeholder="<# Codice coupon #>">
                        <a href="#" onclick="Basket.couponCode();" class="btn btn-default"><# Coupon #></a>
                    </div>
                <?php } ?>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <h4 class="uppercase"><# Informazioni di pagamento #></h4>
                            <p></p>
                        </div>
                    <?php endif; ?>

                <?php elseif($_Step==2): //Checkout ?>
                <div class="col-md-12"><?php include("_ext/include/basket_navbar.inc.php"); ?></div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4 class="uppercase"><# I tuoi dati #></h4>
                    <hr>
                    <div class="form-group">
                        <label><# Nome #><sup>*</sup></label>
                        <input type="text" class="form-control" name="name-bill" value="<?=$S->Customer->name; ?>">
                    </div>
                    <div class="form-group">
                        <label><# Cognome #><sup>*</sup></label>
                        <input type="text" class="form-control" name="surname-bill" value="<?=$S->Customer->surname; ?>">
                    </div>

                    <div class="form-group hide">
                        <# VAT Number #><br>
                            <b><?=$S->Customer->vat; ?></b>
                            <input type="hidden" name="vat" value="<?=$S->Customer->vat; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Stato #></label>
                        <input type="hidden" name="id_country-bill" value="<?=$S->Customer->BillingInfo->id_country; ?>">
                        <select class="form-control"><?php
                            $list = $S->CountryList("sell_enable");
                            foreach($list as $c){
                                if( $c->id==$S->Customer->BillingInfo->id_country ){
                                    ?><option value="<?=$c->id; ?>"<?=$sel; ?>><?=$c->name_inter; ?></option><?php
                                }
                            }
                            ?></select>
                    </div>

                    <div class="form-group">
                        <label><# Città #><sup>*</sup></label>
                        <input type="text" name="city-bill" class="form-control" value="<?=$S->Customer->BillingInfo->city; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Indirizzo #><sup>*</sup></label>
                        <input type="text" name="address-bill" class="form-control" value="<?=$S->Customer->BillingInfo->address; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Numero civico #></label>
                        <input type="text" name="address_number-bill" class="form-control form-auto" maxlength="20" size="15" value="<?=$S->Customer->BillingInfo->address_number; ?>">
                    </div>

                    <div class="form-group">
                        <label><# CAP #><sup>*</sup></label>
                        <input type="text" name="zip-bill" class="form-control form-auto" maxlength="20" size="15" value="<?=$S->Customer->BillingInfo->zip; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Telefono #><sup>*</sup></label>
                        <input type="text" name="tel-bill" class="form-control" value="<?=$S->Customer->BillingInfo->tel; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Telefono (2) #></label>
                        <input type="text" name="tel2-bill" class="form-control" value="<?=$S->Customer->BillingInfo->tel2; ?>">
                    </div>

                    <div class="form-group">
                        <label><# E-mail aggiuntive #></label>
                        <input type="text" name="email_address-bill" class="form-control" value="<?=$S->Customer->BillingInfo->email_address; ?>">
                        <span class="help-block size2"><# Puoi inserire più indirizzi di posta separandoli da virgola (primary@domain.ext,secondary@domain.ext,...) #></span>
                    </div>

                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4 class="uppercase"><# Dati di spedizione #></h4>
                    <hr>
                    <a href="#" class="pull-right" onclick="Basket.copyCustomerData();"><# L&apos;indirizzo di spedizione è lo stesso? #></a>

                    <div class="form-group">
                        <label><# Nome #><sup>*</sup></label>
                        <input type="text" name="name-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->name; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Cognome #><sup>*</sup></label>
                        <input type="text" name="surname-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->surname; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Stato #></label>
                        <select name="id_country-shipping" class="form-control"><?php
                            $list = $S->CountryList("sell_enable");
                            foreach($list as $c){
                                $sel = $c->id==$S->Customer->ShippingInfo[0]->id_country ? ' selected' : '';
                                ?><option value="<?=$c->id; ?>"<?=$sel; ?>><?=$c->name_inter; ?></option><?php
                            }
                            ?></select>
                    </div>

                    <div class="form-group">
                        <label><# Città #><sup>*</sup></label>
                        <input type="text" name="city-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->city; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Indirizzo #><sup>*</sup></label>
                        <input type="text" name="address-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->address; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Numero civico #></label>
                        <input type="text" name="address_number-shipping" class="form-control form-auto" maxlength="20" size="15" value="<?=$S->Customer->ShippingInfo[0]->address_number; ?>">
                    </div>

                    <div class="form-group">
                        <label><# CAP #><sup>*</sup></label>
                        <input type="text" name="zip-shipping" class="form-control form-auto" maxlength="20" size="15" value="<?=$S->Customer->ShippingInfo[0]->zip; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Telefono #><sup>*</sup></label>
                        <input type="text" name="tel-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->tel; ?>">
                    </div>

                    <div class="form-group">
                        <label><# Telefono (2) #></label>
                        <input type="text" name="tel2-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->tel2; ?>">
                    </div>

                    <div class="form-group">
                        <label><# E-mail aggiuntive #></label>
                        <input type="text" name="email_address-shipping" class="form-control" value="<?=$S->Customer->ShippingInfo[0]->email_address; ?>">
                        <span class="help-block size2"><# Puoi inserire più indirizzi di posta separandoli da virgola (primary@domain.ext,secondary@domain.ext,...) #></span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h4 class="uppercase"><# Metodo di pagamento #></h4>
                    <hr>

                    <section id="BasketTotalBox">
                        <input type="hidden" name="vat_perc" value="<?=$Basket->vat>0 ? $Basket->vat : 0; ?>">
                        <input type="hidden" name="vat_cost" value="<?=$Basket->vat_cost>0 ? $Basket->vat_cost : 0; ?>">
                        <div class="Title"><# Verifica il tuo ordine #></div>
                        <ul>
                            <li class="tot-basket">
                                <span class="info total" data-id="total-basket"><?=$Basket->totBasket_txt; ?></span>
                                <# Totale ordine #>
                            </li>
                        </ul>
                        <hr>
                        <h4 class="text-center uppercase"><# Scegli il metodo di pagamento #></h4>

                        <ul class="choicePayment">
                            <li>
                                <label>
                                    <img src="{{img}}icons/checkout-paypal.png" width="32" alt="Paypal">
                                    PAYPAL
                                    <input type="radio" name="payment" value="paypal">
                                </label>
                            </li>
                            <li>
                                <label>
                                    <img src="{{img}}icons/checkout-bonifico.png" width="32" alt="<# Carta di credito #>">
                                    <# Carta di credito #>
                                        <input type="radio" name="payment" value="cc">
                                </label>
                            </li>
                            <li>
                                <label>
                                    <img src="{{img}}icons/checkout-cc.png" width="32" alt="<# Bonifico #>">
                                    <# Bonifico #>
                                    <input type="radio" name="payment" value="bonifico">
                                </label>
                            </li>
                            <li>
                                <label>
                                    <img src="{{img}}icons/checkout-contrassegno.png" width="32" alt="<# Contrassegno #>">
                                    <# Contrassegno #>
                                    <input type="radio" name="payment" value="contrassegno">
                                </label>
                            </li>
                            <input type="hidden" name="advance_payment_perc" value="<?=$Basket->advance_payment; ?>">
                            <input type="hidden" name="advance_payment_cost" value="<?=$Basket->advance_payment_cost; ?>">
                        </ul>
                        <div class="text-center">
                            <i class="icon amex"></i>
                            <i class="icon mastercard"></i>
                            <i class="icon visa"></i>
                        </div>
                        <br>
                        <?php
                        if( $S->BasketMin() || _DEBUG || isset($_GET['inol3']) ){ ?>
                            <a href="#" data-href="{{url <?=$action_url; ?>}}" onclick="Basket.Confirmation();" class="Btn btn block btnConfirmation">
                            <span class="_text">
                                <# Conferma #>
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                            </span>
                                <span class="loader loader16"></span>
                            </a>
                        <?php }else{ ?>
                            <a href="#" class="Btn disable block"><# Conferma #></a>
                            <br>
                            <b><# minimal order: %s %txt=&euro; <?=$S->Money($this->P('minimum_basket'),0); ?> #></b>
                        <?php }	?>

                        <?=$need_help; ?>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <br><br>
                    <div class="box-highlight"><label class="text-nobold">
                            <input type="checkbox" name="privacy" value="1">&nbsp;&nbsp;
                            <# Accetta i nostri termini di utilizzo e la privacy, incluso il trasferimento delle tue informazioni personali in Italia, una giurisdizione che potrebbe non garantire uno stesso livello di protezione dei dati per la legge nel tuo paese. #>
                        </label></div>
                    <br><br>
                </div>
                <div class="col-md-4 col-sm-4 hidden-xs"></div>
            </div>

            <?php endif; ?>
            <?php } ?>

    </div>

    <?php if($_Step<2){ ?>
        <div class="row">
            <div class="col-md-12">
                <hr>

                <h3 class="text-center uppercase"><br><br><# I più venduti #></h3><br>
                <div class="row">
                    <?php
                    $data = array("sort"=>"rand","limit"=>6,"not_id"=>$Item->id);
                    $list = $S->ProductsList($data);
                    $opts = new stdClass();
                    $opts->col = "col-md-2 col-sm-4 col-xs-6";
                    foreach($list->list as $item){
                        echo $UI->ProductItem( $item->id , $opts);
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
    </form></div></section>


<?php
$S->Order_resetBasketAlert();
?>