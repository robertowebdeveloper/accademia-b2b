var GMap = function(){//v. 0.1.0 22/09/2014
	var obj = {
		id: 'gmap',
		map: null,
		markers: new Array(),

		mapOpts: null,

		center: {
			lat: 43.3085673, //Roma
			lng: 13.2332869,
			zoom: 6,
			mapTypeId:google.maps.MapTypeId.ROADMAP
		},

		init: function(){
			var This = this;

			if( !this.mapOpts ){
				this.mapOpts = {
					center: new google.maps.LatLng( This.center.lat, This.center.lng ),
					zoom: This.center.zoom,
					mapTypeId: This.center.mapTypeId
				};
			}

			this.map = new google.maps.Map( document.getElementById( this.id ) , This.mapOpts );
		},

		setCenter: function(lat,lng,zoom){
			var pos = new google.maps.LatLng( lat,lng );
			this.map.setCenter( pos );
			if( zoom>0 ){
				this.map.setZoom(zoom);
			}
		},

		addMarker: function(lat,lng, opts){
			if( !opts ){
				var opts = {};
			}
			opts.position = new google.maps.LatLng( lat,lng );
			/*Possibili opzioni:
			 animation =  google.maps.Animation.BOUNCE;
			 icon: path immagine
			 altro....
			 */
			var marker = new google.maps.Marker( opts );
			this.markers.push( marker );
			marker.setMap( this.map );

			return marker;
		}
	};
	return obj;
};

/*google.maps.event.addDomListener(window, 'load', function(){
 var gmap = new GMap();
 gmap.init();
 });*/