if(typeof console === "undefined" || typeof console.log === "undefined"){ window.console = { log: function(msg) {
//alert(msg);
}};}
var log = function(msg){ if( _DEBUGMODE ){ console.log(msg); } };

var System = {
	sp: '_ext/scripts/ajax.php',
	st: false, //scroll Top
	ajaxTimeout: 15000,
    slimScrollSize: '7px',
	
	xs_width: 768,
	is_xs: false,
	is_mobile: false,
	is_modal: false,
	
	Init: function(){
		var This = this;

		this.sp = path_webroot + this.sp;

        $("a[href='#']").click(function(e){
            e.preventDefault();
        });
		
		$("*[data-e]").each(function(){
			var e = $(this).attr("data-e");
			e = e.split("|");
			var email = e[0] + "@";
			if( e.length==1){
				email += This.domain;
			}else{
				email += e[1];
			}
			$(this).html('<a href="mailto:' + email + '">' + email + '</a>');
		});

		//this.LazyLoad();

		Products.Init();
        Basket.Init();
        Register.Init();
		this.Zoom();

		$('[data-toggle="popover"]').popover({
			trigger: 'click'
		});

		$("._fbox").fancybox();

		if( $(window).innerWidth() < this.xs_width ){
			this.is_xs = true;
		}
		if( $("body").hasClass('modalWindow') ){
			this.is_modal = true;
		}

		$("a[data-signup='1'], button[data-signup='1']").click(function(e){
			$("#LoginForm").addClass('noDisplay');
			$("#RegisterForm").removeClass('noDisplay');
			Register.open();
		});
		$("a[data-signup='0'], button[data-signup='0']").click(function(e){
			Register.close();
		});
		$("a[data-login='1'], button[data-login='1']").click(function(e){
			$("#LoginForm").removeClass('noDisplay');
			$("#RegisterForm").addClass('noDisplay');
			Register.open();
		});

		$("#CookieLawBar button[data-acceptcookie='1']").click(function(e){
			$.post( System.sp , 'act=cookielaw-accept'  );
			$("#CookieLawBar").animate({
				bottom: '-100px'
			},{
				easing: 'easeInSine',
				duration: 500,
				complete: function(){
					$(this).remove();
				}
			})
		});


		//ScrollTop
		if( this.st ){
			var arr = this.st.split(",");
			if(arr[1]){
				var speed = parseInt(arr[1]);
			}else{
				var speed = 650;
			}
			this.ScrollTop('#'+arr[0],speed);
		}
		//Ancore
		$("a[href^='#']").click(function(e){
			e.preventDefault();
			var anchor = $(this).attr("href");
			if(anchor!='#') {
				This.ScrollTop(anchor, 400);
			}
		});

        //customScrollbar
		var startScroll = $("#SearchCampagne > ul a.fired").length>0 ? $("#SearchCampagne > ul a.fired") : false;
		$("#SearchCampagne").mCustomScrollbar({
			theme: 'dark-thick',
			setHeight: '250px'
		});
		if( startScroll ){
			$('#SearchCampagne').mCustomScrollbar("scrollTo", startScroll);
		}
        var startScroll = $("#SearchCategorie > ul a.fired").length>0 ? $("#SearchCategorie > ul a.fired") : false;
        $("#SearchCategorie").mCustomScrollbar({
            theme: 'dark-thick',
            setHeight: '250px'
        });
        if( startScroll ){
            $('#SearchCategorie').mCustomScrollbar("scrollTo", startScroll);
        }

        var startScroll = $("#SearchMarchi > ul a.fired").length>0 ? $("#SearchMarchi > ul a.fired") : false;
        $("#SearchMarchi").mCustomScrollbar({
            theme: 'dark-thick',
            setHeight: '210px'
        });
        if( startScroll ) {
            $('#SearchMarchi').mCustomScrollbar("scrollTo", startScroll);
        }

		//Apre menu filtri in ricerca prodotti
		$("._openMobileFilters").click(function(e){
			This.openMobileFilters(true);
		});
		$("._closeMobileFilters").click(function(e){
			This.openMobileFilters(false);
		});

        //Ricerca ajax
        var form = $("#catalog_search-form");
        var scrollTop_speed = 500;
        $("a[data-ca], a[data-gr]").click(function(e){
			if( !$(this).hasClass('disabled') ) {
				if ($(this).attr('data-ca') == undefined) {
					var name = 'gr'
				} else {
					var name = 'ca';
				}

				if ($(this).hasClass('fired')) {
					var val = 0;
				} else {
					var val = $(this).attr('data-' + name);
				}

				$("a[data-" + name + "]").removeClass('fired');

				if (val != 0) {
					$(this).addClass('fired');
					form.attr('data-last-filter',name);//Segnala l'ultima ricerca fatta per categoria/griffe
				}else{
					form.removeAttr('data-last-filter');//Segnala l'ultima ricerca fatta per categoria/griffe
				}

				$("#catalog_search-form input[name='" + name + "']").val(val);
				$("#catalog_search-form select[name='page']").val(0);
				form.submit();
			}
        });
		$("input[name='tagliegr[]']").change(function(e){
			form.submit();
		});

        form.find("select[name='page']").change(function(e){
            var v = $(this).val();
            form.find("select[name='page']").val( v );
            This.ScrollTop(0,scrollTop_speed);
            form.submit();
        });
        form.find("select[name='sort']").change(function(e){
            var sel = form.find("select[name='page'] > option[value='0']").attr('selected',true);
            var v = $(this).val();
            form.find("select[name='sort']").val( v );

            This.ScrollTop(0,scrollTop_speed);

            form.submit();
        });
        form.find(".Btn[data-nextpage]").click(function(e){
            var v = form.find("select[name='page']").val();
            v = parseInt(v);
            var nextpage = $(this).attr('data-nextpage');
            nextpage = parseInt(nextpage);

            if(nextpage==-1 && v>0){
                form.find("select[name='page']").val(v-1);
            }else if(nextpage==1){
                form.find("select[name='page']").val(v+1);
            }

            This.ScrollTop(0,scrollTop_speed);

            form.submit();
        });

        form.submit(function(e){
            e.preventDefault();

            var data = $("#baseForm").serialize();
            data += '&act=search-products&' +  $("#catalog_search-form").serialize();

            $("#SearchResult .Loader").stop().fadeIn(350);

            $.ajax({
                type: 'post',
                url: This.sp,
                data: data,
                timeout: This.ajaxTimeout,
                complete: function(xhr,error){
                    $("#SearchResult .Loader").stop().fadeOut(350);
                    log(error);
                },
                success: function(data){
                    log(data);
                    data = $.parseJSON(data);
                    if( data.status ){
						var last_filter = form.attr('data-last-filter');
						form.removeAttr('data-last-filter');

                        //window.history.pushState(null,null,data.fields.url);
                        if( data.fields.n > 0) {
                            $("#SearchResult .noresult").addClass('hide');
                            $("#SearchResult .result").removeClass('hide');
                            $("#SearchResult .result .founded").html( data.fields.founded );
                            $("#SearchResult .result .cont").html(data.msg);
							$("#SearchResult .pre-img-responsive").removeClass('pre-img-responsive').addClass('img-responsive');

                            var select = $("#SearchResult select[name='page']");
                            select.html('');
                            for(var i=0; i < data.fields.page_max; i++ ){
                                select.append( $('<option />').val(i).text( (i+1) + " / " + data.fields.page_max ) );
                            }

							var page = parseInt(data.fields.page);
							var page = page>0 ? page : 0;
                            select.val( page );

                            data.fields.page_max = parseInt(data.fields.page_max);

                            $("#SearchResult .Btn[data-nextpage]").removeClass('disabled');
                            if( page==0 ){
                                $("#SearchResult .Btn[data-nextpage='-1']").addClass('disabled');
                            }else if(page==data.fields.page_max-1){
                                $("#SearchResult .Btn[data-nextpage='1']").addClass('disabled');
                            }

                            if(data.fields.page_max==1){
                                $("#SearchResult .paginationElements").hide();
                            }else if(data.fields.page_max>1){
                                $("#SearchResult .paginationElements").show();
                            }

							if( last_filter!='cam' ){
								$("#SearchCampagne ul > li > a").addClass('disabled');
								for (var i in data.fields.campagne) {
									$("#SearchCampagne ul > li > a[data-cam='" + data.fields.campagne[i] + "']").removeClass('disabled');
								}
							}

							if( last_filter!='ca' ){
								$("#SearchCategorie ul > li > a").addClass('disabled');
								for (var i in data.fields.categorie) {
									$("#SearchCategorie ul > li > a[data-ca='" + data.fields.categorie[i] + "']").removeClass('disabled');
								}
							}

							if( last_filter!='ma' ){
							$("#SearchMarchi ul > li > a").addClass('disabled');
								for (var i in data.fields.marchi) {
									$("#SearchMarchi ul > li > a[data-ma='" + data.fields.marchi[i] + "']").removeClass('disabled');
								}
							}

							if( data.fields.show_taglia=="0" ) {
								$("._SearchTaglie-title").hide();
								$("#SearchTaglie").slideUp();
							}else{
								$("._SearchTaglie-title").show();
								if( data.fields.show_taglia=="all"){
									$("#SearchTaglie li[data-type]").show();
								}else {
									$("#SearchTaglie li[data-type]").hide();
									$("#SearchTaglie li[data-type='" + data.fields.show_taglia + "']").show();
								}
								$("#SearchTaglie").slideDown();
							}

                        }else{
                            $("#SearchResult .result").addClass('hide');
                            $("#SearchResult .noresult").removeClass('hide');
                        }
                    }else{

                    }
                }
            });

            return false;
        });
		if( form.length>0 ){
			form.submit();
		}
        //Ricerca ajax END


		$("#signUp-form, #newsletterFormFooter").submit(function(){
			var f = $(this);
			var data = f.serialize();
			log(data);
			var btn = f.find('button');
			var loading = parseInt( btn.attr("loading") );
			if( !loading ){
				btn.attr("loading",1);
				btn.find('._text').css('visibility','hidden');
				btn.find('.loader').css('display','block');
				$.ajax({
					type: 'POST',
					url: System.sp,
					data: data,
					timeout: System.ajaxTimeout,
					complete: function(xhr,error){
						log(xhr);
						log(error);

						btn.attr("loading",0);
						btn.find('._text').css('visibility','visible')
						btn.find('.loader').css('display','none');
					},
					success: function(data){
						log(data);
						data = $.parseJSON(data);
						if( data.status ){
							f.get(0).reset();
							System.Alert( data.msg, data.fields.title );
						}else{
							System.Alert( data.msg );
						}
					}
				});
			}
			return false;
		});

		$("._form").submit(function(){
			var f = $(this);
			var form_id = f.attr("id");
			var data = f.serialize();
			log(data);
			var btn = f.find("button[type='submit']");
			var loading = parseInt( btn.attr("data-loading") );
			if(!loading){
				f.find('._response').slideUp();

				btn.attr("data-loading" , 1);
				btn.find('._text').css('visibility','hidden');
				btn.find('.loader').css('display','block');

				$.ajax({
					type: 'POST',
					url: System.sp,
					data: data,
					timeout: System.ajaxTimeout,
					complete: function(xhr,error){
						btn.attr("data-loading" , 0);
						btn.find('._text').css('visibility','visible');
						btn.find('.loader').css('display','none');
					},
					success: function(data){
						log(data);
						data = $.parseJSON(data);
						if(data.status){
							f.get(0).reset();
							if(form_id=='formContact'){
								f.find('._response .alert-success').removeClass('hide');
								f.find('._response .alert-danger').addClass('hide');
								f.find('._response').slideDown();
							}else{
								System.Alert( data.msg , data.fields.title );
							}
						}else{
							if(form_id=='formContact'){
								f.find('._response .alert-success').addClass('hide');
								f.find('._response .alert-danger').html( data.msg).removeClass('hide');
								f.find('._response').slideDown();
							}else{
								System.Alert( data.msg );
							}
						}
					}
				});
			}
			return false;
		});

		switch( System.Page.k ){
			case 'home':
				if( $("#homeSlider > div > div").length>1 ){
                    $("#homeSlider > div").bxSlider({
						auto: true,
                        pager: false,
                        controls: true
                    });
                }
				break;
            case 'product-item':
                $(".colore-thumb").click(function(e){
                    var src = $(this).attr("data-src");
					var zoom = $(this).attr("data-colore-zoom");
					$("#mainFoto").attr("src" , src).parent().attr("data-zoom",zoom);
					This.Zoom();
                    $(".colore-thumb").removeClass('fired');
                    $(this).addClass('fired');
                });
                $(".seleziona-colore a[data-colore]").click(function(e){
                   var colore = $(this).attr('data-colore');
                    $(".colore-thumbs").addClass('hide');
                    $(".colore-thumbs[data-codice='" + colore + "']").removeClass('hide').find('.colore-thumb:first-child').click();

                    $(".seleziona-taglia ul").addClass('hide');
                    $(".seleziona-taglia ul[data-codice='" + colore + "']").removeClass('hide').find('label:first-child').click();

                    $(".seleziona-colore a[data-colore]").removeClass('fired');
                    $(this).addClass('fired');

                });
                $(".seleziona-taglia label").click(function(e){
                    $(".seleziona-taglia label").removeClass('fired');
                    $(this).addClass('fired');
                });
                break;
			case 'orders':
				$("#OrderReturn").submit(function(e){
					var f = $(this);
					var data = f.serialize();
					log(data);
					var btn = f.find('button');
					var loading = parseInt( btn.attr("loading") );
					if( !loading ){
						btn.attr("loading",1);
						btn.find('._text').css('visibility','hidden');
						btn.find('.loader').removeClass('hide').css('visibility','visible');
						$.ajax({
							type: 'POST',
							url: System.sp,
							data: data,
							timeout: System.ajaxTimeout,
							complete: function(xhr,error){
								log(xhr);
								log(error);

								btn.attr("loading",0);
								btn.find('._text').css('visibility','visible')
								btn.find('.loader').addClass('hide');
							},
							success: function(data){
								log(data);
								data = $.parseJSON(data);
								if( data.status ){
									f.get(0).reset();
									System.Alert( data.msg, data.fields.title );
								}else{
									System.Alert( data.msg );
								}
							}
						});
					}
					return false;
				});

				break;
			case 'stores':
				$(".gmap").each(function(){
					var id = $(this).attr('id');
					var lat = $(this).attr("data-lat");
					var lng = $(this).attr("data-lng");
					var gmap = new GMap();
					gmap.id = id;
					gmap.init();
					gmap.addMarker(lat,lng);
					gmap.setCenter(lat,lng,11);
				});

				$("a[data-formcontact]").click(function(e){
					var id = $(this).attr("data-formcontact");
					$("#formContact").find("input[name='to']").val( id );
				});

				break;
		}
		
		this.activeButtons();
		
		$(".ajaxform").submit(function(e){//qui
			e.preventDefault();
			var data = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: This.sp,
				data: data,
				complete: function(xhr,error){},
				success: function(data){
					log(data);
					data = $.parseJSON(data);
					if(data.status){

					}else{

					}
				}
			});

			return false;
		});
		
		$('#MenuMiniCustomer.dropdown').hover(function() {
	      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
	    }, function() {
	      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
	    });
	    
	    $("#accountForm ._btnSave").click(function(){
	    	var f = $("#accountForm");
	    	var btn = $(this);
	    	btn.find("._text").css('visibility','hidden');
	    	btn.find(".loader").css({
	    		position: 'absolute',
	    		left: '50%',
	    		'margin-left' : '-8px',
	    		top: '10px',
	    		'visibility' : 'visible'
	    	}).removeClass("hide");
	    	$("#return_msg").html('');
	    	
	    	var data = $("#accountForm").serialize();
	    	var az = f.find("input[name='act']").val();
	    	$.ajax({
	    		type: 'POST',
	    		url: System.sp,
	    		timeout: System.ajaxTimeout,
	    		data: data,
	    		complete: function(xhr,error){
	    			log(xhr);
	    			log(error);
	    			btn.find("._text").css('visibility','visible');
	    			btn.find(".loader").addClass('hide');
	    		},
	    		success: function(data){
	    			log(data);
	    			data = $.parseJSON(data);
	    			if(data.status && az=='changePw' ){
	    				$("#return_msg").html( data.msg );
	    				f.get(0).reset();
	    			}else if( !data.status ){
	    				System.Alert(data.msg);
	    			}
	    		}
	    	});
	    });
	    
	    $("#recoveryPwForm").submit(function(){
	    	var f = $(this);
	    	var btn = f.find("button[type='submit']");
	    	btn.find('span').css('visibility','hidden');
	    	btn.find('.loader').css('visibility','visible');
	    	f.find('.alert-success').slideUp();
	    	
	    	$.ajax({
	    		type: 'POST',
	    		data: f.serialize(),
	    		url: System.sp,
	    		timeout: This.ajaxTimeout,
	    		complete: function(xhr,error){
	    			log(xhr);
	    			log(error);
	    			btn.find('span').css('visibility','visible');
	    			btn.find('.loader').css('visibility','hidden');
	    		},
	    		success: function(data){
	    			log(data);
	    			data = $.parseJSON(data);
	    			if( data.status ){
	    				f.get(0).reset();
	    				f.find('.alert-success').hide().removeClass('hide').slideDown();
	    			}else{
	    				System.Alert(data.msg);
	    			}
	    		} 
	    	});
	    		    	
	    	return false;
	    });
	    
		this.ScrollEvent();
	},

	openMobileFilters: function(show){
		var This = this;
		if(show){
			var ar = $("#AuthReq");
			if( ar.hasClass('required') ){
				ar.fadeIn();
			}else {
				ar.addClass('required');
			}
			ar.unbind('click').click(function (e) {
				This.openMobileFilters(false);
			});
			var left = 0;
			$("#Wrapper").css({
				'overflow': 'hidden',
				'height'	:	0
			});
		}else {
			var left = '-100%';
			$("#AuthReq").fadeOut();
			var complete = function(){
				$("#AuthReq").fadeOut();
			};
			$("#Wrapper").css({
				'overflow': 'auto',
				'height'	:	'auto'
			});
		}
		$("#Products_SearchCol").animate({
			left: left
		},500);
	},

	Zoom: function(){
		//Zoom image
		$(".zoom").zoom();//Zoom generico

		//Zoom pagina prodotto
		$(".productZoom").each(function(e){
			var imgZoom = $(this).attr('data-zoom');
			$(this).zoom({
				url: imgZoom
			});
		});
	},
	
	validateEmail: function(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	},
	
	LazyLoad: function(){
        $("img[data-original]").lazyload({
			effect : "fadeIn"
		});
	},
	
	activeButtons: function(){
		var This = this;
		
		this.LazyLoad();
		
		$("a[href='#']").click(function(e){
			e.preventDefault();
		});
	},
	
	ScrollEvent: function(){
		var x,y;
		window.scrollX = x = $(window).scrollLeft();
		window.scrollY = y = $(window).scrollTop();
		//console.log(" ScrollY: " + window.scrollY + " ScrollX: " + window.scrollX);

        //var scrollTop_limit = 212;
		if(y>62 && !System.is_xs && !System.is_modal && $('#Header').length>0 ){
			$('#Header').addClass('scrolled');
		}else{
			$('#Header').removeClass('scrolled');
		}
		
		//Abilita Infinity Scroll su Tutti i prodotti / Ricerca
		if( $("#AllProducts").length>0 ){
			var o = $(".IS_Element");
			var hWindow = $(window).height();
			var pos = o.offset();
			var footer = $("#Footer");
			var div = $("#AllProducts");
			
			var stop = div.attr("data-stop")=="1" ? true : false; 
			var page = parseInt( div.attr("data-page") );
			var orderBy = div.attr("data-orderBy");
			var order = div.attr("data-order");		
			//console.log( (pos.top-y) +" < " + hWindow);	
			if( (pos.top-y) < hWindow && !stop && !System.searchLoading && page>=0){
				console.log( (y) +" < " + pos.top);
				o.addClass('Open');
				System.searchLoading = true;
				System.Search(orderBy,order,page);
			}
		}
	},
	
	ScrollTop: function(y,speed){
		var offset = $("#Wrapper").css('padding-top');
		offset = parseInt(offset);
		$.scrollTo(y,speed,{
			easing: 'easeOutSine',
			offset: -offset
		});
	},
	
	baseForm: function(){
		var data = $("#baseForm").serialize();
		return data+'&';
	},
	
	test: function(){
		var x = '';
		x = $(window).height() +" "+$('#Wrapper').innerHeight();
		x += ' ' + $('body').scrollTop() + " " + $(window).scrollTop() + ' ' + $("#Schema").scrollTop() + ' ' + $(document).scrollTop() + ' js: '+ window.pageYOffset+' '+document.documentElement.scrollTop;
		alert(x);
	},
	
	Confirm: function(msg,title,callback){
		var o = $("#Alert");
		if( title ){
			o.find('.modal-title').html( title );
		}else{
			o.find('.modal-title').html( o.find('.modal-title').attr("data-txt") );
		}
		o.find('.modal-body').html(msg);
		o.find('.confirmBtn').removeClass('hide').click( callback );
		o.modal();
	},
	
	Alert: function(msg,title){
		var o = $("#Alert");
		if( title ){
			o.find('.modal-title').html( title );
		}else{
			o.find('.modal-title').html( o.find('.modal-title').attr("data-txt") );
		}
		o.find('.modal-body').html(msg);
		o.find('.confirmBtn').addClass('hide');
		o.modal();
	}
};

var Products = {
	Init: function(){
		var This = this;
		
		this.changeColor();
		$("#ProductForm input[name='qty']").change(function(){
			var data = $("#ProductForm").serialize();
			$.ajax({
				type: 'POST',
				url: System.sp,
				data: "act=checkProductQty&" + data,
				complete: function(xhr,error){},
				success: function(data){
					console.log(data);
					data = $.parseJSON(data);
					if( !data.status ){
						System.Alert(data.msg);	
					}
				}
			});
		});
		$("#ProductForm ._addBasket").click(function(){
			var btn = $(this);
			var cl = parseInt( btn.attr("data-loading") );
			if( !cl ){
				btn.attr('data-loading',1);
				var data = $("#ProductForm").serialize();
				log(data);
				
				btn.find("._text").css('visibility','hidden');
				btn.find(".loader").css({
					'visibility' : 'visible',
					position: 'absolute',
					left: '50%',
					'margin-left' : '-8px',
					top: '10px'
				}).removeClass('hide');

				$.ajax({
					type: 'POST',
					url: System.sp,
					data: "act=addBasket&" + data,
					complete: function(xhr,error){
						log(xhr);
						log(error);
						btn.attr('data-loading',0);
						btn.find("._text").css('visibility','visible');
						btn.find(".loader").addClass('hide');
					},
					success: function(data){
						log(data);
						data = $.parseJSON(data);
						if( data.status ){
                            $('#statusAddBasket .added').fadeIn();
							//document.location = $("#ProductForm").attr("data-action-this") + '?added&id_color=' + data.fields.id_color;
						}else{
							System.Alert( data.msg );
						}
					}
				});
			}
		});
		/*$("#ProductForm button[data-add='1']").click(function(){
			var data = $("#ProductForm").serialize();
			log(data);
			$.ajax({
				type: 'POST',
				url: System.sp,
				data: "act=addBasket&" + data,
				complete: function(xhr,error){},
				success: function(data){
					log(data);
					data = $.parseJSON(data);
					if( data.status ){
						document.location = $("#ProductForm").attr("data-action");
					}else{
						System.Alert(data.msg);	
					}
				}
			});
		});*/
	},
	changeColor: function(){
		var id_color = $("select[name='id_color']").val();
		if( id_color ){
			$(".sizes").hide();
			$(".sizes[data-idcolor='" + id_color + "']").show();
			$("#mainFoto").attr("src", products_photos[ id_color ]);
			this.setFirstSize(id_color);
			
			return id_color;
		}
	},
	changeSize: function(id_size){
		$(".sizes > ul > li").removeClass("fired");
		$(".sizes > ul > li[data-idsize='" + id_size + "']").addClass("fired");
		
		$("#ProductForm input[name='id_size']").val( id_size );
	},
	setFirstSize: function(id_color){
		var This = this;
		
		var flag = false;
		$(".sizes[data-idcolor='" + id_color + "'] > ul > li").each(function(){
			if( !$(this).hasClass('disabled') && !flag){
				flag = true;
				$(this).children("a").click();
			}
		});
	}
};

var Basket = {
	step: 1,
	
	Init: function(){
		var This = this;
		this.step = $("#step").val();
		
		if( this.step==1 ){
			This.updatePriceBasket();
		}
		
		$("#BasketTable select[name^='qty']").change(function(){
			This.updateBasket(function(){
				This.updatePriceBasket();
			});
		});
		$("#BasketTable .remove").click(function(){
			var id_ps = $(this).attr("data-id");
			var txt = $(this).attr("data-text-confirm");
			System.Confirm( txt,'',function(){
				This.removeProductBasket(id_ps);
			});
		});
		
		$("#BasketForm .choicePayment > li").click(function(){
			$("#BasketForm .choicePayment > li").removeClass("fired");
			$("#BasketForm .choicePayment > li").find("input[type='radio']").prop("checked",false);
			$(this).find("input[type='radio']").prop("checked",true);
			$(this).addClass("fired");
		});
		$("#BasketForm .choicePayment > li[data-checked='true']").click();
	},
	
	copyCustomerData: function(){
		var data = "name,surname,id_country,city,address,address_number,zip,tel,tel2,email_address";
		data = data.split(",");
		for(var i in data){
			$("#BasketForm *[name='" + data[i] + "-shipping']").val( $("#BasketForm *[name='" + data[i] + "-bill']").val() );
		}
	},
	
	Confirmation: function(){
		var This = this;
		if( !this.Confirmation_running ){
			This.Confirmation_running = true;
			var f = $("#BasketForm");
			var data = "act=BasketConfirmation&" + f.serialize();
			log( data );
			$(".btnConfirmation .loader").css("visibility","visible");
			$.ajax({
				type: 'POST',
				data: data,
				url: System.sp,
				success: function(data){
					This.Confirmation_running = false;
					log(data);
					$(".btnConfirmation .loader").css("visibility","hidden");
					data = $.parseJSON(data);
					if(data.status){
						var url = $("#BasketForm").attr("data-action");
						//alert(url);
						document.location = url;
					}else{
						System.Alert( data.msg );
					}
				}
			});
		}
	},
	
	updateBasket: function(callback){
		var data = "act=updateBasket&" + $("#BasketForm").serialize();
		log("updateBasket send: " + data);
		$.ajax({
			type: 'POST',
			url: System.sp,
			data: data,
			complete: function(xhr,error){},
			success: function(data){
				log(data);
				data = $.parseJSON(data);
				
				if(callback){
					callback();
				}
			}
		});
	},
	
	removeProductBasket: function(id_ps){
		var id_basket = $("#BasketForm input[name='id_basket']").val();
		var data = "act=removeProductBasket&id_basket="+id_basket+"&id_ps="+id_ps;
		log(data);
		$.ajax({
			type: 'POST',
			url: System.sp,
			data:data,
			complete: function(xhr,error){},
			success: function(data){
				log(data);
				data = $.parseJSON(data);
				if( data.status ){
					document.location = $("#BasketForm").attr("data-action-this");
				}else{
					System.Alert( data.msg );
				}
			}
		});
	},
	
	updatePriceBasket: function(){
		var data = "act=updatePriceBasket&" + $("#BasketForm").serialize();
		log("updatePriceBasket send: " + data);
		$.ajax({
			type: 'POST',
			url: System.sp,
			data: data,
			complete: function(xhr,error){},
			success: function(data){
				log(data);
				data = $.parseJSON(data);
				if( data.status ){
					for(var i in data.fields.list){
						var o = data.fields.list[i];
						$("#BasketTable .price[data-id='price_" + o.id + "']").html( o.v );
					}	
					$(".info[data-id='total-basket']").html( data.fields.total_basket_txt );
					$(".info[data-id='buono-cost']").html( data.fields.buono_cost_txt );
					$(".info[data-id='vat']").html( data.fields.vat_cost_txt );
					$(".info[data-id='total']").html( data.fields.total_txt );
				}
			}
		});
	},
	
	couponCode: function(){
		var coupon_code = $("#couponCode").val();
		var data = "act=addCoupon&" + $("#BasketForm").serialize(); 
		$.ajax({
			type: 'POST',
			url: System.sp,
			data: data,
			success: function(data){
				log(data);
				data = $.parseJSON(data);
				if(data.status){
					document.location = $("#BasketForm").attr("data-action-this");
				}else{
					System.Alert(data.msg);
				}
			}
		});
	}
};

var Register = {
	div: null,
	
	Init: function(){
		var This = this;
		
		this.div = $("#LoginBox");
		/*$("#RegisterForm select[name='id_country-bill']").change(function(e){
			var cee = $(this).children('option:selected').attr("data-cee");
			if(cee=="1"){
				$("#vat-required-star").show();
			}else{
				$("#vat-required-star").hide();
			}
		});*/
		$("#LoginForm").submit(function(){
			var loginBtn = $(this).find(".LoginBtn");
			//loginBtn.find(".text").addClass('hide');
			loginBtn.find(".text").css('visibility','hidden');
			loginBtn.find(".loader").css('visibility','visible').removeClass('hide');
			
			var f = $(this);
			var data = f.serialize();
			log(data);
			$.ajax({
				type: 'POST',
				url: System.sp,
				data: data,
				timeout: System.ajaxTimeout,
				complete: function(xhr,error){
					log(xhr);
					log(error);
					
					//loginBtn.find(".text").removeClass('hide');
					loginBtn.find(".text").css('visibility','visible');
					loginBtn.find(".loader").addClass('hide');
				},
				success: function(data){
					log(data);

					data = $.parseJSON(data);
					if(data.status){
						f.unbind('submit');
						f.attr('action', f.attr('data-action') ).submit();
					}else{
						System.Alert(data.msg);
					}
				}
			});
			return false;
		});
		this.div.find(".RegBtn").click(function(){
			$("#LoginForm").slideUp();
			$("#RegisterForm").slideDown();
			This.regLoader(false);
		});
		this.div.find(".regCancelBtn").click(function(){
			$("#LoginForm").slideDown();
			$("#RegisterForm").slideUp();
		});
		this.div.find(".regRegisterBtn").click(function(){
			This.regLoader(true);
			var data = $("#RegisterForm").serialize();
			$.ajax({
				type: 'POST',
				url: System.sp,
				data: data,
				timeout: 10000,
				complete: function(xhr,error){
					log(xhr);
					log(error);
				},
				success: function(data){
					log(data);
					data = $.parseJSON(data);
					This.regLoader(false);
					
					if( data.status ){
						var f = $("#RegisterFormConfirm");
						f.find("input[name='id_customer']").val( data.fields.id_customer );
						f.submit();
					}else{
						System.Alert(data.msg);	
					}
				}
			});
		});
	},
	
	regLoader: function(show){
		if(show){
			this.div.find(".regRegisterBtn").css('opacity',0);
			this.div.find(".regLoader").css("visibility",'visible');
		}else{
			this.div.find(".regRegisterBtn").css('opacity',1);
			this.div.find(".regLoader").css("visibility",'hidden');
		}
	},

	show: function(show){
		var au = $("#AuthReq");
		var lo = $("#LoginBox");
		if(show){
			au.fadeIn();
			lo.fadeIn();
		}else{
			au.fadeOut();
			lo.fadeOut();
		}
	},
	open: function(){
		this.show(true);
	},
	close: function(){
		this.show(false);
	}
};

$(document).ready(function(){
	System.Init();
	$(window).scroll( System.ScrollEvent );
	$(".venobox").venobox();
	$(".venoboxRecipes").venobox({
		framewidth: '90%',
		border: '10px',
		frameheight: '75%'
	});
});


//* MOBILE
/*var myScroll;
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);

document.querySelector( "#nav-toggle" ).addEventListener( "click", function() {
  this.classList.toggle( "active" );
});*/

// openMobileMenu
function openMobileMenu(){
	if( $("#nav-toggle").hasClass('menuOpen') ){
		$("#wrapperMainMenu").slideUp(250);
		$("#nav-toggle").removeClass('menuOpen');						
	}else{
		$("#wrapperMainMenu").slideDown(250);
		$("#nav-toggle").addClass('menuOpen');
		// Menu Mobile Horizontal
		myScroll = new IScroll('#wrapperMainMenu', {
			scrollbars: false,
			scrollX: true,
			mouseWheel: true,
			click: true
		});
	}
}