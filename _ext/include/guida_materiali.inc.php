<div id="guida-materiali-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><# Guida materiali #></h4>
            </div>
            <div class="modal-body">

                <table class="table table-responsive table-condensed table-bordered">
                    <tbody>
                        <tr>
                            <td>WO</td>
                            <td><# LANA #></td>
                        </tr>
                        <tr>
                            <td>WV</td>
                            <td><# LANA VERGINE #></td>
                        </tr>
                        <tr>
                            <td>WP</td>
                            <td><# ALPACA #></td>
                        </tr>
                        <tr>
                            <td>WS</td>
                            <td><# KASHMIR #></td>
                        </tr>
                        <tr>
                            <td>WS</td>
                            <td><# MOHAIR #></td>
                        </tr>
                        <tr>
                            <td>WA</td>
                            <td><# ANGORA #></td>
                        </tr>
                        <tr>
                            <td>SE</td>
                            <td><# SETA #></td>
                        </tr>
                        <tr>
                            <td>CO</td>
                            <td><# COTONE #></td>
                        </tr>
                        <tr>
                            <td>LI</td>
                            <td><# LINO #></td>
                        </tr>
                        <tr>
                            <td>CA</td>
                            <td><# CANAPA #></td>
                        </tr>
                        <tr>
                            <td>JU</td>
                            <td><# JUTA #></td>
                        </tr>
                        <tr>
                            <td>RA</td>
                            <td><# RAMIE' #></td>
                        </tr>
                        <tr>
                            <td>AC</td>
                            <td><# ACETATO #></td>
                        </tr>
                        <tr>
                            <td>CU</td>
                            <td><# CUPRO - RAYON #></td>
                        </tr>
                        <tr>
                            <td>MD</td>
                            <td><# MODAL #></td>
                        </tr>
                        <tr>
                            <td>TA</td>
                            <td><# TRIACETATO #></td>
                        </tr>
                        <tr>
                            <td>VI</td>
                            <td><# VISCOSA #></td>
                        </tr>
                        <tr>
                            <td>PC</td>
                            <td><# ACRILICO #></td>
                        </tr>
                        <tr>
                            <td>PA</td>
                            <td><# POLIAMMIDE #></td>
                        </tr>
                        <tr>
                            <td>LY</td>
                            <td><# LYOCELL #></td>
                        </tr>
                        <tr>
                            <td>PL</td>
                            <td><# POLIESTERE #></td>
                        </tr>
                        <tr>
                            <td>PU</td>
                            <td><# POLIURETANICA #></td>
                        </tr>
                        <tr>
                            <td>EA</td>
                            <td><# ELASTAN #></td>
                        </tr>
                        <tr>
                            <td>GL</td>
                            <td><# VETRO TESSILE #></td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><# Chiudi #></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->