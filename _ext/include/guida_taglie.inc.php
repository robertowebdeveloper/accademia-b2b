<?php
$guida_taglie_arr = array(
    'Donna' => array(
        'donna-abbigliamento' => array("Donna","abbigliamento"),
        //'donna-intimo' => array("Donna","intimo"),
        'donna-scarpe' => array("Donna","scarpe"),
        //'donna-accessori' => array("Donna","accessori")
    ),
    'Uomo' => array(
        'uomo-abbigliamento' => array("Uomo","abbigliamento"),
        'uomo-scarpe' => array("Uomo","scarpe"),
        //'uomo-accessori' => array("Uomo","accessori")
    )/*,
    'Bambino' => array(
        'bambino' => array("Bambino","")
    )*/
);
?>
<div id="guida-taglie-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><# Guida alle taglie #></h4>
            </div>
            <div class="modal-body">

                <nav id="guida-taglie" class="navbar navbar-default navbar-static">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <?php
                                $iLi = 0;
                                foreach($guida_taglie_arr as $k=>$v){
                                    if(count($v)>1) {
                                        ?>
                                        <li class="dropdown<?= $iLi == 0 ? ' active' : ''; ?>">
                                            <a href="#" class="dropdown-toggle"
                                               data-toggle="dropdown" role="button" aria-haspopup="true"
                                               aria-expanded="false">
                                                <# <?= $k; ?> #> <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <?php
                                                foreach ($v as $sub_k => $sub_v) {
                                                    ?>
                                                    <li>
                                                        <a href="#<?=$sub_k; ?>">
                                                            <# <?= $sub_v[0]; ?> #>
                                                            <small>
                                                                <# <?=$sub_v[1]; ?> #>
                                                            </small>
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }else{
                                        foreach($v as $sub_k => $sub_v) {
                                            ?>
                                            <li>
                                                <a href="#<?= $sub_k; ?>">
                                                    <# <?= $k; ?> #>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    $iLi++;
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div data-spy="scroll" data-target="#guida-taglie" data-offset="0" class="scrollspy-guida-taglie">
                    <?php
                    foreach($guida_taglie_arr as $k=>$v) {
                        foreach ($v as $sub_k => $sub_v){
                            $txt = $sub_v[0];
                            $txt .= empty($sub_v[1]) ? '' : " - {$sub_v[1]}";
                            ?>
                            <h4 id="<?=$sub_k; ?>">
                                <# <?=$txt; ?> #>
                            </h4>
                            <?php
                            include("_ext/include/guida_taglie/{$sub_k}.inc.php");
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><# Chiudi #></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->