<form id="baseForm">
	<input type="hidden" name="id_lang" value="<?=$S->id_lang; ?>">
	<input type="hidden" name="lang" value="<?=$S->_lang; ?>">
	<input type="hidden" name="id_country" value="<?=$S->id_country; ?>">
	<input type="hidden" name="id_customer" value="<?=$S->id_customer; ?>">
	<input type="hidden" name="ses" value="<?=session_id(); ?>">
</form>

<div id="Alert" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><# Chiudi #></span></button>
        <h4 class="modal-title" data-txt="<# Attenzione #>"></h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default uppercase cancelBtn" data-dismiss="modal" data-txt="<# Chiudi #>"><# Chiudi #></button>
        <button type="button" class="btn btn-primary uppercase hide confirmBtn" data-dismiss="modal" data-txt="<# Conferma #>"><# Conferma #></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->