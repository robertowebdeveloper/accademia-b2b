<?php $class = $this->Page->auth_req ? ' class="required"' : ''; ?>
<div id="AuthReq"<?=$class; ?>></div>
<div id="LoginBox"<?=$class; ?>>
	<?php if(false){ ?>
		<a href="#" data-signup="0" class="Btn btn pull-right"><# Chiudi #> <i class="glyphicon glyphicon-remove-circle"></i></a>
	<?php } ?>
	<a href="{{url home}}" id="LoginHome" class="Btn btn"><i class="glyphicon glyphicon-menu-left"></i> Home page</a>
	<br>
	<?php if( $S->reg_code_invalid ){ ?>
		<div class="row"><div class="col-md-10 col-md-offset-1">
			<div class="alert alert-danger">
				<# <b>Il codice di attivazione non è valido.</b> Verifica la tua e-mail di benvenuto e clicca sul link di registrazione #>
			</div>
		</div></div>
	<?php } ?>
	<?php
	$class_login = $class_register = '';
	if( isset($_GET['signup']) ){
		$class_login = ' class="noDisplay"';
	}else{
		$class_register = ' class="noDisplay"';
	}
	?>
	<form id="LoginForm"<?=$class_login; ?> enctype="application/x-www-form-urlencoded" method="post" data-action="{{url prodotti}}" role="form">
		<?=$S->HF('login'); ?>
		<div class="row">
			<div class="col-md-6 col-sm-6 hidden-xs">
				<h4 class="uppercase"><# Registrati #></h4>
				<!--h2 class="uppercase"><# devi registrarti #></h2-->
				<div class="littleFont">
					<b><# Iscriviti sul nostro sito #></b>
				</div>
				<br><br>
				<ol id="RegisterSteps">
					<li>
						<i class="glyphicon glyphicon-pencil"></i>
						<# Inserisci i tuoi dati #>
					</li>
					<li>
						<i class="glyphicon glyphicon-envelope"></i>
						<# Controlla la tua casella di posta #>
					</li>
					<li>
						<i class="glyphicon glyphicon glyphicon-link"></i>
						<# Clicca sulla mail di conferma #>
					</li>
					<li>
						<i class="glyphicon glyphicon-thumbs-up"></i>
						<# Sei pronto per acquistare sul nostro sito! #>
					</li>
				</ol>
				<br>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h4 class="uppercase"><# Accedi #></h4>
				<!--h2 class="uppercase"><# member? #></h2-->
				<span class="littleFont">
					<b><# Sei già registrato? #></b>

					<# Inserisci i tuoi dati: #>
				</span>
				<br><br>
				<label>
					<# E-mail #><sup class="Red">*</sup>
				</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<input type="text" name="login" class="form-control" placeholder="<# e-mail #>">
				</div>
				<label>
					<# Password #><sup class="Red">*</sup>
				</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input type="password" name="pw" class="form-control" placeholder="<# password #>">
				</div>

                <br>
                <label>
                    <input type="checkbox" name="remember" value="1"> <# Ricordati di me #>
                </label>
				<br>
				<span class="littleFont">
					<a href="{{url recovery-password}}" class="pull-right"><em><# Hai dimenticato la tua password? #></em></a>
					<span class="Red"><sup>*</sup><# Campo richiesto #></span>
				</span>
				<br><br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 hidden-xs">
				<a href="#" class="Btn btn RegBtn"><# Registrati #></a>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<button type="submit" class="Btn btn big block LoginBtn">
					<span class="text"><# Login #></span>
					<span class="loader loader16 hide" style="margin-left: -32px; top: 10px;"></span>
				</button>
				<div class="visible-xs">
					<hr>
					<a href="#" class="Btn btn RegBtn"><# Registrati #></a>
				</div>
			</div>
		</div>
	</form>
	<form id="RegisterForm"<?=$class_register; ?> role="form">
		<?=$S->HF('registerAccount'); ?>

		<h2 class="text-center"><# Registrati #></h2>
		<hr>
		
		<div class="row">
			<?php $tabindex=1; ?>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# E-mail #><sup class="Red">*</sup></label>
					<input type="text" name="email" class="form-control" tabindex="<?=$tabindex++; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# E-mail di conferma #><sup class="Red">*</sup></label>
					<input type="text" name="email_confirm" class="form-control" tabindex="<?=$tabindex++; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Password #><sup class="Red">*</sup></label>
					<input type="password" name="pw" class="form-control" tabindex="<?=$tabindex++; ?>">
					<span class="help-block mini"><# La password deve essere di almeno <b>%s caratteri</b> %txt=<?=$S->P('pw-min-length'); ?> #></span>
				</div>
				<hr class="hidden-xs">
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Password di conferma #><sup class="Red">*</sup></label>
					<input type="password" name="pw_confirm" class="form-control" tabindex="<?=$tabindex++; ?>">
					<span class="help-block mini">&nbsp;</span>
				</div>
				<hr>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Ragione sociale #><sup class="Red">*</sup></label>
					<input type="text" name="company" class="form-control" tabindex="<?=$tabindex++; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Partita iva #><sup class="Red">*</sup></label>
					<input type="text" name="vat" class="form-control" tabindex="<?=$tabindex++; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Nome #><sup class="Red">*</sup></label>
					<input type="text" name="name" class="form-control" tabindex="<?=$tabindex++; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Cognome #><sup class="Red">*</sup></label>
					<input type="text" name="surname" class="form-control" tabindex="<?=$tabindex++; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Codice fiscale #></label>
					<input type="text" name="cf" class="form-control" tabindex="<?=$tabindex+=2; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Nazione #><sup class="Red">*</sup></label>
					<select name="id_country-bill" class="form-control" tabindex="<?=$tabindex+=2; ?>">
						<option value="0">-</option>
						<?php
						$list = $S->CountryList();
						foreach($list as $r){
							?><option value="<?=$r->id; ?>" data-cee="<?=$r->cee; ?>"><?=$r->name_inter; ?></option><?php
						}
						?>
					</select>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Città #></label>
					<input type="text" name="city-bill" class="form-control" tabindex="<?=$tabindex+=2; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# CAP #></label>
					<input type="text" name="zip-bill" class="form-control" tabindex="<?=$tabindex+=2; ?>">
				</div>
			</div>
			<div class="col-sm-10 col-xs-12">
				<div class="form-group">
					<label><# Indirizzo #></label>
					<input type="text" name="address-bill" class="form-control" tabindex="<?=$tabindex+=2; ?>">
				</div>
			</div>
			<div class="col-sm-2 col-xs-12">
				<div class="form-group">
					<label><# Numero civico #></label>
					<input type="text" name="address_number-bill" class="form-control form-auto" size="10" tabindex="<?=$tabindex+=2; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Telefono #></label>
					<input type="text" name="tel-bill" class="form-control" placeholder="(+1) 055.12334567" tabindex="<?=$tabindex+=2; ?>">
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label><# Telefono (2) #></label>
					<input type="text" name="tel2-bill" placeholder="(+1) 055.12334567" class="form-control" tabindex="<?=$tabindex+=2; ?>">
				</div>
			</div>
			<?php if(false){ ?>
				<div class="form-group">
					<label><# Company #><sup class="Red">*</sup></label>
					<input type="text" name="company" class="form-control" tabindex="<?=$tabindex+=2; ?>">
				</div>
				<div class="form-group">
					<label><# Partita iva #><sup id="vat-required-star" class="Red">*</sup></label>
					<input type="text" name="vat" class="form-control" tabindex="<?=$tabindex+=2; ?>">
				</div>
			<?php } ?>
			<div class="col-sm-6 col-xs-12">
				<div class="form-group">
					<label>
						<input type="checkbox" name="privacy" value="1">
						&nbsp;&nbsp;&nbsp;
						<# Accetta Termini e condizioni e le norme sulle privacy #>
					</label>
					<label>
						<input type="checkbox" name="newsletter_subscribe" value="1">
						<input type="hidden" name="mc-group-name[0]" value="Negozio">
						<input type="hidden" name="mc-group-groups[0]" value="BO - Adulto">
						&nbsp;&nbsp;&nbsp;
						<# Iscriviti alla nostra newsletter #>
					</label>
				</div>
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="text-right relative">
					<a href="#Wrapper" class="Btn btn regCancelBtn"><# Annulla #></a>
					<a href="#" class="Btn btn regRegisterBtn"><span><# Registra #></span></a>
					<span class="regLoader loader loader16 Black"></span>
				</div>
			</div>

		</div>
	</form>
	<form id="RegisterFormConfirm" method="post" action="{{url confirm-registration}}" enctype="application/x-www-form-urlencoded">
		<input type="hidden" name="id_customer" value="0">
	</form>
</div>