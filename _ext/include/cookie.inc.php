<?php
//setcookie('so_cp',1,time(),'/');
if( !isset($_COOKIE['so_cp']) ){ ?>
	<section id="CookieLawBar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<# Il presente sito utilizza i cookie per facilitare la navigazione. #><!-- We uses cookies in order to provide the best possible experience to users of this site. -->
					<button type="button" class="btn btn-success btn-xs" data-acceptcookie="1"><# Accetta e continua #></button> <!-- Accept and continue -->
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-right">
					<a href="{{url cookie-policy}}"><# In caso di mancata accettazione puoi cambiare le impostazioni dei cookie #><!-- For more information on how we use cookies or for help, please visit our Cookies Policy page  --></a>
				</div>
			</div>
		</div>
	</section>
<?php } ?>