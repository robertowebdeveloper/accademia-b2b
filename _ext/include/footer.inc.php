<footer id="Footer">
	<section class="cont">
		<div class="container"><div class="row">
			<div class="col-md-3 col-sm-3 col-xs-6">
				<h4><# Menu #></h4>
                <hr>
				<ul>
					<?php
					$list = array('home','marchi','linea','campagna');
					foreach($list as $r){
						?><li{{urlfired_class <?=$r; ?>}}><a href="{{url <?=$r; ?>}}">{{urlname <?=$r; ?>}}</a></li><?php
					} ?>
				</ul>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6">
                <h4><# Account #></h4>
                <hr>
                <ul>
                    <?php
					if( $_SESSION['id_customer']>0 ){
						$list = array('account','orders');
						foreach($list as $r){
							?><li{{urlfired_class <?=$r; ?>}}><a href="{{url <?=$r; ?>}}">{{urlname <?=$r; ?>}}</a></li><?php
						}
					}else{
						?>
						<li><a href="#Wrapper" data-signup="1"><# Registrati #></a></li>
						<li><a href="#Wrapper" data-login="1"><# Login #></a></li>
						<?php
					}
					?>
                </ul>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6">
				<h4><# Supporto e informazioni #></h4>
                <hr>
                <ul>
                    <?php
                    $list = array('termini','privacy');
                    foreach($list as $r){
                    	?><li{{urlfired_class <?=$r; ?>}}><a href="{{url <?=$r; ?>}}">{{urlname <?=$r; ?>}}</a></li><?php
                    } ?>
					<li{{urlfired_class terms}}><a href="{{url termini}}#art-spedizioni-resi"><# Spedizioni e resi #></a></li>
					<li{{urlfired_class terms}}><a href="{{url termini}}#art-pagamento-sicuro"><# Metodi di pagamento #></a></li>
                </ul>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6">
				<h4><# Newsletter #></h4>
                <hr>
				<form id="newsletterFormFooter" class="ajaxform">
					<?=$S->HF('signup-nl'); ?>
					<input type="hidden" name="mc-group-name[0]" value="Negozio">
					<input type="hidden" name="mc-group-groups[0]" value="BO - Adulto">
                    <div class="uppercase">
                        <# Resta informato su gli ultimi arrivi e le offerte. #>
                    </div>
                    <div class="Margin"></div>
					<div class="insert">
						<input type="text" name="email" placeholder="<# E-mail #>">
						<button type="submit" class="Btn btn btn-sm">
							<span class="_text"><# Invia #></span>
							<span class="loader loader16 loader-hide" style="top: 8px;"></span>
						</button>
					</div>
                    <div class="Margin"></div>
					<div>
						<div class="pull-right">
							<?php if($S->_lang=="it"){ ?>
								<a href="//www.iubenda.com/privacy-policy/123414" class="iubenda-black iubenda-embed" title="Privacy Policy">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
							<?php }else{ ?>
								<a href="//www.iubenda.com/privacy-policy/652364" class="iubenda-black iubenda-embed" title="Privacy Policy">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
							<?php } ?>
						</div>
						<input type="checkbox" name="privacy" value="1">
						&nbsp;<# Accetta privacy  #>
					</div>
				</form>
			</div>
		</div></div>
	</section>
	<section class="info">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-xs-12">
                    &copy; <?=date("Y"); ?> ACCADEMIA. All rights reserved - Designed by <a href="http://www.inol3.com" target="_blank">inol3.com</a>
                </div>
                <div class="col-sm-4 col-xs-12 text-right">
                </div>
            </div>
        </div>
	</section>	
</footer>