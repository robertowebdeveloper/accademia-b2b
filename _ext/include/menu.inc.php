<?php
$list_menu = array('marchi'=>true,'linea'=>true,'campagna'=>true);
?>
<div id="barMenu">
	<div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-offset-6 col-sm-offset-3 col-sm-6 col-xs-12 static">
                <nav id="mainMenu"><ul>
                    <?php
                    $arr = array();
                    foreach($list_menu as $r=>$has_submenu){
                        $li_class = $has_submenu ? ' class="_has_submenu"' : '';
                        ?>

                        <li{{urlfired_class <?=$r;?>}}<?=$li_class; ?>>
                            <a href="{{url <?=$r; ?>}}">{{urlname <?=$r; ?>}}</a>
                            <?php
                                switch($r){
                                    case 'marchi':
                                        ?>
                                        <div class="submenu _marchi">
                                            <div>
                                                <div class="row">
                                                    <?php
                                                    $arr = array('Disney Big Hero','Disney Frozen','Disney Violetta','SpongeBob');
                                                    foreach($arr as $m){ ?>
                                                        <div class="col-md-2">
                                                            <a href="{{url products}}"><?=$m; ?></a>
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 'linea':
                                        ?>
                                        <div class="submenu _linea">
                                            <div>
                                                <div class="row">
                                                    <?php
                                                    for($i=0;$i<20;$i++){ ?>
                                                        <div class="col-md-2">
                                                            <a href="{{url linea}}?linea=<?=$i; ?>">Linea <?=($i+1); ?></a>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        break;
                                    case 'campagna':
                                        break;
                                }
                            }
                            ?>
                        </li>
                </ul></nav>
            </div>

            <div class="col-md-4 col-sm-3 hidden-xs">
                <form id="searchForm" enctype="application/x-www-form-urlencoded" method="get" action="{{url products}}" role="form">
                    <div class="input-group">
                        <input type="text" class="form-control" name="s" value="<?=$_GET['s']; ?>" placeholder="<# cerca un prodotto o un marchio #>">
                        <span class="input-group-btn">
                            <button type="submit" class="Btn btn"><# Cerca #></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="lineEnd"></div>
            </div>
        </div>
	</div>
</div>