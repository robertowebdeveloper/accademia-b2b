<?php if(false){ ?>
	<nav id="basketNav"><ul>
		<li<?=$_Step>=1 ? ' class="fired"' : ''; ?>>
			<a href="{{url basket}}"><span class="dot">1</span>
			<# Il tuo carrello #></a>
		</li>
		<li<?=$_Step>=2 ? ' class="fired"' : ''; ?>>
			<?php if($_Step>=2){ ?><a href="{{url basket-checkout}}"><?php } ?>
			<span class="dot">2</span>
			<# Metodo di pagamento #>
			<?php if($_Step>=2){ ?></a><?php } ?>
		</li>
		<li<?=$_Step>=3 ? ' class="fired"' : ''; ?>>
			<span class="dot">3</span>
			<# Conferma ordine #>
		</li>
	</ul></nav>
<?php }else{ ?>
	<ul id="basketNavTabs" class="nav nav-tabs nav-justified">
		<li role="presentation"<?=$_Step==1 ? ' class="active"' : ' class="prev"'; ?>>
			<?php $url = $_Step>1 ? '{{url basket}}' : '#'; ?>
			<a href="<?=$url; ?>">
				<span class="dot">1</span>
				<# Il tuo carrello #>
			</a>
		</li>

		<?php
		if($_Step>2){
			$class = ' class="prev"';
		}else if($_Step==2){
			$class = ' class="active"';
		}else{
			$class = '';
		}
		?>
		<li role="presentation"<?=$class; ?>>
			<?php $url = $_Step>2 ? '{{url basket-checkout}}' : '#'; ?>
			<a href="<?=$url; ?>">
				<span class="dot">2</span>
				<# Metodo di pagamento #>
			</a>
		</li>
		<li role="presentation"<?=$_Step==3 ? ' class="active"' : ''; ?>>
			<a href="#">
				<span class="dot">3</span>
				<# Conferma ordine #>
			</a>
		</li>
	</ul>
	<br><br>
<?php } ?>