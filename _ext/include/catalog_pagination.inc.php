<div class="catalog_pagination">
    <div class="row">
        <div class="col-sm-6 col-xs-5">
            <span class="uppercase hidden-xs"><# Ordina per #></span>
            &nbsp;&nbsp;
            <select name="sort" class="form-control selmini" data-refresh="1">
                <?php
                $arr = array(
                    "price,asc" => 'Prezzo &and;',
                    "price,desc" => 'Prezzo &or;',
                    "descr,asc" => 'Nome &and;',
                    "descr,desc" => 'Nome &or;'
                );
                foreach($arr as $k=>$v){
                    $sel = $k==$_GET['sort'] ? ' selected' : '';
                    ?><option value="<?=$k; ?>"<?=$sel; ?>><# <?=$v; ?> #></option><?php
                }
                ?>
            </select>
        </div>
        <div class="col-sm-6 col-xs-7">
            <div class="text-right text-left-xs paginationElements">
                <button type="button" class="Btn btn btn-sm disabled" data-nextPage="-1">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </button>
                &nbsp;&nbsp;
                <span class="uppercase hidden-xs hidden-sm"><# Pagina #></span>
                <select name="page" class="form-control hidden-xs" data-refresh="1">
                    <?php
                    for($i=0;$i<$ProductsList->page_max;$i++){
                        $sel = $i==$_GET['page'] ? ' selected' : '';
                        ?>
                        <option value="<?=$i; ?>"<?=$sel; ?>><?=($i+1)." / ".$ProductsList->page_max; ?></option>
                    <?php } ?>
                </select>
                &nbsp;&nbsp;
                <button type="button" class="Btn btn btn-sm<?=$ProductsList->page_max>1 ? '' : ' disabled'; ?>" data-nextPage="1">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </button>
            </div>
        </div>
    </div>
</div>