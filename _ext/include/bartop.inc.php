<div id="barTop">
    <div class="container">
        <div class="row">
	        <div class="col-md-6 col-xs-4 col-no-padding-right">
                <a href="{{url home}}" id="Logo"></a>
            </div>
	        <div class="col-md-6 col-xs-8 col-no-padding-left text-right">
                <nav>
                    <ul>
                        <?php if(false){ ?>
                            <?php if($S->_lang=="it"){ ?>
                                <li><a href="{{url lang en}}" class="gray"><# English version #></a></li>
                            <?php }else{ ?>
                                <li><a href="{{url lang it}}" class="gray"><# Versione italiana #></a></li>
                            <?php } ?>
                        <?php } ?>

                        <?php if($S->isAuth()){ ?>
                            <li class="btnAccount">
                                <span><# Benvenuto <b>%s</b> %txt=<?=$S->Customer->name; ?> #></span>
                                <ul class="submenu">
                                    <li><a href="{{url account}}"><# Il tuo profilo #></a></li>
                                    <li><a href="{{url ordini}}"><# I tuoi ordini #></a></li>
                                    <li><a href="{{url home}}?logout"><# Logout #></a></li>
                                </ul>
                            </li>
                        <?php }else{ ?>
                            <li><a href="#" data-signup="1"><# Registrati #></a></li>
                            <li><a href="#" data-login="1"><# Login #></a></li>
                        <?php } ?>
                        <li>
                            <a href="{{url basket}}" id="btnBasket"><i class="fa fa-shopping-cart fa-lg"></i> <span></span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>